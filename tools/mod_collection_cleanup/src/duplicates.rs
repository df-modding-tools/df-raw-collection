use sha2::{Digest, Sha256};
use std::{collections::HashMap, path::PathBuf};

/// Check only with default DF files
pub fn find_duplicate_files(files: Vec<PathBuf>) -> Vec<(PathBuf, PathBuf)> {
    let mut duplicates = Vec::new();

    let mut hash_table: HashMap<String, PathBuf> = HashMap::new();

    for file in files {
        let file_data = std::fs::read(&file).unwrap();
        let digest = Sha256::digest(&file_data);

        let digest_string = hex::encode(digest.as_slice());

        if let Some(matched_file) = hash_table.get(&digest_string) {
            if file.starts_with("../../df_v0.47.04/") {
                duplicates.push((file.clone(), matched_file.clone()));
            } else {
                duplicates.push((matched_file.clone(), file.clone()));
            }
        } else if file.starts_with("../../df_v0.47.04/") {
            hash_table.insert(digest_string, file.clone());
        }
    }

    duplicates
}
