mod duplicates;

use common_tools::{find_in_files, Filter};
use duplicates::find_duplicate_files;
use regex::Regex;
use std::path::Path;

fn main() {
    let root_folder = Path::new("../../");

    println!("Checking folder and subfolder.");

    // Find Non-txt files
    println!("Looking for non-txt files:");
    let filter = Filter::Exclude(Regex::new(r"^.*\.txt$").unwrap());
    let found_files = find_in_files(root_folder, &filter);
    if found_files.is_empty() {
        println!("✅ No non-.txt files found.");
    } else {
        println!("Files found:");
        for file in found_files {
            println!("File: {}", file.display());
        }
    }

    // Find Duplicates
    let filter = Filter::Include(Regex::new(r".*").unwrap());
    let found_files = find_in_files(root_folder, &filter);
    let duplicates = find_duplicate_files(found_files);
    if duplicates.is_empty() {
        println!("✅ No duplicate files found.");
    } else {
        println!("Duplicates found:");
        for (file1, file2) in duplicates {
            println!("{} = {}", file1.display(), file2.display());
        }
    }
}
