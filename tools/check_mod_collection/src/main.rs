mod check_file;
mod simple_logger;

use common_tools::{find_in_files, Filter};
use lsp_types::{Diagnostic, NumberOrString};
use regex::Regex;
use std::{
    collections::HashMap,
    path::{Path, PathBuf},
};

fn main() {
    initialize_logger();

    let root_folder = Path::new("../../");
    let filter = Filter::Include(Regex::new(r"^.*(?:\.txt)|(?:\.df)$").unwrap());
    let found_files = find_in_files(root_folder, &filter);
    println!("Found {} files to check", found_files.len());
    let mut all_diagnostics = HashMap::new();

    // Only do first X files for testing
    #[cfg(debug_assertions)]
    let found_files = found_files.into_iter().take(10);

    #[cfg(not(debug_assertions))]
    let found_files = found_files.into_iter();

    // Start timer
    let now = std::time::Instant::now();

    for file in found_files {
        let now = std::time::Instant::now();
        let diagnostics = check_file::check_file(&file);
        let elapsed_time = now.elapsed().as_millis();
        log::info!("Check file took: {} millisec", elapsed_time);
        // Files that take more then 2000 millisec, report.
        if elapsed_time > 2000 {
            println!("File: {} looks {} millisec", file.display(), elapsed_time);
        }
        log::info!(
            "Added {} diagnostic messages from: `{}`",
            diagnostics.len(),
            file.display()
        );
        all_diagnostics.insert(file.clone(), diagnostics);
    }
    println!("Checking file took: {} millisec", now.elapsed().as_millis());
    println!(
        "Total diagnostics: {}",
        all_diagnostics.iter().map(|(_k, v)| v.len()).sum::<usize>()
    );
    let filtered_diagnostics = filter_diagnostics(all_diagnostics);
    // Convert to Vec
    let mut filtered_diagnostics: Vec<(_, _)> = filtered_diagnostics
        .into_iter()
        .map(|(k, v)| (k, v))
        .collect();
    // Sort
    filtered_diagnostics.sort_by_key(|(_k, v)| {
        if let Some(diagnostic) = v.get(0) {
            diagnostic.message.clone()
        } else {
            String::new()
        }
    });
    println!(
        "Total diagnostics after filter: {}",
        filtered_diagnostics
            .iter()
            .map(|(_k, v)| v.len())
            .sum::<usize>()
    );

    let format = common_tools::DiagnosticFormat::Text;
    for (file, diagnostics) in filtered_diagnostics {
        if !diagnostics.is_empty() {
            println!("Diagnostics from {}", file.display());
            common_tools::print_diagnostics(diagnostics, &format, true);
        }
    }
}

fn filter_diagnostics(
    all_diagnostics: HashMap<PathBuf, Vec<Diagnostic>>,
) -> HashMap<PathBuf, Vec<Diagnostic>> {
    let mut new_list = HashMap::new();
    for (file, diagnostics) in all_diagnostics {
        let mut file_diagnostics = Vec::new();
        for diagnostic in diagnostics {
            if let Some(NumberOrString::String(name)) = &diagnostic.code {
                // Filter here
                if name != "unknown_token" {
                    continue;
                }
                file_diagnostics.push(diagnostic);
            }
        }
        new_list.insert(file.clone(), file_diagnostics);
    }
    new_list
}

/// Setup logger. This will select where to print the log message and how many.
fn initialize_logger() {
    let log_filter: log::LevelFilter = log::LevelFilter::Off;
    // Setup logger and log level
    log::set_logger(&simple_logger::LOGGER).unwrap();
    log::set_max_level(log_filter);
}
