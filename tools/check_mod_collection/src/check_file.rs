use lsp_types::Diagnostic;
use std::path::PathBuf;

pub fn check_file(file_path: &PathBuf) -> Vec<Diagnostic> {
    // Checking only 1 file.
    log::error!("Checking file: {}", file_path.display());
    // Check if the path points to a file.
    if !file_path.is_file() {
        panic!(
            "The given path is not a file, or the application \
            does not have access to this file."
        );
    }
    let source_code = std::fs::read(file_path).expect("Could not read test file");

    check_source(&source_code)
}

fn check_source(source: &[u8]) -> Vec<Diagnostic> {
    let now = std::time::Instant::now();
    // Convert source from CP437 to UTF-8
    let source_bytes = df_cp437::convert_cp437_to_utf8(source);
    let source = String::from_utf8(source_bytes).expect("Non UTF-8 characters found");
    // 1: Do Lexical Analysis (Tokenizer)
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);

    // 2: Do Syntax Analysis
    let (_structure, diagnostic_list_syntax): (df_ls_structure::DFRaw, Vec<Diagnostic>) =
        df_ls_syntax_analysis::do_syntax_analysis(&tree, &source);

    // Disabled for initial release
    // 3: Do Semantic Analysis
    // let diagnostic_list_semantics = df_ls_semantic_analysis::do_semantic_analysis(&structure);

    log::info!("This took: {} millisec", now.elapsed().as_millis());
    vec![
        diagnostic_list_lexer,
        diagnostic_list_syntax,
        // diagnostic_list_semantics,
    ]
    .concat()
}
