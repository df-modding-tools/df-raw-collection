use regex::Regex;
use std::path::{Path, PathBuf};

#[allow(dead_code)]
pub enum Filter {
    Include(Regex),
    Exclude(Regex),
}

pub fn find_in_files(root_folder: &Path, filter: &Filter) -> Vec<PathBuf> {
    if !root_folder.is_dir() {
        panic!("`root_folder` is not a folder");
    }

    let dir_read = std::fs::read_dir(root_folder).unwrap();
    let mut file_list = Vec::new();

    for path in dir_read {
        let file_path = path.unwrap().path();
        if is_in_ignore_list(file_path.as_path()) {
            // Skip over ignored folders/files
            continue;
        }
        if file_path.is_dir() {
            let mut list_from_subfolder = find_in_files(file_path.as_path(), filter);
            file_list.append(&mut list_from_subfolder);
        } else if file_path.is_file() {
            let file_path_string = format!("{}", file_path.display());
            // Match Regex
            match &filter {
                Filter::Include(include) => {
                    if include.is_match(&file_path_string) {
                        file_list.push(file_path.clone());
                    }
                }
                Filter::Exclude(exclude) => {
                    if !exclude.is_match(&file_path_string) {
                        file_list.push(file_path.clone());
                    }
                }
            }
        }
    }
    file_list
}

pub fn is_in_ignore_list(path: &Path) -> bool {
    let ignore_list = vec![
        Path::new("../../.git"),
        Path::new("../../tools"),
        Path::new("../../Cargo.lock"),
        Path::new("../../Cargo.toml"),
        Path::new("../../README.md"),
        Path::new("../../target"),
    ];

    ignore_list.contains(&path)
}
