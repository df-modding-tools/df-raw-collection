creature_standard

[OBJECT:CREATURE]

[CREATURE:KRAKEN]
	[DESCRIPTION:A staggeringly vast land octopus with a hunger for entire cities.]
	[NAME:kraken:krakens:kraken]
	[CASTE_NAME:kraken:krakens:kraken]
	[CREATURE_TILE:'O'][COLOR:6:0:0]
	[PETVALUE:30000][NOPAIN][NOFEAR]
	[LARGE_ROAMING]
	[PET][TRAINABLE][BUILDINGDESTROYER:2]
	[LARGE_PREDATOR]
	[HUNTS_VERMIN][MOUNT]
	[MEGABEAST][DIFFICULTY:10]
		[ATTACK_TRIGGER:80:10000:100000]
	[NOBONES][AMPHIBIOUS][UNDERSWIM]
	[NATURAL]
	[BIOME:ANY_LAND]
	[PREFSTRING:intelligence]
	[PREFSTRING:many arms]
	[BODY:BASIC_1PARTBODY:BASIC_HEAD:2EYES:BEAK:8_SIMPLE_HEAD_ARMS:BRAIN]
	[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
		[REMOVE_MATERIAL:HAIR]
		[REMOVE_MATERIAL:BONE]
		[REMOVE_MATERIAL:CARTILAGE]
		[USE_MATERIAL_TEMPLATE:CHITIN:CHITIN_TEMPLATE]
	[BODY_DETAIL_PLAN:STANDARD_TISSUES]
		[REMOVE_TISSUE:HAIR]
		[REMOVE_TISSUE:BONE]
		[REMOVE_TISSUE:CARTILAGE]
		[USE_TISSUE_TEMPLATE:CHITIN:CHITIN_TEMPLATE]
	[ATTACK:BITE:BODYPART:BY_CATEGORY:BEAK]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bite:bites]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PENETRATION_PERC:100]
		[ATTACK_FLAG_EDGE]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:MAIN]
		[ATTACK_FLAG_CANLATCH]
	[ATTACK:BITE:BODYPART:BY_CATEGORY:ARM]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bash:bashes]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PENETRATION_PERC:100]
		[ATTACK_FLAG_WITH]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:MAIN]
		[ATTACK_FLAG_CANLATCH]
	[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SKIN:FAT:MUSCLE:CHITIN:CHITIN]
	[HAS_NERVES]
	[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
		[STATE_COLOR:ALL:BLUE] copper not iron based
	[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
	[USE_MATERIAL_TEMPLATE:INK:INK_TEMPLATE]
		[STATE_COLOR:ALL:BLACK]
	[CREATURE_CLASS:GENERAL_POISON]
	[GETS_WOUND_INFECTIONS]
	[GETS_INFECTIONS_FROM_ROT]
	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
	[APPLY_CREATURE_VARIATION:STANDARD_QUADRUPED_GAITS:900:657:438:219:1900:2900] 40 kph
	[SWIMS_INNATE]
	[CANNOT_JUMP]
	[MUNDANE][CHILD:20]
	[BODY_SIZE:0:0:20]
	[BODY_SIZE:40:168:30000000]
	[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
	[CREPUSCULAR]
	[NO_SLEEP]
	[NO_DIZZINESS]
	[CASTE:FEMALE]
		[FEMALE]
	[CASTE:MALE]
		[MALE]
	[SELECT_CASTE:ALL]
	[SET_TL_GROUP:BY_CATEGORY:ALL:SKIN]
		[TL_COLOR_MODIFIER:BROWN:1] should be able to change its color
			[TLCM_NOUN:skin:SINGULAR]


[CREATURE:ARMORED_MEGALODON]
	[DESCRIPTION:A massive landshark covered in chitin as hard as iron. Nigh impossible to slay with any but the strongest weapons and armor.]
	[NAME:armored megalodon:armored megalodons:armored megalodon]
	[CASTE_NAME:armored megalodon:armored megalodons:armored megalodon]
	[CHILD:1][GENERAL_CHILD_NAME:armored megalodon pup:armored megalodon pups]
	[CREATURE_TILE:'M'][COLOR:6:0:0]
	[LARGE_ROAMING][PET][MOUNT][TRAINABLE][BUILDINGDESTROYER:2]
	[AMPHIBIOUS][UNDERSWIM][BIOME:ANY_LAND]
	[LARGE_PREDATOR][NATURAL]
	[PETVALUE:20000][CHILD:30]
	[PREFSTRING:ability to make one afraid]
	[MEGABEAST][DIFFICULTY:10]
		[ATTACK_TRIGGER:80:10000:100000][BODY:BASIC_2PARTBODY:BASIC_HEAD:SIDE_FINS:DORSAL_FIN:TAIL:2EYES:HEART:GUTS:ORGANS:SPINE:BRAIN:SKULL:MOUTH:RIBCAGE:GENERIC_TEETH]
	[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
		[REMOVE_MATERIAL:HAIR]
		[REMOVE_MATERIAL:BONE]
	[BODY_DETAIL_PLAN:STANDARD_TISSUES]
		[REMOVE_TISSUE:HAIR]
		[REMOVE_TISSUE:BONE]
	[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SKIN:FAT:MUSCLE:CARTILAGE:CARTILAGE]
	[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
	 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
		[TL_MAJOR_ARTERIES]
	[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
	[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
	[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
	[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
	[HAS_NERVES]
	[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
	[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
	[CREATURE_CLASS:GENERAL_POISON]
	[GETS_WOUND_INFECTIONS]
	[GETS_INFECTIONS_FROM_ROT]
	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
	[TISSUE:METAL2]
		[TISSUE_NAME:chitin:chitin]
		[TISSUE_MATERIAL:INORGANIC:HARDENED_CHITIN2]
		[MUSCULAR]
		[HEALING_RATE:10]
		[FUNCTIONAL]
		[STRUCTURAL]
		[RELATIVE_THICKNESS:1]
		[CONNECTS]
		[TISSUE_SHAPE:LAYER]
	[TISSUE_LAYER:BY_CATEGORY:ALL:METAL2]
	[BODY_SIZE:0:0:50000]
	[BODY_SIZE:1:0:500000]
	[BODY_SIZE:15:0:10000000]
	[BODY_APPEARANCE_MODIFIER:LENGTH:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
	[ATTACK:BITE:CHILD_BODYPART_GROUP:BY_CATEGORY:HEAD:BY_CATEGORY:TOOTH]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bite:bites]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PENETRATION_PERC:100]
		[ATTACK_PRIORITY:MAIN]
		[ATTACK_FLAG_EDGE]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_FLAG_CANLATCH]
	[ATTACK:SLAP:BODYPART:BY_CATEGORY:TAIL]
		[ATTACK_SKILL:STANCE_STRIKE]
		[ATTACK_VERB:slap:slaps]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PREPARE_AND_RECOVER:4:4]
		[ATTACK_PRIORITY:SECOND]
		[ATTACK_FLAG_WITH]
		[ATTACK_FLAG_BAD_MULTIATTACK]
	[ALL_ACTIVE]
	[NO_DRINK]
	[HOMEOTHERM:10067]
	[APPLY_CREATURE_VARIATION:STANDARD_WALKING_GAITS:900:691:482:251:1900:2900] 35 kph, NO DATA[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:900:691:482:251:1900:2900] 35 kph, NO DATA
	[SWIMS_INNATE]
	[CANNOT_JUMP]
	[MUNDANE]
	[CASTE:FEMALE]
		[FEMALE]
	[CASTE:MALE]
		[MALE]
	[SELECT_CASTE:ALL]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SKIN]
			[TL_COLOR_MODIFIER:WHITE:1]
				[TLCM_NOUN:skin:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:ALL:METAL2]
			[TL_COLOR_MODIFIER:BROWN:1]
				[TLCM_NOUN:chitin:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:BLACK:1]
				[TLCM_NOUN:eyes:PLURAL]


[CREATURE:BRINE_GIANT]
	[DESCRIPTION:An enormous humanoid giant grown mad and cruel. Giants were once wise sages and dispensers of wisdom, but no more.]
	[NAME:brine giant:brine giants:brine giant]
	[CREATURE_TILE:'G'][COLOR:3:0:0]
	[CREATURE_CLASS:MAMMAL]
	[FANCIFUL]
	[MEGABEAST][DIFFICULTY:10]
		[ATTACK_TRIGGER:80:10000:100000]
	[LARGE_PREDATOR]
	[INTELLIGENT]
	[CANOPENDOORS][POWER]
	[BUILDINGDESTROYER:2]
	[PREFSTRING:height]
	[BIOME:ANY_LAND]
	[CURIOUSBEAST_EATER]
	[CURIOUSBEAST_GUZZLER]
	[CURIOUSBEAST_ITEM]
	[SPHERE:FOOD]
	[SPHERE:STRENGTH]
	[PERSONALITY:AMBITION:50:75:100]
	[PERSONALITY:CRUELTY:50:75:100][BODY:HUMANOID_NECK:2EYES:2EARS:NOSE:2LUNGS:HEART:GUTS:ORGANS:HUMANOID_JOINTS:THROAT:NECK:SPINE:BRAIN:SKULL:5FINGERS:5TOES:MOUTH:TONGUE:FACIAL_FEATURES:TEETH:RIBCAGE]
	[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
	[BODY_DETAIL_PLAN:STANDARD_TISSUES]
	[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SKIN:FAT:MUSCLE:BONE:CARTILAGE]
	[BODY_DETAIL_PLAN:HEAD_HAIR_TISSUE_LAYERS]
	[USE_TISSUE_TEMPLATE:EYEBROW:EYEBROW_TEMPLATE]
	[TISSUE_LAYER:BY_CATEGORY:HEAD:EYEBROW:ABOVE:BY_CATEGORY:EYE]
	[USE_TISSUE_TEMPLATE:EYELASH:EYELASH_TEMPLATE]
	[TISSUE_LAYER:BY_CATEGORY:EYELID:EYELASH:FRONT]
	[USE_MATERIAL_TEMPLATE:NAIL:NAIL_TEMPLATE]
	[USE_TISSUE_TEMPLATE:NAIL:NAIL_TEMPLATE]
	[TISSUE_LAYER:BY_CATEGORY:FINGER:NAIL:FRONT]
	[TISSUE_LAYER:BY_CATEGORY:TOE:NAIL:FRONT]
	[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
	 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
		[TL_MAJOR_ARTERIES]
	[BODY_DETAIL_PLAN:FACIAL_HAIR_TISSUES]
	[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_HEAD_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_RELSIZES]
	[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
	[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
	[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
	[HAS_NERVES]
	[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
	[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
	[CREATURE_CLASS:GENERAL_POISON]
	[GETS_WOUND_INFECTIONS]
	[GETS_INFECTIONS_FROM_ROT]
	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
	[USE_MATERIAL_TEMPLATE:SWEAT:SWEAT_TEMPLATE]
	[USE_MATERIAL_TEMPLATE:TEARS:TEARS_TEMPLATE]
	[USE_MATERIAL_TEMPLATE:SPIT:SPIT_TEMPLATE]
	[SECRETION:LOCAL_CREATURE_MAT:SWEAT:LIQUID:BY_CATEGORY:ALL:SKIN:EXERTION]
	[SECRETION:LOCAL_CREATURE_MAT:TEARS:LIQUID:BY_CATEGORY:EYE:ALL:EXTREME_EMOTION]
	[CAN_DO_INTERACTION:MATERIAL_EMISSION]
		[CDI:ADV_NAME:Spit]
		[CDI:USAGE_HINT:NEGATIVE_SOCIAL_RESPONSE]
		[CDI:USAGE_HINT:TORMENT]
		[CDI:BP_REQUIRED:BY_CATEGORY:MOUTH]
		[CDI:MATERIAL:LOCAL_CREATURE_MAT:SPIT:LIQUID_GLOB]
		[CDI:VERB:spit:spits:NA]
		[CDI:TARGET:C:LINE_OF_SIGHT]
		[CDI:TARGET_RANGE:C:15]
		[CDI:MAX_TARGET_NUMBER:C:1]
		[CDI:WAIT_PERIOD:30]
	[BODY_SIZE:0:0:200000]
	[BODY_SIZE:1:168:3000000]
	[BODY_SIZE:12:0:20000000]
	[BODY_APPEARANCE_MODIFIER:HEIGHT:75:95:98:100:102:105:125]
		[APP_MOD_IMPORTANCE:500]
	[BODY_APPEARANCE_MODIFIER:BROADNESS:75:95:98:100:102:105:125]
		[APP_MOD_IMPORTANCE:500]
	[SET_BP_GROUP:BY_CATEGORY:EYE]
		[BP_APPEARANCE_MODIFIER:CLOSE_SET:0:70:90:100:110:130:200]
			[APP_MOD_NOUN:eyes:PLURAL]
		[BP_APPEARANCE_MODIFIER:DEEP_SET:0:70:90:100:110:130:200]
			[APP_MOD_NOUN:eyes:PLURAL]
		[BP_APPEARANCE_MODIFIER:ROUND_VS_NARROW:0:70:90:100:110:130:200]
			[APP_MOD_NOUN:eyes:PLURAL]
		[BP_APPEARANCE_MODIFIER:LARGE_IRIS:25:70:90:100:110:130:200]
			[APP_MOD_NOUN:eyes:PLURAL]
			[APP_MOD_DESC_RANGE:30:60:90:110:150:190]
	[SET_BP_GROUP:BY_CATEGORY:LIP]
		[BP_APPEARANCE_MODIFIER:THICKNESS:50:70:90:100:110:130:200]
			[APP_MOD_NOUN:lips:PLURAL]
			[APP_MOD_DESC_RANGE:55:70:90:110:150:190]
	[SET_BP_GROUP:BY_CATEGORY:NOSE]
		[BP_APPEARANCE_MODIFIER:BROADNESS:25:70:90:100:110:130:200]
			[APP_MOD_DESC_RANGE:30:60:90:110:150:190]
		[BP_APPEARANCE_MODIFIER:LENGTH:25:70:90:100:110:130:200]
			[APP_MOD_DESC_RANGE:30:60:90:110:150:190]
		[BP_APPEARANCE_MODIFIER:UPTURNED:0:70:90:100:110:130:200]
		[BP_APPEARANCE_MODIFIER:CONVEX:0:70:90:100:110:130:200]
			[APP_MOD_NOUN:nose bridge:SINGULAR]
	[SET_BP_GROUP:BY_CATEGORY:EAR]
		[BP_APPEARANCE_MODIFIER:SPLAYED_OUT:0:70:90:100:110:130:200]
			[APP_MOD_NOUN:ears:PLURAL]
		[BP_APPEARANCE_MODIFIER:HANGING_LOBES:0:70:90:100:110:130:200]
			[APP_MOD_NOUN:ears:PLURAL]
		[BP_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
			[APP_MOD_IMPORTANCE:700]
			[APP_MOD_NOUN:ears:PLURAL]
			[APP_MOD_DESC_RANGE:91:94:98:102:106:109]
		[BP_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
			[APP_MOD_IMPORTANCE:700]
			[APP_MOD_NOUN:ears:PLURAL]
			[APP_MOD_DESC_RANGE:91:94:98:102:106:109]
	[SET_BP_GROUP:BY_CATEGORY:TOOTH]
		[BP_APPEARANCE_MODIFIER:GAPS:0:70:90:100:110:130:200]
			[APP_MOD_NOUN:teeth:PLURAL]
		[BP_APPEARANCE_MODIFIER:LENGTH:100:100:100:100:100:100:100] for vampires
			[APP_MOD_IMPORTANCE:1000]
			[APP_MOD_NOUN:teeth:PLURAL]
			[APP_MOD_DESC_RANGE:30:60:90:110:150:190]
	[SET_BP_GROUP:BY_CATEGORY:SKULL]
		[BP_APPEARANCE_MODIFIER:HIGH_CHEEKBONES:0:70:90:100:110:130:200]
		[BP_APPEARANCE_MODIFIER:BROAD_CHIN:0:70:90:100:110:130:200]
		[BP_APPEARANCE_MODIFIER:JUTTING_CHIN:0:70:90:100:110:130:200]
		[BP_APPEARANCE_MODIFIER:SQUARE_CHIN:0:70:90:100:110:130:200]
	[SET_BP_GROUP:BY_CATEGORY:THROAT]
		[BP_APPEARANCE_MODIFIER:DEEP_VOICE:0:70:90:100:110:130:200]
		[BP_APPEARANCE_MODIFIER:RASPY_VOICE:0:70:90:100:110:130:200]
	[SET_BP_GROUP:BY_CATEGORY:HEAD]
		[BP_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
			[APP_MOD_IMPORTANCE:700]
			[APP_MOD_DESC_RANGE:91:94:98:102:106:109]
		[BP_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
			[APP_MOD_IMPORTANCE:700]
			[APP_MOD_DESC_RANGE:91:94:98:102:106:109]
	[ATTACK:PUNCH:BODYPART:BY_TYPE:GRASP]
		[ATTACK_SKILL:GRASP_STRIKE]
		[ATTACK_VERB:punch:punches]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_FLAG_WITH]
		[ATTACK_PRIORITY:MAIN]
	[ATTACK:KICK:BODYPART:BY_TYPE:STANCE]
		[ATTACK_SKILL:STANCE_STRIKE]
		[ATTACK_VERB:kick:kicks]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PREPARE_AND_RECOVER:4:4]
		[ATTACK_FLAG_WITH]
		[ATTACK_PRIORITY:SECOND]
		[ATTACK_FLAG_BAD_MULTIATTACK]
	[ATTACK:SCRATCH:CHILD_TISSUE_LAYER_GROUP:BY_TYPE:GRASP:BY_CATEGORY:FINGER:NAIL]
		[ATTACK_SKILL:GRASP_STRIKE]
		[ATTACK_VERB:scratch:scratches]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PENETRATION_PERC:100]
		[ATTACK_FLAG_EDGE]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:SECOND]
	[ATTACK:BITE:CHILD_BODYPART_GROUP:BY_CATEGORY:HEAD:BY_CATEGORY:TOOTH]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bite:bites]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PENETRATION_PERC:100]
		[ATTACK_FLAG_EDGE]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:SECOND]
		[ATTACK_FLAG_CANLATCH]
	[BABY:1]
	[CHILD:12]
	[EQUIPS]
	[DIURNAL]
	[LAIR:SIMPLE_BURROW:100]
	[HOMEOTHERM:10067]
	[APPLY_CREATURE_VARIATION:STANDARD_BIPED_GAITS:900:657:438:219:1900:2900] 40 kph
	[APPLY_CREATURE_VARIATION:STANDARD_CLIMBING_GAITS:2990:2257:1525:731:4300:6100] 12 kph
	[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:2990:2257:1525:731:4300:6100] 12 kph
	[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:2990:2257:1525:731:4300:6100] 12 kph
	[SWIMS_LEARNED]
	[CASTE:FEMALE]
		[FEMALE]
		[MULTIPLE_LITTER_RARE]
		[CASTE_NAME:brine giantess:brine giantesses:brine giantess]
	[CASTE:MALE]
		[MALE]
		[SET_BP_GROUP:BY_TYPE:LOWERBODY][BP_ADD_TYPE:GELDABLE]
		[BODY_DETAIL_PLAN:FACIAL_HAIR_TISSUE_LAYERS]
		[CASTE_NAME:brine giant:brine giants:brine giant]
	[SELECT_CASTE:ALL]
		[SET_TL_GROUP:BY_CATEGORY:HEAD:HAIR]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:CHEEK_WHISKERS]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:CHIN_WHISKERS]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:MOUSTACHE]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:SIDEBURNS]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:EYEBROW]
		 [PLUS_TL_GROUP:BY_CATEGORY:EYELID:EYELASH]
	[TL_COLOR_MODIFIER:GRAY:1]
				[TLCM_NOUN:hair:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:HEAD:EYEBROW]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:LENGTH:50:80:90:100:110:120:150]
				[APP_MOD_NOUN:eyebrows:PLURAL]
				[APP_MOD_DESC_RANGE:55:70:90:110:130:145]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:DENSE:50:80:90:100:110:120:150]
				[APP_MOD_NOUN:eyebrows:PLURAL]
				[APP_MOD_DESC_RANGE:55:70:90:110:130:145]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:HIGH_POSITION:0:70:90:100:110:130:200]
				[APP_MOD_NOUN:eyebrows:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:EYELID:EYELASH]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:LENGTH:50:80:90:100:110:120:150]
				[APP_MOD_NOUN:eyelashes:PLURAL]
				[APP_MOD_DESC_RANGE:55:70:90:110:130:145]
		[SET_TL_GROUP:BY_CATEGORY:HEAD:HAIR]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:CHEEK_WHISKERS]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:CHIN_WHISKERS]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:MOUSTACHE]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:SIDEBURNS]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:LENGTH:0:0:0:0:0:0:0]
				[APP_MOD_NOUN:hair:SINGULAR]
				[APP_MOD_RATE:1:DAILY:0:1000:0:0:NO_END]
				[APP_MOD_DESC_RANGE:10:25:75:125:200:300]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:CURLY:0:70:90:100:110:130:200]
				[APP_MOD_NOUN:hair:SINGULAR]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:GREASY:0:70:90:100:110:130:200]
				[APP_MOD_NOUN:hair:SINGULAR]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:DENSE:50:80:90:100:110:120:150]
				[APP_MOD_NOUN:hair:SINGULAR]
				[APP_MOD_DESC_RANGE:55:70:90:110:130:145]
		[SET_TL_GROUP:BY_CATEGORY:HEAD:HAIR]
			[TISSUE_STYLE_UNIT:HAIR:STANDARD_HAIR_SHAPINGS]
				[TSU_NOUN:hair:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:HEAD:CHEEK_WHISKERS]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:CHIN_WHISKERS]
			[TISSUE_STYLE_UNIT:BEARD:STANDARD_BEARD_SHAPINGS]
				[TSU_NOUN:beard:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:HEAD:MOUSTACHE]
			[TISSUE_STYLE_UNIT:MOUSTACHE:STANDARD_MOUSTACHE_SHAPINGS]
				[TSU_NOUN:moustache:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:HEAD:SIDEBURNS]
			[TISSUE_STYLE_UNIT:SIDEBURNS:STANDARD_SIDEBURNS_SHAPINGS]
				[TSU_NOUN:sideburns:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SKIN]
	[TL_COLOR_MODIFIER:SLATE_GRAY:1]
				[TLCM_NOUN:salt-encrusted skin:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:BLACK:1]
				[TLCM_NOUN:eyes:PLURAL]

[CREATURE:MOUNTAIN_CRAB]
	[DESCRIPTION:A gigantic crab resembling a small mountain. Its claws are as strong as iron.]
	[NAME:mountain crab:mountain crabs:mountain crab]
	[CASTE_NAME:mountain crab:mountain crabs:mountain crab]
	[CREATURE_TILE:'C'][COLOR:0:0:1]
	[NATURAL]
	[MEGABEAST][DIFFICULTY:10][BUILDINGDESTROYER:2]
		[ATTACK_TRIGGER:80:10000:100000]
	[BIOME:ANY_LAND]
	[LARGE_ROAMING]
	[PREFSTRING:size]
	[AMPHIBIOUS][UNDERSWIM]
	[ALL_ACTIVE]
	[NO_SLEEP]
	[HOMEOTHERM:10071]
	[APPLY_CREATURE_VARIATION:STANDARD_WALKING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[APPLY_CREATURE_VARIATION:STANDARD_CLIMBING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[NATURAL_SKILL:CLIMBING:15]
	[MUNDANE]
	[NOBONES]
	[BODY:CRAB_BODY:2EYES:HEART:BRAIN:UPPERBODY_PINCERS:MOUTH]
	[BODY_DETAIL_PLAN:CHITIN_MATERIALS]
	[BODY_DETAIL_PLAN:CHITIN_TISSUES]
	[BODY_DETAIL_PLAN:EXOSKELETON_TISSUE_LAYERS:CHITIN:FAT:MUSCLE]
	[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
	[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
	[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
	[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
	[TISSUE:METAL2]
		[TISSUE_NAME:chitin:chitin]
		[TISSUE_MATERIAL:INORGANIC:HARDENED_CHITIN3]
		[MUSCULAR]
		[HEALING_RATE:10]
		[FUNCTIONAL]
		[STRUCTURAL]
		[RELATIVE_THICKNESS:1]
		[CONNECTS]
		[TISSUE_SHAPE:LAYER]
	[TISSUE_LAYER:BY_CATEGORY:PINCER:METAL2]
	[HAS_NERVES]
	[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
		[STATE_COLOR:ALL:BLUE] copper not iron based
	[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
	[CREATURE_CLASS:GENERAL_POISON]
	[GETS_WOUND_INFECTIONS]
	[GETS_INFECTIONS_FROM_ROT]
	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
	[BODY_SIZE:0:0:1]
	[BODY_SIZE:30:0:40000000]
	[ATTACK:BITE:BODYPART:BY_CATEGORY:MOUTH]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bite:bites]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_FLAG_EDGE]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:SECOND]
		[ATTACK_FLAG_CANLATCH]
	[APPLY_CREATURE_VARIATION:PINCER_ATTACK]
	[CASTE:FEMALE]
		[FEMALE]
	[CASTE:MALE]
		[MALE]
	[SELECT_CASTE:ALL]
		[SET_TL_GROUP:BY_CATEGORY:ALL:CHITIN]
			[TL_COLOR_MODIFIER:ASH_GRAY:1]
				[TLCM_NOUN:chitin:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:BLACK:1]
				[TLCM_NOUN:eyes:PLURAL]

[CREATURE:HYDRA-MORAY]
	[DESCRIPTION:A eel-like creature with a seal's body and five terribly strong heads.]
	[NAME:hydra-moray:hydra-morays:hydra-moray]
	[CASTE_NAME:hydra-moray:hydra-morays:hydra-moray]
	[CREATURE_TILE:'H'][COLOR:2:0:1]
	[PETVALUE:10000]
	[PET_EXOTIC][TRAINABLE][MOUNT_EXOTIC]
	[BIOME:ANY_LAND]
	[FREQUENCY:5]
	[FANCIFUL]
	[LARGE_PREDATOR]
	[MEGABEAST][DIFFICULTY:10]
		[ATTACK_TRIGGER:80:10000:100000]
	[SPHERE:MUCK]
	[SPHERE:REBIRTH]
	[SPHERE:STRENGTH]
	[NOFEAR][NOEXERT]
	[NOSTUN]
	[NOPAIN]
	[BUILDINGDESTROYER:2]
	[GRASSTRAMPLE:50]
	[BONECARN]
	[PREFSTRING:five heads]
	[SEMIMEGABEAST][DIFFICULTY:5]
		[ATTACK_TRIGGER:50:5000:50000]

[BODY:BASIC_2PARTBODY:5HEADNECKS:BASIC_3PARTLEGS:TAIL:2EYES:NOSE:2LUNGS:HEART:GUTS:ORGANS:THROAT:SPINE:BRAIN:SKULL:3TOES:MOUTH:TONGUE:GENERIC_TEETH_WITH_FANGS:RIBCAGE]
	[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
	[BODY_DETAIL_PLAN:STANDARD_TISSUES]
	[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SKIN:FAT:MUSCLE:BONE:CARTILAGE]
	[USE_MATERIAL_TEMPLATE:CLAW:NAIL_TEMPLATE]
	[USE_TISSUE_TEMPLATE:CLAW:CLAW_TEMPLATE]
	[TISSUE_LAYER:BY_CATEGORY:TOE:CLAW:FRONT]
	[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
		[TL_MAJOR_ARTERIES]
	[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
	[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
	[TENDONS:LOCAL_CREATURE_MAT:SINEW:1]
	[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:1]
	[HAS_NERVES]
	[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
	[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
	[CREATURE_CLASS:GENERAL_POISON]
	[GETS_WOUND_INFECTIONS]
	[GETS_INFECTIONS_FROM_ROT]
	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
	[BODY_SIZE:0:0:200000]
	[BODY_SIZE:20:0:8000000]
	[BODY_APPEARANCE_MODIFIER:LENGTH:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
	[ATTACK:BITE:CHILD_BODYPART_GROUP:BY_CATEGORY:HEAD:BY_CATEGORY:TOOTH]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bite:bites]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PENETRATION_PERC:100]
		[ATTACK_FLAG_EDGE]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:MAIN]
		[ATTACK_FLAG_CANLATCH]
		[ATTACK_FLAG_INDEPENDENT_MULTIATTACK]
	[ATTACK:CLAW:CHILD_TISSUE_LAYER_GROUP:BY_TYPE:STANCE:BY_CATEGORY:ALL:CLAW]
		[ATTACK_SKILL:GRASP_STRIKE]
		[ATTACK_VERB:claw:claws]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PENETRATION_PERC:100]
		[ATTACK_FLAG_EDGE]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:SECOND]
	[ALL_ACTIVE]
	[LAIR:SIMPLE_BURROW:100]
	[LAIR_HUNTER]
	[MULTIPART_FULL_VISION]
	[NATURAL_SKILL:BITE:8]
	[NATURAL_SKILL:GRASP_STRIKE:6]
	[NATURAL_SKILL:MELEE_COMBAT:6]
	[NATURAL_SKILL:DODGING:6]
	[NATURAL_SKILL:SITUATIONAL_AWARENESS:14]
	[APPLY_CREATURE_VARIATION:STANDARD_QUADRUPED_GAITS:900:730:561:351:1900:2900] 25 kph
	[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:3512:2634:1756:878:4900:6900] 10 kph
	[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:6561:6115:5683:1755:7456:8567] 5 kph
	[SWIMS_INNATE]
	[CANNOT_JUMP]
	[HOMEOTHERM:10040]
	[CASTE:FEMALE]
		[FEMALE]
	[CASTE:MALE]
		[MALE]
	[SELECT_CASTE:ALL]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SKIN]
			[TL_COLOR_MODIFIER:GREEN:1]
				[TLCM_NOUN:skin:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:BLACK:1]
				[TLCM_NOUN:eyes:PLURAL]
	[SELECT_MATERIAL:ALL]
		[MULTIPLY_VALUE:40]
	[SELECT_TISSUE_LAYER:ALL]
		[TL_HEALING_RATE:1]

[CREATURE:CLAWED_APE]
	[DESCRIPTION:A giant, vicious ape-like thing covered in coarse, shaggy hair that hides their features besides a toothy maw, and a pair of crab-like claws. Believed to be the result of unnatural experimentation.]
	[NAME:clawed ape:clawed apes:clawed ape]
	[CASTE_NAME:clawed ape:clawed apes:clawed ape]
	[CREATURE_TILE:'A'][COLOR:6:0:0]
	[CREATURE_CLASS:EVILMONSTER]
	[BUILDINGDESTROYER:1][BIOME:ANY_LAND]
	[LARGE_PREDATOR][NOFEAR]
	[FREQUENCY:5][LARGE_ROAMING]
	[SEMIMEGABEAST][DIFFICULTY:5]
		[ATTACK_TRIGGER:50:5000:50000]
	[NATURAL_SKILL:GRASP_STRIKE:3]
	[NATURAL_SKILL:STANCE_STRIKE:3]
	[NATURAL_SKILL:WRESTLING:3]
	[NATURAL_SKILL:BITE:3]
		[PHYS_ATT_RANGE:TOUGHNESS:700:1200:1400:1500:1600:1800:2500]            ++
		[PHYS_ATT_RANGE:STRENGTH:700:1200:1400:1500:1600:1800:2500]            ++[BODY:HUMANOID_NOHAND:2EYES:2EARS:NOSE:2LUNGS:HEART:GUTS:ORGANS:HUMANOID_JOINTS:THROAT:NECK:SPINE:BRAIN:SKULL:5TOES:MOUTH:TONGUE:FACIAL_FEATURES:TEETH:RIBCAGE:HAND_PINCERS]
	[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
	[BODY_DETAIL_PLAN:STANDARD_TISSUES]
	[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SKIN:FAT:MUSCLE:BONE:CARTILAGE]
	[BODY_DETAIL_PLAN:HEAD_HAIR_TISSUE_LAYERS]
	[USE_TISSUE_TEMPLATE:EYEBROW:EYEBROW_TEMPLATE]
	[TISSUE_LAYER:BY_CATEGORY:HEAD:EYEBROW:ABOVE:BY_CATEGORY:EYE]
	[USE_TISSUE_TEMPLATE:EYELASH:EYELASH_TEMPLATE]
	[TISSUE_LAYER:BY_CATEGORY:EYELID:EYELASH:FRONT]
	[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
	 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
		[TL_MAJOR_ARTERIES]
	[BODY_DETAIL_PLAN:FACIAL_HAIR_TISSUES]
	[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_HEAD_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_RELSIZES]
	[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
	[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
	[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
	[HAS_NERVES]
	[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
	[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
	[CREATURE_CLASS:GENERAL_POISON]
	[GETS_WOUND_INFECTIONS]
	[GETS_INFECTIONS_FROM_ROT]
	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
	[USE_MATERIAL_TEMPLATE:SWEAT:SWEAT_TEMPLATE]
	[USE_MATERIAL_TEMPLATE:TEARS:TEARS_TEMPLATE]
	[USE_MATERIAL_TEMPLATE:SPIT:SPIT_TEMPLATE]
	[SECRETION:LOCAL_CREATURE_MAT:SWEAT:LIQUID:BY_CATEGORY:ALL:SKIN:EXERTION]
	[SECRETION:LOCAL_CREATURE_MAT:TEARS:LIQUID:BY_CATEGORY:EYE:ALL:EXTREME_EMOTION]
	[CAN_DO_INTERACTION:MATERIAL_EMISSION]
		[CDI:ADV_NAME:Spit]
		[CDI:USAGE_HINT:NEGATIVE_SOCIAL_RESPONSE]
		[CDI:USAGE_HINT:TORMENT]
		[CDI:BP_REQUIRED:BY_CATEGORY:MOUTH]
		[CDI:MATERIAL:LOCAL_CREATURE_MAT:SPIT:LIQUID_GLOB]
		[CDI:VERB:spit:spits:NA]
		[CDI:TARGET:C:LINE_OF_SIGHT]
		[CDI:TARGET_RANGE:C:15]
		[CDI:MAX_TARGET_NUMBER:C:1]
		[CDI:WAIT_PERIOD:30]
	[BODY_SIZE:0:0:4000]
	[BODY_SIZE:1:168:17500]
	[BODY_SIZE:12:0:9000000]
	[BODY_APPEARANCE_MODIFIER:HEIGHT:75:95:98:100:102:105:125]
		[APP_MOD_IMPORTANCE:500]
	[BODY_APPEARANCE_MODIFIER:BROADNESS:75:95:98:100:102:105:125]
		[APP_MOD_IMPORTANCE:500]
	[SET_BP_GROUP:BY_CATEGORY:EYE]
		[BP_APPEARANCE_MODIFIER:CLOSE_SET:0:70:90:100:110:130:200]
			[APP_MOD_NOUN:eyes:PLURAL]
		[BP_APPEARANCE_MODIFIER:DEEP_SET:0:70:90:100:110:130:200]
			[APP_MOD_NOUN:eyes:PLURAL]
		[BP_APPEARANCE_MODIFIER:ROUND_VS_NARROW:0:70:90:100:110:130:200]
			[APP_MOD_NOUN:eyes:PLURAL]
		[BP_APPEARANCE_MODIFIER:LARGE_IRIS:25:70:90:100:110:130:200]
			[APP_MOD_NOUN:eyes:PLURAL]
			[APP_MOD_DESC_RANGE:30:60:90:110:150:190]
	[SET_BP_GROUP:BY_CATEGORY:LIP]
		[BP_APPEARANCE_MODIFIER:THICKNESS:50:70:90:100:110:130:200]
			[APP_MOD_NOUN:lips:PLURAL]
			[APP_MOD_DESC_RANGE:55:70:90:110:150:190]
	[SET_BP_GROUP:BY_CATEGORY:NOSE]
		[BP_APPEARANCE_MODIFIER:BROADNESS:25:70:90:100:110:130:200]
			[APP_MOD_DESC_RANGE:30:60:90:110:150:190]
		[BP_APPEARANCE_MODIFIER:LENGTH:25:70:90:100:110:130:200]
			[APP_MOD_DESC_RANGE:30:60:90:110:150:190]
		[BP_APPEARANCE_MODIFIER:UPTURNED:0:70:90:100:110:130:200]
		[BP_APPEARANCE_MODIFIER:CONVEX:0:70:90:100:110:130:200]
			[APP_MOD_NOUN:nose bridge:SINGULAR]
	[SET_BP_GROUP:BY_CATEGORY:EAR]
		[BP_APPEARANCE_MODIFIER:SPLAYED_OUT:0:70:90:100:110:130:200]
			[APP_MOD_NOUN:ears:PLURAL]
		[BP_APPEARANCE_MODIFIER:HANGING_LOBES:0:70:90:100:110:130:200]
			[APP_MOD_NOUN:ears:PLURAL]
		[BP_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
			[APP_MOD_IMPORTANCE:700]
			[APP_MOD_NOUN:ears:PLURAL]
			[APP_MOD_DESC_RANGE:91:94:98:102:106:109]
		[BP_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
			[APP_MOD_IMPORTANCE:700]
			[APP_MOD_NOUN:ears:PLURAL]
			[APP_MOD_DESC_RANGE:91:94:98:102:106:109]
	[SET_BP_GROUP:BY_CATEGORY:TOOTH]
		[BP_APPEARANCE_MODIFIER:GAPS:0:70:90:100:110:130:200]
			[APP_MOD_NOUN:teeth:PLURAL]
		[BP_APPEARANCE_MODIFIER:LENGTH:100:100:100:100:100:100:100] for vampires
			[APP_MOD_IMPORTANCE:1000]
			[APP_MOD_NOUN:teeth:PLURAL]
			[APP_MOD_DESC_RANGE:30:60:90:110:150:190]
	[SET_BP_GROUP:BY_CATEGORY:SKULL]
		[BP_APPEARANCE_MODIFIER:HIGH_CHEEKBONES:0:70:90:100:110:130:200]
		[BP_APPEARANCE_MODIFIER:BROAD_CHIN:0:70:90:100:110:130:200]
		[BP_APPEARANCE_MODIFIER:JUTTING_CHIN:0:70:90:100:110:130:200]
		[BP_APPEARANCE_MODIFIER:SQUARE_CHIN:0:70:90:100:110:130:200]
	[SET_BP_GROUP:BY_CATEGORY:THROAT]
		[BP_APPEARANCE_MODIFIER:DEEP_VOICE:0:70:90:100:110:130:200]
		[BP_APPEARANCE_MODIFIER:RASPY_VOICE:0:70:90:100:110:130:200]
	[SET_BP_GROUP:BY_CATEGORY:HEAD]
		[BP_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
			[APP_MOD_IMPORTANCE:700]
			[APP_MOD_DESC_RANGE:91:94:98:102:106:109]
		[BP_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
			[APP_MOD_IMPORTANCE:700]
			[APP_MOD_DESC_RANGE:91:94:98:102:106:109]
	[MAXAGE:60:120]
	[APPLY_CREATURE_VARIATION:PINCER_ATTACK]
	[ATTACK:KICK:BODYPART:BY_TYPE:STANCE]
		[ATTACK_SKILL:STANCE_STRIKE]
		[ATTACK_VERB:kick:kicks]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PREPARE_AND_RECOVER:4:4]
		[ATTACK_FLAG_WITH]
		[ATTACK_PRIORITY:SECOND]
		[ATTACK_FLAG_BAD_MULTIATTACK]
	[ATTACK:BITE:CHILD_BODYPART_GROUP:BY_CATEGORY:HEAD:BY_CATEGORY:TOOTH]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bite:bites]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PENETRATION_PERC:100]
		[ATTACK_FLAG_EDGE]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:SECOND]
		[ATTACK_FLAG_CANLATCH]
	[BABY:1]
	[CHILD:12]
	[EQUIPS]
	[DIURNAL]
	[SMELL_TRIGGER:90]
	[HOMEOTHERM:10067]
	[APPLY_CREATURE_VARIATION:STANDARD_BIPED_GAITS:900:675:450:225:1900:2900] 39 kph
	[APPLY_CREATURE_VARIATION:STANDARD_CLIMBING_GAITS:6561:6115:5683:1755:7456:8567] 5 kph, NO DATA
	[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:5341:4723:4112:1254:6433:7900] 7 kph
	[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:2990:2257:1525:731:4300:6100] 12 kph, NO DATA
	[SWIMS_INNATE]
	[CASTE:FEMALE]
		[FEMALE]
		[MULTIPLE_LITTER_RARE]
	[CASTE:MALE]
		[MALE]
		[SET_BP_GROUP:BY_TYPE:LOWERBODY][BP_ADD_TYPE:GELDABLE]
		[BODY_DETAIL_PLAN:FACIAL_HAIR_TISSUE_LAYERS]
	[SELECT_CASTE:ALL]
		[SET_TL_GROUP:BY_CATEGORY:HEAD:HAIR]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:CHEEK_WHISKERS]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:CHIN_WHISKERS]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:MOUSTACHE]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:SIDEBURNS]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:EYEBROW]
		 [PLUS_TL_GROUP:BY_CATEGORY:EYELID:EYELASH]
	[TL_COLOR_MODIFIER:BLACK:1]
				[TLCM_NOUN:hair:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:HEAD:EYEBROW]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:LENGTH:50:80:90:100:110:120:150]
				[APP_MOD_NOUN:eyebrows:PLURAL]
				[APP_MOD_DESC_RANGE:55:70:90:110:130:145]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:DENSE:50:80:90:100:110:120:150]
				[APP_MOD_NOUN:eyebrows:PLURAL]
				[APP_MOD_DESC_RANGE:55:70:90:110:130:145]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:HIGH_POSITION:0:70:90:100:110:130:200]
				[APP_MOD_NOUN:eyebrows:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:EYELID:EYELASH]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:LENGTH:50:80:90:100:110:120:150]
				[APP_MOD_NOUN:eyelashes:PLURAL]
				[APP_MOD_DESC_RANGE:55:70:90:110:130:145]
		[SET_TL_GROUP:BY_CATEGORY:HEAD:HAIR]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:CHEEK_WHISKERS]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:CHIN_WHISKERS]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:MOUSTACHE]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:SIDEBURNS]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:LENGTH:0:0:0:0:0:0:0]
				[APP_MOD_NOUN:hair:SINGULAR]
				[APP_MOD_RATE:1:DAILY:0:1000:0:0:NO_END]
				[APP_MOD_DESC_RANGE:10:25:75:125:200:300]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:CURLY:0:70:90:100:110:130:200]
				[APP_MOD_NOUN:hair:SINGULAR]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:GREASY:0:70:90:100:110:130:200]
				[APP_MOD_NOUN:hair:SINGULAR]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:DENSE:50:80:90:100:110:120:150]
				[APP_MOD_NOUN:hair:SINGULAR]
				[APP_MOD_DESC_RANGE:55:70:90:110:130:145]
		[SET_TL_GROUP:BY_CATEGORY:HEAD:HAIR]
			[TISSUE_STYLE_UNIT:HAIR:STANDARD_HAIR_SHAPINGS]
				[TSU_NOUN:hair:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:HEAD:CHEEK_WHISKERS]
		 [PLUS_TL_GROUP:BY_CATEGORY:HEAD:CHIN_WHISKERS]
			[TISSUE_STYLE_UNIT:BEARD:STANDARD_BEARD_SHAPINGS]
				[TSU_NOUN:beard:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:HEAD:MOUSTACHE]
			[TISSUE_STYLE_UNIT:MOUSTACHE:STANDARD_MOUSTACHE_SHAPINGS]
				[TSU_NOUN:moustache:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:HEAD:SIDEBURNS]
			[TISSUE_STYLE_UNIT:SIDEBURNS:STANDARD_SIDEBURNS_SHAPINGS]
				[TSU_NOUN:sideburns:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SKIN]
	[TL_COLOR_MODIFIER:BROWN:1]
				[TLCM_NOUN:skin:SINGULAR]
			[TISSUE_LAYER_APPEARANCE_MODIFIER:WRINKLY:0:0:0:0:0:0:0]
				[APP_MOD_RATE:1:YEARLY:0:100:30:0:NO_END]
				[APP_MOD_NOUN:skin:SINGULAR]
				[APP_MOD_DESC_RANGE:0:0:0:1:25:50]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:IRIS_EYE_RED:1]
				[TLCM_NOUN:eyes:PLURAL]

