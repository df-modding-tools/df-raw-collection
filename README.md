# Dwarf Fortress RAW collection

This a collection of Dwarf Fortress RAW files. This is intended to test and search though for
creating the [DF Raw Syntax](https://gitlab.com/df-modding-tools/df-raw-syntax).
The files included in this repo are not intended to be used by the game. But are put into a
collection for research purposes.

## Mods included

The mods included in the repo are semi random to get see what people to and have done with RAW
files. The images and other assets are removed.

| Folder                       | Name                                 | Version         | Link                                                    |
| ---------------------------- | ------------------------------------ | --------------- | ------------------------------------------------------- |
| `df_v0.47.04`                | (Vanilla) Dwarf Fortress             | v0.47.04        | https://www.bay12games.com/dwarves/                     |
| `df_v0.50.01`                | (Vanilla) Dwarf Fortress             | v0.50.01        | https://store.steampowered.com/app/975370               |
| `masterwork_df_v1.31`        | ☼MASTERWORK DF☼                      | v1.31           | http://www.bay12forums.com/smf/index.php?topic=125638   |
| `meph_tileset_v5.5`          | ☼Meph Tileset☼                       | v5.5            | http://www.bay12forums.com/smf/index.php?topic=161047   |
| `dwarfest_dungeon`           | Dwarfest Dungeon                     | 2020/10/08      | http://www.bay12forums.com/smf/index.php?topic=168740   |
| `ancient_weapons_mod_v1.0`   | Ancient Weapons Mod                  | v1.0            | http://www.bay12forums.com/smf/index.php?topic=160827   |
| `dwarf_4tress_from_scratch`  | Dwarf 4tress From Scratch            | v0.4            | http://www.bay12forums.com/smf/index.php?topic=160947   |
| `kobold_kamp`                | Kobold Kamp                          | .47.02 - .47.04 | http://www.bay12forums.com/smf/index.php?topic=156989   |
| `fortress_defense_mod_2`     | Fortress Defense Mod II              | v0.21           | http://www.bay12forums.com/smf/index.php?topic=62874    |
| `haberdasher_cloth_armor`    | Haberdasher Cloth Armor/Industry Mod | v1.4            | http://www.bay12forums.com/smf/index.php?topic=158967   |
| `adventurecraft`             | Adventurecraft                       | .47.02 - .47.04 | http://www.bay12forums.com/smf/index.php?topic=153036   |
| `the_earth_strikes_back`     | The Earth Strikes Back               | v2.15           | http://www.bay12forums.com/smf/index.php?topic=144831   |
| `stals_armoury_pack`         | Stal's Armoury Pack                  | v1.9            | http://www.bay12forums.com/smf/index.php?topic=142993   |
| `dark_ages_4_war_and_mythos` | Dark Ages IV: War & Mythos           | v39h            | http://www.bay12forums.com/smf/index.php?topic=143540   |
| `df_wanderer`                | DF Wanderer                          | r21             | http://www.bay12forums.com/smf/index.php?topic=140950   |
| `phoebus_graphic_set`        | Phoebus' Graphic Set                 | 0.34.11v01      | http://www.bay12forums.com/smf/index.php?topic=57557    |
| `spacefox`                   | Spacefox                             | 2013/08/01      | http://www.bay12forums.com/smf/index.php?topic=129219   |
| `mayday`                     | Mayday's tileset                     | 47.04           | http://www.bay12forums.com/smf/index.php?topic=137370   |
| `the_long_night`             | The Long Night                       | 3.786           | http://www.bay12forums.com/smf/index.php?topic=179676   |
| `fall_from_grace`            | Fall From Grace                      | 2.52            | http://www.bay12forums.com/smf/index.php?topic=174951   |
| `endless_shores`             | Endless Shores                       | 1.72            | http://www.bay12forums.com/smf/index.php?topic=172418   |
| `genesis_mod`                | Genesis Mod                          | 3.27b           | http://www.bay12forums.com/smf/index.php?topic=52988    |
| `old_genesis_mod`            | Old Genesis Mod                      | 47.05eplus      | http://www.bay12forums.com/smf/index.php?topic=156059   |
| `fallout_equestria`          | Fallout Equestria                    | v0.30c beta     | http://www.bay12forums.com/smf/index.php?topic=118893   |
| `mushroom_kingdom`           | Rise of the Mushroom Kingdom         | 0.44            | http://www.bay12forums.com/smf/index.php?topic=110704   |
| `corrosion`                  | Corrosion                            | 1.1.13          | http://www.bay12forums.com/smf/index.php?topic=81068    |
| `succubus_dungeon`           | Succubus Dungeon                     | 17.1            | http://www.bay12forums.com/smf/index.php?topic=124135   |
| `spellcrafts`                | Spellcrafts Mod                      | 5zE             | http://www.bay12forums.com/smf/index.php?topic=149426   |
| `grimlockes_hist`            | Grimlocke's History & Realism Mods   | Rev 7b          | http://www.bay12forums.com/smf/index.php?topic=146737   |
| `underhive_settlement`       | Underhive Settlement                 | 1.6.1           | http://www.bay12forums.com/smf/index.php?topic=120494   |
| `highfantasy`                | Highfantasy Mod                      | v1.4.00s        | http://www.bay12forums.com/smf/index.php?topic=178840   |

## Regex searches

Here are some common Regex expressions that can be used to find the things you are looking for.

```javascript
// Search for all tokens that start with a lower case (should not be lowercase)
// Make sure to have `Match Casing` enabled
\[[a-z]
// Check for tokens that start with `[CREATURE:` and have more then 1 argument
\[CREATURE:[^:]*:[^:]*\]
```

Command that might help when searching for particular tokens:

```bash
# Find token with any (or 0) amount of arguments
rg "\[OBJECT(:[^:]*)*\]"
# Find all uniq arguments of 2nd value of the token `[CATEGORY_KEY:`
rg "\[CATEGORY_KEY:([^:\]\[]*)\]" --no-filename --only-matching ./ | sed -nr 's/\[CATEGORY_KEY:([^:]*)\]/\1/p' | sort | uniq
# Find all 3rd value that are uniq
rg "\[BUILDING:[^:\]\[]*:([^:\]\[]*)\]" --no-filename --only-matching ./ | sed -nr 's/\[BUILDING:[^:\]\[]*:([^:]*)\]/\1/p' | sort | uniq
```

## License

This project is a collection of files that can only be used for research and are not created by us.
But created by the people in the community listed above. You should contact them/look at there
licenses if you want to use there work.

The `tools` folder for contains code/projects created by us and
licensed under the MIT or Apache 2.0 license.
