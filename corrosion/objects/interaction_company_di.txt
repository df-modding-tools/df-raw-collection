interaction_company

[OBJECT:INTERACTION]

[INTERACTION:INJECT]
	[I_TARGET:A:CORPSE]
		[IT_LOCATION:CONTEXT_ITEM]
		[IT_AFFECTED_CLASS:GENERAL_POISON]
		[IT_REQUIRES:FIT_FOR_ANIMATION]
		[IT_FORBIDDEN:NOT_LIVING]
		[IT_MANUAL_INPUT:corpse]
	[I_EFFECT:RESURRECT]
		[IE_TARGET:A]
		[IE_IMMEDIATE]
		[IE_ARENA_NAME:Infected by Company]
		[SYNDROME]
		[CE_FLASH_TILE:TILE:90:6:0:0:FREQUENCY:2000:1000:START:0]
			[CE_PHYS_ATT_CHANGE:STRENGTH:300:1000:TOUGHNESS:300:1000:START:0]
			[CE_SPEED_CHANGE:SPEED_PERC:60:START:0]
[CE_ADD_TAG:NO_AGING:NOT_LIVING:OPPOSED_TO_LIFE:EXTRAVISION:NOEXERT:NOPAIN:NOBREATHE:NOSTUN:NONAUSEA:NO_DIZZINESS:NO_FEVERS:NOEMOTION:PARALYZEIMMUNE:NOFEAR:NO_EAT:NO_DRINK:NO_SLEEP:NO_PHYS_ATT_GAIN:NO_PHYS_ATT_RUST:NOTHOUGHT:NO_THOUGHT_CENTER_FOR_MOVEMENT:NO_CONNECTIONS_FOR_MOVEMENT:START:0]
			[CE_REMOVE_TAG:HAS_BLOOD:TRANCES:LIKES_FIGHTING:MISCHIEVOUS:START:0]
				[CDI:ADV_NAME:Inject corpse with immortality sirum]

[INTERACTION:INJECT_SPEED]
	[I_SOURCE:CREATURE_ACTION]
	[I_TARGET:A:CREATURE]
		[IT_LOCATION:CONTEXT_CREATURE]
		[IT_MANUAL_INPUT:creature]
	[I_EFFECT:ADD_SYNDROME]
		[IE_TARGET:A]
		[IE_IMMEDIATE]
		[IE_ARENA_NAME:Speed]
			[SYNDROME]
				[CE_SPEED_CHANGE:SPEED_PERC:200:START:0:PEAK:1:END:3]
				[CE_SPEED_CHANGE:SPEED_PERC:30:START:10:PEAK:11:END:13]
				[CDI:ADV_NAME:Inject self with adrenaline]