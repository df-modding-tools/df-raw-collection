inorganic_boosters

[OBJECT:INORGANIC]

[INORGANIC:ADRENALINE]
	[STATE_NAME_ADJ:ALL:adrenaline]
	[DISPLAY_COLOR:2:0:1][TILE:'#']
	[SPEC_HEAT:8001]
	[BOILING_POINT:8000]
	[SYNDROME]
	[SYN_INHALED]
	[SYN_AFFECTED_CLASS:HUMAN]
		[SYN_NAME:adrenaline]
		[CE_DISPLAY_NAME:NAME:adrenalined:adrenalined:adrenalined:START:0:END:11200]
			[CE_SPEED_CHANGE:SPEED_PERC:150:START:0:PEAK:10:END:11200]
			[CE_NUMBNESS:SEV:500:PROB:100:START:0:PEAK:10:END:11200]
			[CE_ADD_TAG:NO_SLEEP:START:0:END:11200]
			[CE_REMOVE_TAG:NO_SLEEP:START:11201]

[INORGANIC:HIBERNATION_SHOT]
	[STATE_NAME_ADJ:ALL:hibernation shot]
	[DISPLAY_COLOR:2:0:1][TILE:'#']
	[SPEC_HEAT:8001]
	[BOILING_POINT:8000]
	[SYNDROME]
	[SYN_INHALED]
	[SYN_AFFECTED_CLASS:HUMAN]
		[SYN_NAME:hibernation shot]
		[CE_DISPLAY_NAME:NAME:hybernating:hybernating:hybernating:START:0:END:33600]
			[CE_DROWSINESS:SEV:1000:PROB:100:START:0:PEAK:10:END:33600]
			[CE_ADD_TAG:NO_EAT:NO_DRINK:NO_PHYS_ATT_RUST:START:0:END:33600]
			[CE_REMOVE_TAG:NO_EAT:NO_DRINK:NO_PHYS_ATT_RUST:START:33601]

[INORGANIC:VITAMIN]
	[STATE_NAME_ADJ:ALL:vitamin]
	[DISPLAY_COLOR:2:0:1][TILE:'#']
	[SPEC_HEAT:8001]
	[BOILING_POINT:8000]
	[SYNDROME]
	[SYN_INHALED]
	[SYN_AFFECTED_CLASS:HUMAN]
		[SYN_NAME:vitamin]
		[CE_DISPLAY_NAME:NAME:in good health:in good health:good health:START:0:END:33600]
			[CE_PHYS_ATT_CHANGE:STRENGTH:125:0:START:0:END:33600]
			[CE_PHYS_ATT_CHANGE:AGILITY:125:0:START:0:END:33600]
			[CE_PHYS_ATT_CHANGE:ENDURANCE:125:0:START:0:END:33600]
			[CE_PHYS_ATT_CHANGE:RECUPERATION:125:0:START:0:END:33600]
			[CE_PHYS_ATT_CHANGE:TOUGHNESS:125:0:START:0:END:33600]
			[CE_PHYS_ATT_CHANGE:DISEASE_RESISTANCE:125:0:START:0:END:33600]

[INORGANIC:APATHY_SHOT]
	[STATE_NAME_ADJ:ALL:apathy shot]
	[DISPLAY_COLOR:2:0:1][TILE:'#']
	[SPEC_HEAT:8001]
	[BOILING_POINT:8000]
	[SYNDROME]
	[SYN_INHALED]
	[SYN_AFFECTED_CLASS:HUMAN]
		[SYN_NAME:apathy]
		[CE_DISPLAY_NAME:NAME:apathetic:apathetic:apathetic:START:0:END:33600]
			[CE_NUMBNESS:SEV:500:PROB:100:START:0:END:33600]

[INORGANIC:RAVING_FRENZY_PILL]
	[STATE_NAME_ADJ:ALL:raving frenzy pill]
	[DISPLAY_COLOR:2:0:1][TILE:'#']
	[SPEC_HEAT:8001]
	[BOILING_POINT:8000]
	[SYNDROME]
	[SYN_INHALED]
	[SYN_AFFECTED_CLASS:HUMAN]
		[SYN_NAME:raving frenzy]
		[CE_DISPLAY_NAME:NAME:raving frenzy:raving frenzy:raving frenzy:START:0:END:11200]
			[CE_NUMBNESS:SEV:1000:PROB:100:START:0:END:33600]
			[CE_PHYS_ATT_CHANGE:STRENGTH:500:0:START:0:END:33600]
			[CE_PHYS_ATT_CHANGE:AGILITY:500:0:START:0:END:33600]
			[CE_PHYS_ATT_CHANGE:ENDURANCE:500:0:START:0:END:33600]									[CE_PHYS_ATT_CHANGE:RECUPERATION:500:0:START:0:END:33600]
			[CE_PHYS_ATT_CHANGE:TOUGHNESS:500:0:START:0:END:33600]
			[CE_PHYS_ATT_CHANGE:DISEASE_RESISTANCE:500:0:START:0:END:33600]
			[CE_SPEED_CHANGE:SPEED_PERC:300:START:0:PEAK:10:END:33600]
			[CE_ADD_TAG:NO_DRINK:NO_EAT:NO_SLEEP:CRAZED:START:0:END:33600]
			[CE_MATERIAL_FORCE_MULTIPLIER:MAT_MULT:NONE:NONE:1:10]