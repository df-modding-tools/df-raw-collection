item_zHistoric_AnA_Legacy

[OBJECT:ITEM]
╔═╗ ╔╗  ╦ ╔╦╗ ╦   ╔═╗ ╔═╗ ║╔═ ╔═╗ ╗ ╔═╗   ╗ ╔ ╦ ╔═╗ ╔╦╗ ╔═╗ ╔╗  ╦ ╔═╗   ╔═╗ ╔╗  ╔╦╗ ╔═╗    ╔╗   ╔═╗ ╔╗  ╔╦╗ ╔═╗ ╔╗ 
║╠╗ ╠╩╗ ║ ║║║ ║   ║ ║ ║   ╠╩╗ ╠╣    ╚═╗   ╠═╣ ║ ╚═╗  ║  ║ ║ ╠╩╗ ║ ║     ╠═╣ ╠╩╗ ║║║ ╚═╗   ╔╩╣   ╠═╣ ╠╩╗ ║║║ ║ ║ ╠╩╗
╚═╝ ║ ║ ╩ ║╩║ ╚═╝ ╚═╝ ╚═╝ ║ ║ ╚═╝   ╚═╝   ╝ ╚ ╩ ╚═╝  ║  ╚═╝ ║ ║ ╩ ╚═╝   ║ ║ ║ ║ ║╩║ ╚═╝   ╚═╬   ║ ║ ║ ║ ║╩║ ╚═╝ ║ ║
Legacy weapons and armor. Anachronisms amoung the rest of the equipment, entirely superfluous or just likely not every having existed.

Bay12 forum thread: http://www.bay12forums.com/smf/index.php?topic=146737.0


<><><><><><><><>    Weapons    <><><><><><><><>

[ITEM_WEAPON:ITEM_WEAPON_SWORD_FALCHION_BROAD]
	[NAME:cleaver falchion:cleaver falchions]
	[SIZE:145]
	[SKILL:SWORD]
	[TWO_HANDED:42500]
	[MINIMUM_SIZE:42500]
	[MATERIAL_SIZE:4]
	[ATTACK:EDGE:250:10500:slash:slashes:NO_SUB:3000]
		[ATTACK_PREPARE_AND_RECOVER:5:1]
	[ATTACK:EDGE:250:10500:slash:slashes:NO_SUB:3000]
		[ATTACK_PREPARE_AND_RECOVER:5:1]
	[ATTACK:EDGE:8:800:stab:stabs:NO_SUB:900]
		[ATTACK_PREPARE_AND_RECOVER:4:1]
	[ATTACK:BLUNT:10:0:grab your with blade both hands and strike:grabs his blade with both hands and strikes:crossguard:1100]
		[ATTACK_PREPARE_AND_RECOVER:5:3]
		[ATTACK_FLAG_BAD_MULTIATTACK]
	[ATTACK:BLUNT:10:0:bash:bashes:crossguard:900]
		[ATTACK_PREPARE_AND_RECOVER:5:1]
	[ATTACK:BLUNT:30:0:strike:strikes:pommel:2000]
		[ATTACK_PREPARE_AND_RECOVER:4:1]

[ITEM_WEAPON:ITEM_WEAPON_AXE_BATTLE_NARROW]
	[NAME:light battle axe:light battle axes]
	[SIZE:95]
	[SKILL:AXE]
	[TWO_HANDED:32500]
	[MINIMUM_SIZE:25000]
	[CAN_STONE]
	[MATERIAL_SIZE:2]
	[ATTACK:EDGE:25:6000:hack:hacks:NO_SUB:1000]
		[ATTACK_PREPARE_AND_RECOVER:5:1]
	[ATTACK:EDGE:11:800:spike:spikes:back:1050]
		[ATTACK_PREPARE_AND_RECOVER:5:1]
	[ATTACK:BLUNT:30:0:prod:prods:NO_SUB:1200]
		[ATTACK_PREPARE_AND_RECOVER:4:1]
	[ATTACK:BLUNT:55:0:bash:bashes:knob:2550]
		[ATTACK_PREPARE_AND_RECOVER:4:1]
		
[ITEM_WEAPON:ITEM_WEAPON_AXE_BEARD]
	[NAME:bearded axe:bearded axes]
	[SIZE:210]
	[SKILL:AXE]
	[TWO_HANDED:152500]
	[MINIMUM_SIZE:42500]
	[CAN_STONE]
	[MATERIAL_SIZE:2]
	[ATTACK:EDGE:65:12500:hack:hacks:NO_SUB:3000]
		[ATTACK_PREPARE_AND_RECOVER:3:4]
	[ATTACK:BLUNT:45:0:bash:bashes:back:4000]
		[ATTACK_PREPARE_AND_RECOVER:3:4]
	[ATTACK:BLUNT:30:0:prod:prods:NO_SUB:2000]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
	[ATTACK:BLUNT:120:0:bash:bashes:shaft:3000]
		[ATTACK_PREPARE_AND_RECOVER:4:2]
	[ATTACK:BLUNT:55:0:bash:bashes:knob:3000]
		[ATTACK_PREPARE_AND_RECOVER:5:1]

[ITEM_WEAPON:ITEM_WEAPON_POLEARM_VOULGE]
	[NAME:voulge:voulges]
	[SIZE:480]
	[SKILL:PIKE]
	[TWO_HANDED:230000]
	[MINIMUM_SIZE:52500]
	[MATERIAL_SIZE:4]
	[ATTACK:EDGE:155:15500:hack:hacks:NO_SUB:5300]
		[ATTACK_PREPARE_AND_RECOVER:1:7]
	[ATTACK:EDGE:11:1200:spike:spikes:back:1400]
		[ATTACK_PREPARE_AND_RECOVER:1:7]
	[ATTACK:EDGE:16:1500:stab:stabs:NO_SUB:1400]
		[ATTACK_PREPARE_AND_RECOVER:1:5]
	[ATTACK:BLUNT:120:0:bash:bashes:shaft:3000]
		[ATTACK_PREPARE_AND_RECOVER:3:2]
	[ATTACK:BLUNT:55:0:strike:strikes:butt:3000]
		[ATTACK_PREPARE_AND_RECOVER:2:3]
	
	
<><><><><><><><>   Ammo   <><><><><><><><>

[ITEM_AMMO:ITEM_AMMO_ARROWS_LEAF]
	[NAME:leaf arrow:leaf arrows]
	[CLASS:ARROW]
	[SIZE:70]
	[ATTACK:EDGE:18:7000:stab:stabs:NO_SUB:400]

[ITEM_AMMO:ITEM_AMMO_ARROWS_TREFOIL]
	[NAME:trefoil arrow:trefoil arrows]
	[CLASS:ARROW]
	[SIZE:70]
	[ATTACK:EDGE:14:6000:stab:stabs:NO_SUB:400]
		
		
<><><><><><><><>   Armor   <><><><><><><><>
		
[ITEM_GLOVES:ITEM_GLOVES_MITTENS_PADDED]
	[NAME:padded mitten:padded mittens]
	[ARMORLEVEL:1]
	[UPSTEP:0]
	[LAYER:ARMOR]
	[COVERAGE:1250]15000
	[LAYER_SIZE:12]
	[LAYER_PERMIT:2]
	[MATERIAL_SIZE:6]
	[SOFT]
	[STRUCTURAL_ELASTICITY_CHAIN_METAL]
	
[ITEM_SHOES:ITEM_SHOES_BOOTS_PADDED]
	[NAME:padded boot:padded boots]
	[ARMORLEVEL:1]
	[UPSTEP:0]
	[LAYER:ARMOR]
	[COVERAGE:1250]15000
	[LAYER_SIZE:12]
	[LAYER_PERMIT:2]
	[MATERIAL_SIZE:6]
	[SOFT]
	[STRUCTURAL_ELASTICITY_CHAIN_METAL]