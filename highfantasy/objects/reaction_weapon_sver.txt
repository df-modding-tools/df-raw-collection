reaction_weapon_sver

[OBJECT:REACTION]


	The reactions to make certain weapons more expensive for the player or to work around certain other issues.
	Strongly recommended for better game balance.


===========================================================
hand ballista
===========================================================

[REACTION:BRONZE_HAND_BALLISTA_SVER]
[NAME:assemble bronze hand ballista]
[BUILDING:METALSMITH:NONE]
[REAGENT:metal:900:BAR:NO_SUBTYPE:METAL:BRONZE] 6 bars
[REAGENT:mechanisms:4:TRAPPARTS:NO_SUBTYPE:METAL:BRONZE]
[REAGENT:part:3:BALLISTAPARTS:NONE:NONE:NONE]
[REAGENT:thread:90000:THREAD:NONE:NONE:NONE][NOT_WEB] 6 units of thread
[PRODUCT:100:1:WEAPON:ITEM_WEAPON_HAND_BALLISTA_SVER:METAL:BRONZE]
[FUEL]
[SKILL:SIEGECRAFT]
[CATEGORY:HAND_BALLISTA_SVER]
[CATEGORY_NAME:Assemble a hand ballista]
[CATEGORY_DESCRIPTION:Requires 3 ballista parts, 6 units of thread, 6 bars of metal and 4 mechanisms of the same metal.]

===========================================================

[REACTION:BISMUTH_BRONZE_HAND_BALLISTA_SVER]
[NAME:assemble bismuth bronze hand ballista]
[BUILDING:METALSMITH:NONE]
[REAGENT:metal:900:BAR:NO_SUBTYPE:METAL:BISMUTH_BRONZE]
[REAGENT:mechanisms:4:TRAPPARTS:NO_SUBTYPE:METAL:BISMUTH_BRONZE]
[REAGENT:part:3:BALLISTAPARTS:NONE:NONE:NONE]
[REAGENT:thread:90000:THREAD:NONE:NONE:NONE][NOT_WEB]
[PRODUCT:100:1:WEAPON:ITEM_WEAPON_HAND_BALLISTA_SVER:METAL:BISMUTH_BRONZE]
[FUEL]
[SKILL:SIEGECRAFT]
[CATEGORY:HAND_BALLISTA_SVER]

===========================================================

[REACTION:IRON_HAND_BALLISTA_SVER]
[NAME:assemble iron hand ballista]
[BUILDING:METALSMITH:NONE]
[REAGENT:metal:900:BAR:NO_SUBTYPE:METAL:IRON]
[REAGENT:mechanisms:4:TRAPPARTS:NO_SUBTYPE:METAL:IRON]
[REAGENT:part:3:BALLISTAPARTS:NONE:NONE:NONE]
[REAGENT:thread:90000:THREAD:NONE:NONE:NONE][NOT_WEB]
[PRODUCT:100:1:WEAPON:ITEM_WEAPON_HAND_BALLISTA_SVER:METAL:IRON]
[FUEL]
[SKILL:SIEGECRAFT]
[CATEGORY:HAND_BALLISTA_SVER]


===========================================================

[REACTION:STEEL_HAND_BALLISTA_SVER]
[NAME:assemble steel hand ballista]
[BUILDING:METALSMITH:NONE]
[REAGENT:metal:900:BAR:NO_SUBTYPE:METAL:STEEL]
[REAGENT:mechanisms:4:TRAPPARTS:NO_SUBTYPE:METAL:STEEL]
[REAGENT:part:3:BALLISTAPARTS:NONE:NONE:NONE]
[REAGENT:thread:90000:THREAD:NONE:NONE:NONE][NOT_WEB]
[PRODUCT:100:1:WEAPON:ITEM_WEAPON_HAND_BALLISTA_SVER:METAL:STEEL]
[FUEL]
[SKILL:SIEGECRAFT]
[CATEGORY:HAND_BALLISTA_SVER]

===========================================================
large dagger
===========================================================

[REACTION:COPPER_LARGE_DAGGER_SVER]
[NAME:forge large copper dagger]
[BUILDING:METALSMITH:NONE]
[REAGENT:metal:900:BAR:NO_SUBTYPE:METAL:COPPER] 6 bars
[PRODUCT:100:1:WEAPON:ITEM_WEAPON_DAGGER_LARGE:METAL:COPPER]
[FUEL]
[SKILL:FORGE_WEAPON]
[CATEGORY:DAGGER_SVER]
[CATEGORY_NAME:Forge a dagger]
[CATEGORY_DESCRIPTION:Requires fuel and 2 bars of metal.]

===========================================================

[REACTION:BRONZE_LARGE_DAGGER_SVER]
[NAME:forge large bronze dagger]
[BUILDING:METALSMITH:NONE]
[REAGENT:metal:900:BAR:NO_SUBTYPE:METAL:BRONZE]
[PRODUCT:100:1:WEAPON:ITEM_WEAPON_DAGGER_LARGE:METAL:BRONZE]
[FUEL]
[SKILL:FORGE_WEAPON]
[CATEGORY:DAGGER_SVER]

===========================================================

[REACTION:BISMUTH_BRONZE_LARGE_DAGGER_SVER]
[NAME:forge large bismuth bronze dagger]
[BUILDING:METALSMITH:NONE]
[REAGENT:metal:900:BAR:NO_SUBTYPE:METAL:BISMUTH_BRONZE]
[PRODUCT:100:1:WEAPON:ITEM_WEAPON_DAGGER_LARGE:METAL:BISMUTH_BRONZE]
[FUEL]
[SKILL:FORGE_WEAPON]
[CATEGORY:DAGGER_SVER]

===========================================================

[REACTION:IRON_LARGE_DAGGER_SVER]
[NAME:forge large iron dagger]
[BUILDING:METALSMITH:NONE]
[REAGENT:metal:900:BAR:NO_SUBTYPE:METAL:IRON]
[PRODUCT:100:1:WEAPON:ITEM_WEAPON_DAGGER_LARGE:METAL:IRON]
[FUEL]
[SKILL:FORGE_WEAPON]
[CATEGORY:DAGGER_SVER]

===========================================================

[REACTION:STEEL_LARGE_DAGGER_SVER]
[NAME:forge large steel dagger]
[BUILDING:METALSMITH:NONE]
[REAGENT:metal:900:BAR:NO_SUBTYPE:METAL:STEEL]
[PRODUCT:100:1:WEAPON:ITEM_WEAPON_DAGGER_LARGE:METAL:STEEL]
[FUEL]
[SKILL:FORGE_WEAPON]
[CATEGORY:DAGGER_SVER]

===========================================================
parrying dagger
===========================================================

[REACTION:COPPER_PARRYING_DAGGER_SVER]
[NAME:forge copper parrying dagger]
[BUILDING:METALSMITH:NONE]
[REAGENT:metal:300:BAR:NO_SUBTYPE:METAL:COPPER] 2 bars
[PRODUCT:100:1:WEAPON:ITEM_WEAPON_PARRYING_DAGGER_SVER:METAL:COPPER]
[FUEL]
[SKILL:FORGE_WEAPON]
[CATEGORY:DAGGER_SVER]

===========================================================

[REACTION:BRONZE_PARRYING_DAGGER_SVER]
[NAME:forge bronze parrying dagger]
[BUILDING:METALSMITH:NONE]
[REAGENT:metal:300:BAR:NO_SUBTYPE:METAL:BRONZE]
[PRODUCT:100:1:WEAPON:ITEM_WEAPON_PARRYING_DAGGER_SVER:METAL:BRONZE]
[FUEL]
[SKILL:FORGE_WEAPON]
[CATEGORY:DAGGER_SVER]

===========================================================

[REACTION:BISMUTH_BRONZE_PARRYING_DAGGER_SVER]
[NAME:forge bismuth bronze parrying dagger]
[BUILDING:METALSMITH:NONE]
[REAGENT:metal:300:BAR:NO_SUBTYPE:METAL:BISMUTH_BRONZE]
[PRODUCT:100:1:WEAPON:ITEM_WEAPON_PARRYING_DAGGER_SVER:METAL:BISMUTH_BRONZE]
[FUEL]
[SKILL:FORGE_WEAPON]
[CATEGORY:DAGGER_SVER]

===========================================================

[REACTION:IRON_PARRYING_DAGGER_SVER]
[NAME:forge iron parrying dagger]
[BUILDING:METALSMITH:NONE]
[REAGENT:metal:300:BAR:NO_SUBTYPE:METAL:IRON]
[PRODUCT:100:1:WEAPON:ITEM_WEAPON_PARRYING_DAGGER_SVER:METAL:IRON]
[FUEL]
[SKILL:FORGE_WEAPON]
[CATEGORY:DAGGER_SVER]

===========================================================

[REACTION:STEEL_PARRYING_DAGGER_SVER]
[NAME:forge steel parrying dagger]
[BUILDING:METALSMITH:NONE]
[REAGENT:metal:300:BAR:NO_SUBTYPE:METAL:STEEL]
[PRODUCT:100:1:WEAPON:ITEM_WEAPON_PARRYING_DAGGER_SVER:METAL:STEEL]
[FUEL]
[SKILL:FORGE_WEAPON]
[CATEGORY:DAGGER_SVER]

===========================================================