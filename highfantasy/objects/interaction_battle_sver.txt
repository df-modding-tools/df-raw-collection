interaction_battle_sver

[OBJECT:INTERACTION]

=============================================================

Adrenaline by Grimlocke.
Makes pain and fatigue matter less, thus, making the combat longer in general.

[INTERACTION:BATTLE_FERVOUR_GRIMLOCKE] Sver: some minor tweaks
	[I_SOURCE:CREATURE_ACTION]
	[I_TARGET:A:CREATURE]
		[IT_LOCATION:CONTEXT_CREATURE]
		[IT_REQUIRES:HAS_BLOOD]
		[IT_FORBIDDEN:NOTHOUGHT]
		[IT_FORBIDDEN:NOEMOTION]
		[IT_FORBIDDEN:NOFEAR]
		[IT_FORBIDDEN:NOEXERT]
		[IT_FORBIDDEN:NO_SLEEP]
		[IT_CANNOT_TARGET_IF_ALREADY_AFFECTED]
		[IT_MANUAL_INPUT:any living creature]
	[I_EFFECT:ADD_SYNDROME]
		[IE_TARGET:A]
		[SYNDROME]
			[SYN_NAME:battle fervour]
			[SYN_NO_HOSPITAL]
			[CE_MENT_ATT_CHANGE:WILLPOWER:200:3000:START:0:END:320:DWF_STRETCH:2]
			[CE_PHYS_ATT_CHANGE:ENDURANCE:150:1500:START:0:END:320:DWF_STRETCH:2]
			--CE_FLASH_TILE:TILE:'!':5:0:1:FREQUENCY:1300:100:START:0:END:320:DWF_STRETCH:2] For testing purposes.
			
			
			--CE_DROWSINESS:SEV:30000:PROB:100:START:640:PEAK:650:END:1250] Work in progress.
			--CE_FLASH_TILE:TILE:'!':6:0:1:FREQUENCY:1300:100:START:640:END:1250] For testing purposes.

=============================================================