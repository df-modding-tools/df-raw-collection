Honestly, don't use these.

These texture changes involve turning sands into "a" and gems into "-", among others.
They were copied from one of the baseline mods in HF from v1.0 on,
but they never really added anything and no one liked them.

Thanks to Ramiel. for finally correcting these files.