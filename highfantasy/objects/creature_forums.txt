creature_forums

[OBJECT:CREATURE]

[CREATURE:HYDLING]
 	[DESCRIPTION:A seven-headed small hairy thing, about the size of a dog. It is very loyal to its masters, and will promptly disembowel any enemy straying too close.]
 	This is the description that shows up in-game when viewing the creature.
 	[NAME:hydling:hydlings:hydlish] If there were a civ made of hydlings, it would appear as "hydlings" in the neighbors screen.
 	[CASTE_NAME:hydling:hydlings:hydling]
 	[CREATURE_TILE:'ð'][COLOR:2:0:1] Will appear as a light green "=".
 	[PETVALUE:78][NATURAL] Creature is known to be naturally occurring by the game. Will cost 40 embark points to buy.
 	[LARGE_ROAMING] Will spawn outdoors, wandering around.
- SI -	[BIOME:MOUNTAIN] everyone doesn't just get this automatically
-SI-	[DIFFICULTY:2] - giant creature or simple deadly creature
- SI -  [SAVAGE]	This is a non-mundane, non-intelligent, non-megabeast creature
 	(domestic removed)[TRAINABLE_WAR][PET_EXOTIC] - SI - 	creatures with fire are bad for hunting
 	[BONECARN] Can eat meat and bones only--no vegetables.
 	[PREFSTRING:personable heads] Dwarves will like it for its loyalty.
 	[LARGE_PREDATOR] Will attack rather than flee.
	[MULTIPART_FULL_VISION] - SI -
 	[BODY:BASIC_2PARTBODY:7HEADNECKS:BASIC_FRONTLEGS:BASIC_REARLEGS:TAIL:2EYES:NOSE:2LUNGS:HEART:GUTS:ORGANS:THROAT:SPINE:BRAIN:SKULL:3TOES_FQ_REG:3TOES_RQ_REG:MOUTH:TONGUE:GENERIC_TEETH_WITH_FANGS:RIBCAGE]
 	Has a lower body, upper body, 4 legs, a tail, ten eyes, ten ears, five noses, two lungs, a heart, guts, a pancreas etc., and 5 heads with all that goes with those.
 	[BODYGLOSS:PAW] Feet will be called "paws"
 	[BODY_DETAIL_PLAN:STANDARD_MATERIALS] Declares the standard materials that most creatures' tissues are made of.
 	[BODY_DETAIL_PLAN:STANDARD_TISSUES] This declares the tissues that the creature's tissue layers are made of.
 	[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SKIN:FAT:MUSCLE:BONE:CARTILAGE] And this describes the tissue layers that the creature is made of.
 	[BODY_DETAIL_PLAN:BODY_HAIR_TISSUE_LAYERS:HAIR] Creature will be covered with a layer of fur.
 	[USE_MATERIAL_TEMPLATE:NAIL:NAIL_TEMPLATE] And it'll have nails.
 	[USE_TISSUE_TEMPLATE:NAIL:NAIL_TEMPLATE]
 	[TISSUE_LAYER:BY_CATEGORY:TOE:NAIL:FRONT] On the toe, specifically.
 	[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
 	 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
 		[TL_MAJOR_ARTERIES] Heart and throat--called above--will cause heavy bleeding if ruptured.
 	[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS] Places eyes, ears and what-have-you into their correct placement, so that you don't have people punching out eyes from behind.
 	[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS] Sets the ribcage as being around lungs and heart.
 	[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE] Defines sinew so that...
 	[TENDONS:LOCAL_CREATURE_MAT:SINEW:200] Tendons...
 	[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200] ...And ligaments can be defined.
 	[HAS_NERVES] Creature has nerves, and as such can be disabled by severing them.
 	[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE] Defines the material BLOOD using the template BLOOD_TEMPLATE.
 	[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID] Defines the creature's BLOOD as being made of the above-defined BLOOD material in a LIQUID state.
 	[CREATURE_CLASS:GENERAL_POISON] Creature can be affected by syndromes that affect GENERAL_POISON.
 	[GETS_WOUND_INFECTIONS] Pretty much self-explanatory. Creature can get infected from wounds.
 	[GETS_INFECTIONS_FROM_ROT] And from necrosis.
 	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE] Defines PUS using PUS_TEMPLATE.
 	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID] Defines PUS as being made of PUS.
 	[BODY_SIZE:0:0:1000] Creature will be 1000 cubic centimeters at birth...
 	[BODY_SIZE:1:0:12500] 12500 cubic centimeters at 1 year old...
 	[BODY_SIZE:2:0:30000] and 30000 cubic centimeters at 2.
 	[BODY_APPEARANCE_MODIFIER:LENGTH:90:95:98:100:102:105:110] Creature can be anywhere from 90% to 110% as long as others.
 	[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110] As above, but with height.
 	[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110] As above, but with broadness. This puts the minimum size of the creature (when fully grown) at 21870 and the maximum size at 39930.
 	[MAXAGE:20:30] Creature will die of old age between the ages of 20 and 30, no later than 30, no sooner than 20.
 	[CAN_DO_INTERACTION:MATERIAL_EMISSION] Creature can use the MATERIAL_EMISSION interaction.
 		[CDI:ADV_NAME:Hurl fireball] In adventurer mode, the MATERIAL_EMISSION interaction will appear as "Hurl fireball".
 		[CDI:USAGE_HINT:ATTACK] Creature will use MATERIAL_EMISSION when it's attacking, on creatures that it's attacking.
 		[CDI:BP_REQUIRED:BY_CATEGORY:HEAD] Creature must have at least one HEAD to use MATERIAL_EMISSION.
 		[CDI:FLOW:FIREBALL] The MATERIAL_EMISSION will shoot a fireball.
 		[CDI:TARGET:C:LINE_OF_SIGHT] The target for the emission--a location--must be within the line of sight of the Hydling.
 		[CDI:TARGET_RANGE:C:15] And must be, at most, 15 tiles away.
 		[CDI:MAX_TARGET_NUMBER:C:1] The hydling can only shoot at one target at a time...
 		[CDI:WAIT_PERIOD:30] and only every 30 ticks (3 tenths of a second at 100 FPS)
 	[ATTACK:BITE:CHILD_BODYPART_GROUP:BY_CATEGORY:HEAD:BY_CATEGORY:TOOTH] Defines a BITE attack that uses teeth.
 		[ATTACK_SKILL:BITE] Attack uses the BITE skill.
 		[ATTACK_VERB:crunch:crunches] "The Hydling noms the Elf in the left first toe, tearing the muscle!"
 		[ATTACK_CONTACT_PERC:100] Will use all of the tooth. Note that this can be more than 100.
 		[ATTACK_PENETRATION_PERC:100] Will sink the tooth all the way in. This can also be more than 100.
 		[ATTACK_FLAG_EDGE] Attack is an EDGE attack.
 		[ATTACK_PRIORITY:MAIN] Attack is of priority MAIN. Other option is SECOND.
 		[ATTACK_FLAG_CANLATCH] Attack can latch on.
                [ATTACK_PREPARE_AND_RECOVER:3:3] Takes 3 ticks to wind up attack and 3 to recover from it.
                [ATTACK_FLAG_INDEPENDENT_MULTIATTACK] Can use each head independently.
 	[ATTACK:SCRATCH:CHILD_TISSUE_LAYER_GROUP:BY_TYPE:STANCE:BY_CATEGORY:ALL:NAIL] As above, but for nail instead of teeth.
 		[ATTACK_SKILL:STANCE_STRIKE] Uses the kicking skill.
 		[ATTACK_VERB:needle:needles] "You slice the Elf in the left foot and the severed part sails off in an arc!"
 		[ATTACK_CONTACT_PERC:100] Uses the whole nail.
 		[ATTACK_PENETRATION_PERC:100] The whole nail goes in.
 		[ATTACK_FLAG_EDGE] Attack is an edge attack.
                [ATTACK_PREPARE_AND_RECOVER:3:3]
 		[ATTACK_PRIORITY:SECOND]
 	[CHILD:1] Hydling will become an adult at 1 year old.
	[GENERAL_BABY_NAME:hydling:hydlings] 
 	[GENERAL_CHILD_NAME:hydling:hydlings] 
 	[DIURNAL] Is active during the daytime.
 	[HOMEOTHERM:10070] Has a body temperature of 102 Fahrenheit.
 	[APPLY_CREATURE_VARIATION:STANDARD_QUADRUPED_GAITS:900:730:561:351:1900:2900] Can run at 25 kph
 	[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:3512:2634:1756:878:4900:6900] Can swim at 10 kph
 	[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:6561:6115:5683:1755:7456:8567] Can crawl at 5 kph
 	[SWIMS_INNATE]Swims innately.
 	[CASTE:FEMALE] Defines a caste called FEMALE.
 		[FEMALE] FEMALE caste is female.
 	[CASTE:MALE] As above, but with male.
 		[MALE] See above.

Phoenix by IAmTheMadLord

[CREATURE:PHOENIX] https://www.reddit.com/r/dwarffortress/comments/54x3bt/raws_for_a_phoenix/

[DESCRIPTION:A bird of blazing fire. All who see it are stunned by its intense glare. Its constantly burning feathers are known to burn down wooden structures.]

[NAME:phoenix:phoenixs:phoenix]

[CASTE_NAME:phoenix:phoenixes:phoenix]

[GENERAL_CHILD_NAME:phoenix chick:phoenix chicks]

[CREATURE_TILE:'R'][COLOR:128:0:0]

[PETVALUE:10000]
 
[PET_EXOTIC]
 
[TRAINABLE] (but good luck with a permanently on-fire bird in your fortress)
 
[BIOME:MOUNTAIN]
 
[FREQUENCY:5]
 
[FANCIFUL]
 
[MEGABEAST][DIFFICULTY:10]
 
    [ATTACK_TRIGGER:80:10000:100000]
 
[SPHERE:SKY]
 
[SPHERE:FIRE]
 
[SPHERE:VOLCANOS]
 
[IMMOLATE]
 
[FIREIMMUNE_SUPER]
 
(CURIOUSBEAST_ITEM)
 
(CURIOUSBEAST_EATER)
 
[NATURAL]
 
[LARGE_PREDATOR]
 
[FLIER]
 
(BONECARN)
 
[CHILD:1]
 
[ALL_ACTIVE]
 
[FIXED_TEMP:20000]
 
[LAIR:WILDERNESS_LOCATION:100]
 
[HABIT_NUM:TEST_ALL]
 
[HABIT:GIANT_NEST:100]
 
[NATURAL_SKILL:BITE:9]
 
[NATURAL_SKILL:STANCE_STRIKE:9]
 
[NATURAL_SKILL:MELEE_COMBAT:9]
 
[NATURAL_SKILL:DODGING:9]
 
[NATURAL_SKILL:SITUATIONAL_AWARENESS:9]
 
[APPLY_CREATURE_VARIATION:STANDARD_FLYING_GAITS:900:528:352:176:1900:2900] 50 kph
 
[APPLY_CREATURE_VARIATION:STANDARD_WALKING_GAITS:3512:2634:1756:878:4900:6900] 10 kph
 
[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:6561:6115:5683:1755:7456:8567] 5 kph
 
[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:6561:6115:5683:1755:7456:8567] 5 kph
 
[SWIMS_INNATE]
 
[PREFSTRING:unending flame]
 
[PREFSTRING:destructive tendencies]
 
[BODY:HUMANOID_ARMLESS_NECK:2WINGS:2EYES:2LUNGS:HEART:GUTS:ORGANS:GIZZARD:HUMANOID_JOINTS:THROAT:NECK:SPINE:BRAIN:SKULL:4TOES:BEAK:TONGUE:RIBCAGE]
 
[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
 
    [REMOVE_MATERIAL:HAIR]
 
    [USE_MATERIAL_TEMPLATE:FEATHER:FEATHER_TEMPLATE]
 
[BODY_DETAIL_PLAN:STANDARD_TISSUES]
 
    [REMOVE_TISSUE:HAIR]
 
    [USE_TISSUE_TEMPLATE:FEATHER:FEATHER_TEMPLATE]
 
[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SKIN:FAT:MUSCLE:BONE:CARTILAGE]
 
[BODY_DETAIL_PLAN:BODY_FEATHER_TISSUE_LAYERS:FEATHER]
 
[USE_MATERIAL_TEMPLATE:TALON:NAIL_TEMPLATE]
 
[USE_TISSUE_TEMPLATE:TALON:TALON_TEMPLATE]
 
[TISSUE_LAYER:BY_CATEGORY:TOE:TALON:FRONT]
 
[BODY_DETAIL_PLAN:EGG_MATERIALS]
 
[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
 
 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
 
    [TL_MAJOR_ARTERIES]
 
[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
 
[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
 
[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
 
[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
 
[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
 
[HAS_NERVES]
 
[EXTRA_BUTCHER_OBJECT:BY_CATEGORY:GIZZARD]
 
    [EBO_ITEM:SMALLGEM:NONE:ANY_HARD_STONE]
 
    [EBO_SHAPE:GIZZARD_STONE]
 
[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
 
[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
 
[CREATURE_CLASS:GENERAL_POISON]
 
[GETS_WOUND_INFECTIONS]
 
[GETS_INFECTIONS_FROM_ROT]
 
[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
 
[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
 
[BODY_SIZE:0:0:140]
 
[BODY_SIZE:1:0:4000]	Size of an eagle at 1 year
 
[BODY_SIZE:25:0:1500000] Size of a megabeast, but not until it's been alive a very long time
 
[BODY_APPEARANCE_MODIFIER:LENGTH:90:95:98:100:102:105:110]
 
[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
 
[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
 
[ATTACK:BITE:BODYPART:BY_CATEGORY:BEAK]
 
    [ATTACK_SKILL:BITE]
 
    [ATTACK_VERB:bite:bites]
 
    [ATTACK_CONTACT_PERC:100]
 
    [ATTACK_PENETRATION_PERC:100]
 
    [ATTACK_FLAG_EDGE]
 
    [ATTACK_PREPARE_AND_RECOVER:3:3]
 
    [ATTACK_PRIORITY:MAIN]
 
    [ATTACK_FLAG_CANLATCH]
 
[ATTACK:SCRATCH:CHILD_TISSUE_LAYER_GROUP:BY_TYPE:STANCE:BY_CATEGORY:ALL:TALON]
 
    [ATTACK_SKILL:STANCE_STRIKE]
 
    [ATTACK_VERB:snatch at:snatches at]
 
    [ATTACK_CONTACT_PERC:100]
 
    [ATTACK_PENETRATION_PERC:100]
 
    [ATTACK_FLAG_EDGE]
 
    [ATTACK_PREPARE_AND_RECOVER:3:3]
 
    [ATTACK_PRIORITY:SECOND]
 
    [ATTACK_FLAG_WITH]
 
[CASTE:FEMALE]
 
    [FEMALE]
 
    [LAYS_EGGS]
 
        [EGG_MATERIAL:LOCAL_CREATURE_MAT:EGGSHELL:SOLID]
 
        [EGG_MATERIAL:LOCAL_CREATURE_MAT:EGG_WHITE:LIQUID]
 
        [EGG_MATERIAL:LOCAL_CREATURE_MAT:EGG_YOLK:LIQUID]
 
        [EGG_SIZE:201000]
 
        [CLUTCH_SIZE:1:2]
 
[CASTE:MALE]
 
    [MALE]
 
[SELECT_CASTE:ALL]
 
    [SET_TL_GROUP:BY_CATEGORY:ALL:FEATHER]
 
        [TL_COLOR_MODIFIER:RED:1]
 
            [TLCM_NOUN:ember feathers:PLURAL]
 
    [SET_TL_GROUP:BY_CATEGORY:ALL:SKIN]
 
        [TL_COLOR_MODIFIER:RED:1]
 
            [TLCM_NOUN:skin:SINGULAR]
 
    [SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
 
        [TL_COLOR_MODIFIER:BLACK:1]
 
            [TLCM_NOUN:eyes:PLURAL]
 
[SELECT_MATERIAL:ALL]
 
	[COLDDAM_POINT:NONE]
 
	[HEATDAM_POINT:NONE]
 
	[IGNITE_POINT:NONE]
 
	[IF_EXISTS_SET_MELTING_POINT:55000]
 
	[IF_EXISTS_SET_BOILING_POINT:57000]
 
	[SPEC_HEAT:30000]
 
    	[MULTIPLY_VALUE:25]
 
[SELECT_MATERIAL:BLOOD]
 
	 [PLUS_MATERIAL:PUS]
 
	 [MELTING_POINT:10000]

[CREATURE:SMOULDERING_CORPSE]
	[DESCRIPTION:Death is a temporary obstacle to success!]
	[NAME:smouldering corpse:smouldering corpses:smouldering corpse]
	[CASTE_NAME:smouldering corpse:smouldering corpses:smouldering corpse]
	[GENERAL_CHILD_NAME:smouldering corpse:smouldering corpses]
	[CREATURE_TILE:144][COLOR:4:0:1]
	[PETVALUE:30][NATURAL]
	PET no you don't
	No biome, prevents spawning
	[VERMIN_GROUNDER][FREQUENCY:100]
	[SMALL_REMAINS]
	[FLIER]
	[CHILD:1]
	[DIURNAL]
	[HOMEOTHERM:10071]
	[APPLY_CREATURE_VARIATION:STANDARD_FLYING_GAITS:900:471:314:157:1900:2900] 56 kph, need to work on base speed for flying
	[APPLY_CREATURE_VARIATION:STANDARD_WALKING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[APPLY_CREATURE_VARIATION:STANDARD_CLIMBING_GAITS:2990:2257:1525:731:4300:6100] 12 kph
	[STANCE_CLIMBER][NATURAL_SKILL:CLIMBING:15]
	[SWIMS_INNATE]
	[MUNDANE]
	[NOT_BUTCHERABLE]
	[BODY:HUMANOID_ARMLESS_NECK:2WINGS:2EYES:2LUNGS:HEART:GUTS:ORGANS:GIZZARD:HUMANOID_JOINTS:THROAT:NECK:SPINE:BRAIN:SKULL:4TOES:BEAK:TONGUE:RIBCAGE]
	[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
		[REMOVE_MATERIAL:HAIR]
		[USE_MATERIAL_TEMPLATE:FEATHER:FEATHER_TEMPLATE]
	[BODY_DETAIL_PLAN:STANDARD_TISSUES]
		[REMOVE_TISSUE:HAIR]
		[USE_TISSUE_TEMPLATE:FEATHER:FEATHER_TEMPLATE]
	[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SKIN:FAT:MUSCLE:BONE:CARTILAGE]
	[BODY_DETAIL_PLAN:BODY_FEATHER_TISSUE_LAYERS:FEATHER]
	[USE_MATERIAL_TEMPLATE:TALON:NAIL_TEMPLATE]
	[USE_TISSUE_TEMPLATE:TALON:TALON_TEMPLATE]
	[TISSUE_LAYER:BY_CATEGORY:TOE:TALON:FRONT]
	[BODY_DETAIL_PLAN:EGG_MATERIALS]
	[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
	 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
		[TL_MAJOR_ARTERIES]
	[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
	[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
	[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
	[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
	[HAS_NERVES]
	[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
	[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
	[CREATURE_CLASS:GENERAL_POISON]
	[GETS_WOUND_INFECTIONS]
	[GETS_INFECTIONS_FROM_ROT]
	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
	[BODY_SIZE:0:0:8]
	[BODY_SIZE:1:0:50]
	[BODY_APPEARANCE_MODIFIER:LENGTH:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
	[MAXAGE:10:20]
	[ATTACK:BITE:BODYPART:BY_CATEGORY:BEAK]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bite:bites]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PENETRATION_PERC:100]
		[ATTACK_FLAG_EDGE]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:MAIN]
		[ATTACK_FLAG_CANLATCH]
	[ATTACK:SCRATCH:CHILD_TISSUE_LAYER_GROUP:BY_TYPE:STANCE:BY_CATEGORY:ALL:TALON]
		[ATTACK_SKILL:STANCE_STRIKE]
		[ATTACK_VERB:snatch at:snatches at]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PENETRATION_PERC:100]
		[ATTACK_FLAG_EDGE]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:SECOND]
		[ATTACK_FLAG_WITH]
		[ATTACK_FLAG_BAD_MULTIATTACK]
	[CASTE:FEMALE]
		[FEMALE]
		[LAYS_EGGS]
			[EGG_MATERIAL:LOCAL_CREATURE_MAT:EGGSHELL:SOLID]
			[EGG_MATERIAL:LOCAL_CREATURE_MAT:EGG_WHITE:LIQUID]
			[EGG_MATERIAL:LOCAL_CREATURE_MAT:EGG_YOLK:LIQUID]
			[EGG_SIZE:9]
			[CLUTCH_SIZE:2:5]
	[CASTE:MALE]
		[MALE]
	[SELECT_CASTE:ALL]
		[SET_TL_GROUP:BY_CATEGORY:ALL:FEATHER]
			[TL_COLOR_MODIFIER:CARDINAL:1]
				[TLCM_NOUN:feathers:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SKIN]
	[TL_COLOR_MODIFIER:BROWN:1:BURNT_UMBER:1:CINNAMON:1:COPPER:1:DARK_BROWN:1:DARK_PEACH:1:DARK_TAN:1:ECRU:1:PALE_BROWN:1:PALE_CHESTNUT:1:PALE_PINK:1:PEACH:1:PINK:1:RAW_UMBER:1:SEPIA:1:TAN:1:TAUPE_PALE:1:TAUPE_SANDY:1]
				[TLCM_NOUN:skin:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:BLACK:1]
				[TLCM_NOUN:eyes:PLURAL]