item_gloves

[OBJECT:ITEM]

[ITEM_GLOVES:ITEM_GLOVES_GLOVES]
[NAME:glove:gloves]
[MATERIAL_SIZE:1]
[LAYER:UNDER]
[COVERAGE:100]
[LAYER_SIZE:10]
[LAYER_PERMIT:5]
[SOFT]
[LEATHER]
[STRUCTURAL_ELASTICITY_WOVEN_THREAD]

[ITEM_GLOVES:ITEM_GLOVES_MITTENS]
[NAME:mitten:mittens]
[LAYER:COVER]
[COVERAGE:150]
[LAYER_SIZE:15]
[LAYER_PERMIT:10]
[MATERIAL_SIZE:1]
[SOFT]
[LEATHER]
[STRUCTURAL_ELASTICITY_WOVEN_THREAD]