creature_large_ocean

[OBJECT:CREATURE]

[CREATURE:FISH_TROUT]
	[DESCRIPTION:A medium-sized spotted fish found in temperate rivers and lakes.]
	[NAME:trout:trout:trout]
	[CASTE_NAME:trout:trout:trout]
	[CREATURE_TILE:224][COLOR:2:0:1]
	[VERMIN_GROUNDER][VERMIN_FISH]
	[AQUATIC][SMALL_REMAINS][FISHITEM][IMMOBILE_LAND][UNDERSWIM][VERMIN_NOTRAP]
	[NATURAL]
	[NOT_BUTCHERABLE]
	[BIOME:ANY_TEMPERATE_LAKE]
	[BIOME:ANY_TEMPERATE_RIVER]
	[POPULATION_NUMBER:250:500]
	[PREFSTRING:coloration]
	[BODY:BASIC_2PARTBODY:BASIC_HEAD:SIDE_FINS:DORSAL_FIN:TAIL:2EYES:HEART:GUTS:ORGANS:SPINE:BRAIN:SKULL:MOUTH:RIBCAGE]
	[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
		[REMOVE_MATERIAL:SKIN]
		[REMOVE_MATERIAL:LEATHER]
		[REMOVE_MATERIAL:PARCHMENT]
		[REMOVE_MATERIAL:HAIR]
		[USE_MATERIAL_TEMPLATE:SCALE:SCALE_TEMPLATE]
	[BODY_DETAIL_PLAN:STANDARD_TISSUES]
		[REMOVE_TISSUE:SKIN]
		[REMOVE_TISSUE:HAIR]
		[USE_TISSUE_TEMPLATE:SCALE:SCALE_TEMPLATE]
	[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SCALE:FAT:MUSCLE:BONE:CARTILAGE]
	[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
	 [PLUS_TISSUE_LAYER:SCALE:BY_CATEGORY:THROAT]
		[TL_MAJOR_ARTERIES]
	[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
	[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
	[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
	[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
	[HAS_NERVES]
	[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
	[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
	[CREATURE_CLASS:GENERAL_POISON]
	[GETS_WOUND_INFECTIONS]
	[GETS_INFECTIONS_FROM_ROT]
	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
	[BODY_SIZE:0:0:200]
	[BODY_APPEARANCE_MODIFIER:LENGTH:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
	[MAXAGE:2:3]
	[ATTACK:BITE:BODYPART:BY_CATEGORY:MOUTH]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bite:bites]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:MAIN]
		[ATTACK_FLAG_CANLATCH]
	[ALL_ACTIVE]
	[NO_DRINK]
	[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:900:750:600:439:1900:2900] 20 kph, NO DATA
	[SWIMS_INNATE]
	[CANNOT_JUMP]
	[MUNDANE]
	[CASTE:FEMALE]
		[FEMALE]
	[CASTE:MALE]
		[MALE]
	[SELECT_CASTE:ALL]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SCALE]
			[TL_COLOR_MODIFIER:ARGENT:1]  *** Obviously not a rainbow.
				[TLCM_NOUN:scales:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:ATROUS:1]
				[TLCM_NOUN:eyes:PLURAL]





[CREATURE:FISH_ROACH]
	[DESCRIPTION:A medium-sized spotted fish found in temperate rivers and lakes.]
	[NAME:roach:roach:roach]
	[CASTE_NAME:roach:roach:roach]
	[CREATURE_TILE:224][COLOR:2:0:1]
	[VERMIN_GROUNDER][VERMIN_FISH]
	[AQUATIC][SMALL_REMAINS][FISHITEM][IMMOBILE_LAND][UNDERSWIM][VERMIN_NOTRAP]
	[NATURAL]
	[NOT_BUTCHERABLE]
	[BIOME:ANY_TEMPERATE_LAKE]
	[BIOME:ANY_TEMPERATE_RIVER]
	[POPULATION_NUMBER:250:500]
	[PREFSTRING:coloration]
	[BODY:BASIC_2PARTBODY:BASIC_HEAD:SIDE_FINS:DORSAL_FIN:TAIL:2EYES:HEART:GUTS:ORGANS:SPINE:BRAIN:SKULL:MOUTH:RIBCAGE]
	[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
		[REMOVE_MATERIAL:SKIN]
		[REMOVE_MATERIAL:LEATHER]
		[REMOVE_MATERIAL:PARCHMENT]
		[REMOVE_MATERIAL:HAIR]
		[USE_MATERIAL_TEMPLATE:SCALE:SCALE_TEMPLATE]
	[BODY_DETAIL_PLAN:STANDARD_TISSUES]
		[REMOVE_TISSUE:SKIN]
		[REMOVE_TISSUE:HAIR]
		[USE_TISSUE_TEMPLATE:SCALE:SCALE_TEMPLATE]
	[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SCALE:FAT:MUSCLE:BONE:CARTILAGE]
	[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
	 [PLUS_TISSUE_LAYER:SCALE:BY_CATEGORY:THROAT]
		[TL_MAJOR_ARTERIES]
	[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
	[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
	[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
	[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
	[HAS_NERVES]
	[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
	[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
	[CREATURE_CLASS:GENERAL_POISON]
	[GETS_WOUND_INFECTIONS]
	[GETS_INFECTIONS_FROM_ROT]
	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
	[BODY_SIZE:0:0:200]
	[BODY_APPEARANCE_MODIFIER:LENGTH:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
	[MAXAGE:2:3]
	[ATTACK:BITE:BODYPART:BY_CATEGORY:MOUTH]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bite:bites]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:MAIN]
		[ATTACK_FLAG_CANLATCH]
	[ALL_ACTIVE]
	[NO_DRINK]
	[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:900:750:600:439:1900:2900] 20 kph, NO DATA
	[SWIMS_INNATE]
	[CANNOT_JUMP]
	[MUNDANE]
	[CASTE:FEMALE]
		[FEMALE]
	[CASTE:MALE]
		[MALE]
	[SELECT_CASTE:ALL]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SCALE]
			[TL_COLOR_MODIFIER:ARGENT:1]  *** Obviously not a rainbow.
				[TLCM_NOUN:scales:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:OCHRE:1]
				[TLCM_NOUN:eyes:PLURAL]

[CREATURE:FISH_PERCH]
	[DESCRIPTION:A small striped fish found in fresh water.]
	[NAME:perch:perch:perch]
	[CASTE_NAME:perch:perch:perch]
	[CREATURE_TILE:224][COLOR:7:0:1]
	[VERMIN_GROUNDER][VERMIN_FISH]
	[AQUATIC][SMALL_REMAINS][FISHITEM][IMMOBILE_LAND][UNDERSWIM][VERMIN_NOTRAP]
	[NATURAL]
	[NOT_BUTCHERABLE]
	[BIOME:RIVER_TEMPERATE_FRESHWATER]
	[BIOME:LAKE_TEMPERATE_FRESHWATER]
	[POPULATION_NUMBER:250:500]
	[PREFSTRING:coloration]
	[BODY:BASIC_2PARTBODY:BASIC_HEAD:SIDE_FINS:DORSAL_FIN:TAIL:2EYES:HEART:GUTS:ORGANS:SPINE:BRAIN:SKULL:MOUTH:RIBCAGE]
	[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
		[REMOVE_MATERIAL:SKIN]
		[REMOVE_MATERIAL:LEATHER]
		[REMOVE_MATERIAL:PARCHMENT]
		[REMOVE_MATERIAL:HAIR]
		[USE_MATERIAL_TEMPLATE:SCALE:SCALE_TEMPLATE]
	[BODY_DETAIL_PLAN:STANDARD_TISSUES]
		[REMOVE_TISSUE:SKIN]
		[REMOVE_TISSUE:HAIR]
		[USE_TISSUE_TEMPLATE:SCALE:SCALE_TEMPLATE]
	[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SCALE:FAT:MUSCLE:BONE:CARTILAGE]
	[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
	 [PLUS_TISSUE_LAYER:SCALE:BY_CATEGORY:THROAT]
		[TL_MAJOR_ARTERIES]
	[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
	[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
	[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
	[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
	[HAS_NERVES]
	[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
	[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
	[CREATURE_CLASS:GENERAL_POISON]
	[GETS_WOUND_INFECTIONS]
	[GETS_INFECTIONS_FROM_ROT]
	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
	[BODY_SIZE:0:0:200]
	[BODY_APPEARANCE_MODIFIER:LENGTH:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
	[MAXAGE:2:3]
	[ATTACK:BITE:BODYPART:BY_CATEGORY:MOUTH]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bite:bites]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:MAIN]
		[ATTACK_FLAG_CANLATCH]
	[ALL_ACTIVE]
	[NO_DRINK]
	[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:900:750:600:439:1900:2900] 20 kph, NO DATA
	[SWIMS_INNATE]
	[CANNOT_JUMP]
	[MUNDANE]
	[CASTE:FEMALE]
		[FEMALE]
	[CASTE:MALE]
		[MALE]
	[SELECT_CASTE:ALL]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SCALE]
			[TL_COLOR_MODIFIER:BRUNNEOUS:1]
				[TLCM_NOUN:scales:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:ATROUS:1]
				[TLCM_NOUN:eyes:PLURAL]


[CREATURE:TOAD]
	[DESCRIPTION:A squat amphibian with leathery skin, found in relatively dry areas.]
	[NAME:toad:toads:toad]
	[CASTE_NAME:toad:toads:toad]
	[CREATURE_TILE:249][COLOR:2:0:0]
	[PETVALUE:10]
	[VERMIN_GROUNDER][FREQUENCY:100][VERMIN_HATEABLE]
	[AMPHIBIOUS][SMALL_REMAINS][NO_WINTER][UNDERSWIM]
	[NATURAL][PET_EXOTIC]
	[NOT_BUTCHERABLE]
	[BIOME:ANY_POOL]
	[POPULATION_NUMBER:250:500]
	[PREFSTRING:beauty]
	[BODY:QUADRUPED_NECK:2EYES:NOSE:2LUNGS:HEART:GUTS:ORGANS:THROAT:NECK:SPINE:BRAIN:SKULL:MOUTH:TONGUE:RIBCAGE]
	[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
		[REMOVE_MATERIAL:HAIR]
 [SELECT_MATERIAL:LEATHER]
        [STATE_NAME:ALL_SOLID:leather]
        [STATE_ADJ:ALL_SOLID:leather]
        [PREFIX:NONE]
	[BODY_DETAIL_PLAN:STANDARD_TISSUES]
		[REMOVE_TISSUE:HAIR]
	[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SKIN:FAT:MUSCLE:BONE:CARTILAGE]
	[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
	 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
		[TL_MAJOR_ARTERIES]
	[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
	[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
	[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
	[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
	[HAS_NERVES]
	[APPLY_CREATURE_VARIATION:STANDARD_WALK_CRAWL_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[SWIMS_INNATE]
	[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
	[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
	[CREATURE_CLASS:GENERAL_POISON]
	[GETS_WOUND_INFECTIONS]
	[GETS_INFECTIONS_FROM_ROT]
	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
	[BODY_SIZE:0:0:200]
	[BODY_APPEARANCE_MODIFIER:LENGTH:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
	[MAXAGE:2:3]
	[ATTACK:BITE:BODYPART:BY_CATEGORY:MOUTH]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bite:bites]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:MAIN]
		[ATTACK_FLAG_CANLATCH]
	[NOCTURNAL]
	[MUNDANE]
	[CASTE:FEMALE]
		[FEMALE]
	[CASTE:MALE]
		[MALE]
	[SELECT_CASTE:ALL]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SKIN]
			[TL_COLOR_MODIFIER:DARK_GREEN:1]
				[TLCM_NOUN:skin:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:BLACK:1]
				[TLCM_NOUN:eyes:PLURAL]


 
[CREATURE:SALAMANDER]
	[DESCRIPTION:A tiny amphibian, found in streams.]
	[NAME:salamander:salamanders:salamander]
	[CASTE_NAME:salamander:salamanders:salamander]
	[CREATURE_TILE:249][COLOR:4:0:1]
	[PETVALUE:10]
	[PET_EXOTIC]
	[VERMIN_GROUNDER][FREQUENCY:100]
	[SMALL_REMAINS]
	[AMPHIBIOUS][UNDERSWIM]
	[NATURAL]
	[NOT_BUTCHERABLE]
	[BIOME:ANY_RIVER]
	[POPULATION_NUMBER:250:500]
	[PREFSTRING:gills]
	[BODY:QUADRUPED_NECK:TAIL:2EYES:NOSE:2LUNGS:HEART:GUTS:ORGANS:THROAT:NECK:SPINE:BRAIN:SKULL:MOUTH:TONGUE:RIBCAGE]
	[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
		[REMOVE_MATERIAL:HAIR]
	[BODY_DETAIL_PLAN:STANDARD_TISSUES]
		[REMOVE_TISSUE:HAIR]
	[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SKIN:FAT:MUSCLE:BONE:CARTILAGE]
	[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
	 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
		[TL_MAJOR_ARTERIES]
	[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
	[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
	[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
	[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
	[HAS_NERVES]
	[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
	[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
	[CREATURE_CLASS:GENERAL_POISON]
	[GETS_WOUND_INFECTIONS]
	[GETS_INFECTIONS_FROM_ROT]
	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
	[BODY_SIZE:0:0:200]
	[BODY_APPEARANCE_MODIFIER:LENGTH:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
	[MAXAGE:2:3]
	[ATTACK:BITE:BODYPART:BY_CATEGORY:MOUTH]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bite:bites]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:MAIN]
		[ATTACK_FLAG_CANLATCH]
	[ALL_ACTIVE]
	[HOMEOTHERM:10040]
	[APPLY_CREATURE_VARIATION:STANDARD_WALK_CRAWL_GAITS:9000:8900:8825:8775:9500:9900] 1 kph, NO DATA
	[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph, NO DATA
	[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph, NO DATA
	[SWIMS_INNATE]
	[CANNOT_JUMP]
	[MUNDANE]
	[CASTE:FEMALE]
		[FEMALE]
	[CASTE:MALE]
		[MALE]
	[SELECT_CASTE:ALL]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SKIN]
			[TL_COLOR_MODIFIER:RED:1]
				[TLCM_NOUN:skin:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:BLACK:1]
				[TLCM_NOUN:eyes:PLURAL]

frogs were sponsored by the generous contributions of the Bay 12 community.

[CREATURE:FROG]
	[DESCRIPTION:A tiny amphibian found anywhere there is lots of water.]
	[NAME:frog:frogs:frog]
	[CASTE_NAME:frog:frogs:frog]
	[CREATURE_TILE:249][COLOR:2:0:1]
	[PETVALUE:10]
	[VERMIN_GROUNDER][FREQUENCY:100]
	[AMPHIBIOUS][SMALL_REMAINS][NO_WINTER][UNDERSWIM]
	[BENIGN][NATURAL][PET_EXOTIC]
	[NOT_BUTCHERABLE]
	[BIOME:ANY_RIVER]
	[BIOME:ANY_POOL]
	[BIOME:ANY_LAKE]
	[BIOME:ANY_WETLAND]
	[POPULATION_NUMBER:250:500]
	[PREFSTRING:distinct mating call]
	[BODY:QUADRUPED_NECK:2EYES:NOSE:2LUNGS:HEART:GUTS:ORGANS:THROAT:NECK:SPINE:BRAIN:SKULL:MOUTH:RIBCAGE]
	[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
		[REMOVE_MATERIAL:HAIR]
	[BODY_DETAIL_PLAN:STANDARD_TISSUES]
		[REMOVE_TISSUE:HAIR]
	[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SKIN:FAT:MUSCLE:BONE:CARTILAGE]
	[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
	 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
		[TL_MAJOR_ARTERIES]
	[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
	[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
	[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
	[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
	[HAS_NERVES]
	[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
	[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
	[CREATURE_CLASS:GENERAL_POISON]
	[GETS_WOUND_INFECTIONS]
	[GETS_INFECTIONS_FROM_ROT]
	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
	[BODY_SIZE:0:0:100]
	[BODY_APPEARANCE_MODIFIER:LENGTH:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
	[MAXAGE:2:5]
	[ATTACK:BITE:BODYPART:BY_CATEGORY:MOUTH]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bite:bites]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:MAIN]
		[ATTACK_FLAG_CANLATCH]
	[NOCTURNAL]
	[APPLY_CREATURE_VARIATION:STANDARD_WALK_CRAWL_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[APPLY_CREATURE_VARIATION:STANDARD_CLIMBING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[STANCE_CLIMBER][NATURAL_SKILL:CLIMBING:15]
	[SWIMS_INNATE]
	[MUNDANE]
	[CASTE:FEMALE]
		[FEMALE]
	[CASTE:MALE]
		[MALE]
	[SELECT_CASTE:ALL]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SKIN]
			[TL_COLOR_MODIFIER:VIRID:1:XANTHIC:1:FLAMMEOUS:1:TAN:1]
				[TLCM_NOUN:skin:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:ATROUS:1]
				[TLCM_NOUN:eyes:PLURAL]



[CREATURE:LEECH]
	[DESCRIPTION:A tiny, aquatic, worm-like creature that feeds on blood.]
	[NAME:leech:leeches:leech]
	[CASTE_NAME:leech:leeches:leech]
	[CREATURE_TILE:'~'][COLOR:0:0:1]
	[PETVALUE:10]
	[VERMIN_SOIL]
	[FREQUENCY:100][VERMIN_HATEABLE]
	[SMALL_REMAINS][VERMIN_NOTRAP][NOBONES]
	[NATURAL][PET_EXOTIC]
	[NOT_BUTCHERABLE]
	[BIOME:ANY_POOL]
	[BIOME:ANY_LAKE]
	[APPLY_CREATURE_VARIATION:STANDARD_WALK_CRAWL_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[SWIMS_INNATE]
	[POPULATION_NUMBER:250:500]
	[PREFSTRING:feeding habits]
	[BODY:BASIC_1PARTBODY:BASIC_HEAD:HEART:GUTS:BRAIN:MOUTH]
	[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
		[REMOVE_MATERIAL:HAIR]
		[REMOVE_MATERIAL:BONE]
	[BODY_DETAIL_PLAN:STANDARD_TISSUES]
		[REMOVE_TISSUE:HAIR]
		[REMOVE_TISSUE:BONE]
	[BODY_DETAIL_PLAN:EXOSKELETON_TISSUE_LAYERS:SKIN:FAT:MUSCLE]
	[ATTACK:BITE:BODYPART:BY_CATEGORY:MOUTH]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bite:bites]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:MAIN]
		[ATTACK_FLAG_CANLATCH]
		[SPECIALATTACK_SUCK_BLOOD:25:50]
	[HAS_NERVES]
	[MUNDANE]
	[USE_MATERIAL_TEMPLATE:ICHOR:ICHOR_TEMPLATE]
	[BLOOD:LOCAL_CREATURE_MAT:ICHOR:LIQUID]
	[CREATURE_CLASS:GENERAL_POISON]
	[GETS_WOUND_INFECTIONS]
	[GETS_INFECTIONS_FROM_ROT]
	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
	[BODY_SIZE:0:0:100]
	[BODY_APPEARANCE_MODIFIER:LENGTH:90:95:98:100:102:105:110]
	[MAXAGE:5:10]
	[ALL_ACTIVE]
	[CANNOT_JUMP]
	[NO_SLEEP]
	[NO_DIZZINESS]
	[EXTRAVISION]
	[SET_TL_GROUP:BY_CATEGORY:ALL:SKIN]
		[TL_COLOR_MODIFIER:BLACK:1]
			[TLCM_NOUN:skin:SINGULAR]


[CREATURE:MINK]
	[DESCRIPTION:A small, predatory, weasel-like mammal.  It is also semi-aquatic.]
	[NAME:mink:minks:mink]
	[CASTE_NAME:mink:minks:mink]
	[GENERAL_CHILD_NAME:mink kit:mink kits]
	[CREATURE_TILE:'m'][COLOR:6:0:0]
	[CREATURE_CLASS:MAMMAL]
	[PETVALUE:50]
	[PET_EXOTIC]
	[NATURAL]
	[BIOME:ANY_TEMPERATE_LAKE]
	[BIOME:ANY_TEMPERATE_RIVER]
	[LARGE_ROAMING]
	[POPULATION_NUMBER:15:30]
	[CLUSTER_NUMBER:1:1]
	[BENIGN]
	[PREFSTRING:long bodies]
	[BODY:QUADRUPED_NECK:TAIL:2EYES:2EARS:NOSE:2LUNGS:HEART:GUTS:ORGANS:THROAT:NECK:SPINE:BRAIN:SKULL:4TOES_FQ_REG:5TOES_RQ_REG:MOUTH:RODENT_TEETH:RIBCAGE]
	[BODYGLOSS:PAW]
	[GRASSTRAMPLE:0]
	[BODY_DETAIL_PLAN:STANDARD_MATERIALS][SELECT_MATERIAL:LEATHER][STATE_NAME:ALL_SOLID:leather][STATE_ADJ:ALL_SOLID:leather][PREFIX:NONE]
	[BODY_DETAIL_PLAN:STANDARD_TISSUES]
		[SELECT_TISSUE:HAIR]
			[INSULATION:200]
	[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SKIN:FAT:MUSCLE:BONE:CARTILAGE]
	[BODY_DETAIL_PLAN:BODY_HAIR_TISSUE_LAYERS:HAIR]
	[USE_MATERIAL_TEMPLATE:NAIL:NAIL_TEMPLATE]
	[USE_TISSUE_TEMPLATE:NAIL:NAIL_TEMPLATE]
	[TISSUE_LAYER:BY_CATEGORY:TOE:NAIL:FRONT]
	[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
	 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
		[TL_MAJOR_ARTERIES]
	[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
	[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
	[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
	[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
	[HAS_NERVES]
	[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
	[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
	[CREATURE_CLASS:GENERAL_POISON]
	[GETS_WOUND_INFECTIONS]
	[GETS_INFECTIONS_FROM_ROT]
	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
	[BODY_SIZE:0:0:80]
	[BODY_SIZE:1:0:400]
	[BODY_SIZE:2:0:800]
	[BODY_APPEARANCE_MODIFIER:LENGTH:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
	[MAXAGE:10:15]
	[ATTACK:BITE:CHILD_BODYPART_GROUP:BY_CATEGORY:HEAD:BY_CATEGORY:TOOTH]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bite:bites]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PENETRATION_PERC:100]
		[ATTACK_FLAG_EDGE]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:MAIN]
		[ATTACK_FLAG_CANLATCH]
	[ATTACK:SCRATCH:CHILD_TISSUE_LAYER_GROUP:BY_TYPE:STANCE:BY_CATEGORY:ALL:NAIL]
		[ATTACK_SKILL:GRASP_STRIKE]
		[ATTACK_VERB:scratch:scratches]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PENETRATION_PERC:100]
		[ATTACK_FLAG_EDGE]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:SECOND]
	[CHILD:1]
	[NOCTURNAL]
	[HOMEOTHERM:10067]
	[APPLY_CREATURE_VARIATION:STANDARD_QUADRUPED_GAITS:5341:4723:4112:1254:6433:7900] 7 kph
	[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[SWIMS_INNATE]
	[MUNDANE]
	[CASTE:FEMALE]
		[FEMALE]
	[CASTE:MALE]
		[MALE]
		[SET_BP_GROUP:BY_TYPE:LOWERBODY][BP_ADD_TYPE:GELDABLE]
	[SELECT_CASTE:ALL]
		[SET_TL_GROUP:BY_CATEGORY:ALL:HAIR]
			[TL_COLOR_MODIFIER:BROWN:1]
				[TLCM_NOUN:hair:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SKIN]
	[TL_COLOR_MODIFIER:BROWN:1:BURNT_UMBER:1:CINNAMON:1:COPPER:1:DARK_BROWN:1:DARK_PEACH:1:TAN_DARK:1:ECRU:1:PALE_BROWN:1:PALE_CHESTNUT:1:PALE_PINK:1:PEACH:1:PINK:1:RAW_UMBER:1:SEPIA:1:TAN:1:TAUPE_PALE:1:TAUPE_SANDY:1]
				[TLCM_NOUN:skin:SINGULAR]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:BLACK:1]
				[TLCM_NOUN:eyes:PLURAL]



[CREATURE:POND_TURTLE]
	[DESCRIPTION:A tiny reptile with a shell on its back.  It can be found in rivers and ponds.]
	[NAME:pond turtle:pond turtles:pond turtle]
	[CASTE_NAME:pond turtle:pond turtles:pond turtle]
	[CREATURE_TILE:15][COLOR:2:0:0]
	[PETVALUE:10]
	[VERMIN_FISH][FISHITEM]
	[AMPHIBIOUS][SMALL_REMAINS][NO_WINTER]
	[BENIGN][NATURAL][PET_EXOTIC]
	[NOT_BUTCHERABLE]
	[CARNIVORE]
	[BIOME:ANY_POOL]
	[BIOME:ANY_LAKE]
	[POPULATION_NUMBER:250:500]
	[PREFSTRING:shells]
	[BODY:QUADRUPED_NECK:TAIL:2EYES:NOSE:2LUNGS:HEART:GUTS:ORGANS:THROAT:NECK:SPINE:BRAIN:SKULL:MOUTH:RIBCAGE:SHELL]
	[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
		[REMOVE_MATERIAL:SKIN]
		[REMOVE_MATERIAL:LEATHER]
		[REMOVE_MATERIAL:PARCHMENT]
		[REMOVE_MATERIAL:HAIR]
		[USE_MATERIAL_TEMPLATE:SCALE:SCALE_TEMPLATE]
		[USE_MATERIAL_TEMPLATE:SHELL:SHELL_TEMPLATE]
			[STATE_COLOR:ALL:DARK_GREEN]
	[BODY_DETAIL_PLAN:STANDARD_TISSUES]
		[REMOVE_TISSUE:SKIN]
		[REMOVE_TISSUE:HAIR]
		[USE_TISSUE_TEMPLATE:SCALE:SCALE_TEMPLATE]
		[USE_TISSUE_TEMPLATE:SHELL:SHELL_TEMPLATE]
	[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SCALE:FAT:MUSCLE:BONE:CARTILAGE]
	[BODY_DETAIL_PLAN:LEATHERY_EGG_MATERIALS]
	[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
	 [PLUS_TISSUE_LAYER:SCALE:BY_CATEGORY:THROAT]
		[TL_MAJOR_ARTERIES]
	[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
	[BODY_DETAIL_PLAN:SHELL_POSITIONS]
	[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
	[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
	[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
	[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
	[HAS_NERVES]
	[APPLY_CREATURE_VARIATION:STANDARD_WALK_CRAWL_GAITS:6561:6115:5683:1755:7456:8567] 5 kph
	[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:9000:8900:8825:8775:9500:9900] 1 kph
	[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:900:734:568:366:1900:2900] 24 kph
	[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
	[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
	[CREATURE_CLASS:GENERAL_POISON]
	[GETS_WOUND_INFECTIONS]
	[GETS_INFECTIONS_FROM_ROT]
	[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
	[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
	[BODY_SIZE:0:0:5]
	[BODY_SIZE:1:0:500]
	[BODY_APPEARANCE_MODIFIER:LENGTH:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
	[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
	[MAXAGE:40:100]
	[ATTACK:BITE:BODYPART:BY_CATEGORY:MOUTH]
		[ATTACK_SKILL:BITE]
		[ATTACK_VERB:bite:bites]
		[ATTACK_CONTACT_PERC:100]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		[ATTACK_PRIORITY:MAIN]
		[ATTACK_FLAG_CANLATCH]
	[RETRACT_INTO_BP:BY_CATEGORY:SHELL:retract into <pro_pos> shell:retracts into <pro_pos> shell:come out of <pro_pos> shell:comes out of <pro_pos> shell]
	[DIURNAL]
	[SWIMS_INNATE]
	[CANNOT_JUMP]
	[MUNDANE]
	[CASTE:FEMALE]
		[FEMALE]
		[LAYS_EGGS]
			[EGG_MATERIAL:LOCAL_CREATURE_MAT:EGGSHELL:SOLID]
			[EGG_MATERIAL:LOCAL_CREATURE_MAT:EGG_WHITE:LIQUID]
			[EGG_MATERIAL:LOCAL_CREATURE_MAT:EGG_YOLK:LIQUID]
			[EGG_SIZE:6]
			[CLUTCH_SIZE:1:15]
	[CASTE:MALE]
		[MALE]
	[SELECT_CASTE:ALL]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SCALE]
			[TL_COLOR_MODIFIER:DARK_GREEN:1]
				[TLCM_NOUN:scales:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:BLACK:1]
				[TLCM_NOUN:eyes:PLURAL]
