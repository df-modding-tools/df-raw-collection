item_armor

[OBJECT:ITEM]



[ITEM_HELM:ITEM_HELM_COIF1]
[NAME:open-faced chainmail coif:open-faced chainmail coifs]
[LAYER:UNDER]
[ARMORLEVEL:2]
[COVERAGE:50]
[LAYER_SIZE:5]
[LAYER_PERMIT:1]
[MATERIAL_SIZE:2]
[METAL]
[STRUCTURAL_ELASTICITY_CHAIN_ALL]

[ITEM_HELM:ITEM_HELM_COIF2]
[NAME:mouth-covering chainmail coif:mouth-covering chainmail coifs]
[LAYER:UNDER]
[ARMORLEVEL:2]
[COVERAGE:70]
[LAYER_SIZE:5]
[LAYER_PERMIT:1]
[MATERIAL_SIZE:2]
[METAL]
[STRUCTURAL_ELASTICITY_CHAIN_ALL]

[ITEM_HELM:ITEM_HELM_COIF3]
[NAME:slitted chainmail coif:slitted chainmail coifs]
[LAYER:UNDER]
[ARMORLEVEL:2]
[COVERAGE:80]
[LAYER_SIZE:5]
[LAYER_PERMIT:1]
[MATERIAL_SIZE:2]
[METAL]
[STRUCTURAL_ELASTICITY_CHAIN_ALL]

[ITEM_HELM:ITEM_HELM_COIF4]
[NAME:eye-holed chainmail coif:eye-holed chainmail coifs]
[LAYER:UNDER]
[ARMORLEVEL:2]
[COVERAGE:90]
[LAYER_SIZE:5]
[LAYER_PERMIT:1]
[MATERIAL_SIZE:2]
[METAL]
[STRUCTURAL_ELASTICITY_CHAIN_ALL]



[ITEM_HELM:ITEM_HELM_AVENTAIL_CHAIN1]
[NAME:open-faced chainmail aventail:open-faced chainmail aventails]
[LAYER:UNDER]
[ARMORLEVEL:2]
[COVERAGE:50]
[LAYER_SIZE:5]
[LAYER_PERMIT:1]
[MATERIAL_SIZE:2]
[METAL]
[STRUCTURAL_ELASTICITY_CHAIN_ALL]

[ITEM_HELM:ITEM_HELM_AVENTAIL_CHAIN2]
[NAME:mouth-covering chainmail aventail:mouth-covering chainmail aventails]
[LAYER:UNDER]
[ARMORLEVEL:2]
[COVERAGE:70]
[LAYER_SIZE:5]
[LAYER_PERMIT:1]
[MATERIAL_SIZE:2]
[METAL]
[STRUCTURAL_ELASTICITY_CHAIN_ALL]

[ITEM_HELM:ITEM_HELM_AVENTAIL_CHAIN3]
[NAME:slitted chainmail aventail:slitted chainmail aventails]
[LAYER:UNDER]
[ARMORLEVEL:2]
[COVERAGE:80]
[LAYER_SIZE:5]
[LAYER_PERMIT:1]
[MATERIAL_SIZE:2]
[METAL]
[STRUCTURAL_ELASTICITY_CHAIN_ALL]

[ITEM_HELM:ITEM_HELM_AVENTAIL_CHAIN4]
[NAME:eye-holed chainmail aventail:eye-holed chainmail aventails]
[LAYER:UNDER]
[ARMORLEVEL:2]
[COVERAGE:90]
[LAYER_SIZE:5]
[LAYER_PERMIT:1]
[MATERIAL_SIZE:2]
[METAL]
[STRUCTURAL_ELASTICITY_CHAIN_ALL]



