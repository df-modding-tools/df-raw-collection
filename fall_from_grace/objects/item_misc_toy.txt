item_toy

[OBJECT:ITEM]

[ITEM_TOY:ITEM_TOY_TOY1]
[NAME:spinning top:spinning tops]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_TOY2]
[NAME:doll:dolls]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_TOY3]
[NAME:spinning top:spinning tops]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_TOY4]
[NAME:stilts:stilts]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_TOY5]
[NAME:ball:balls]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_TOY6]
[NAME:hoop-and-stick:hoop-and-sticks]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_TOY7]
[NAME:block:blocks]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_TOY8]
[NAME:toy sword:toy swords]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_TOY9]
[NAME:toy animal:toy animals]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_TOY10]
[NAME:bandalore:bandalores]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_TOY11]
[NAME:puzzle box:puzzle boxes]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_CLOCKTOY1]
[NAME:clockwork learning kit:clockwork learning kits]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_CLOCKTOY2]
[NAME:clockwork music box:clockwork music boxes]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_CLOCKTOY3]
[NAME:clockwork diorama:clockwork dioramas]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_CLOCKTOY4]
[NAME:clockwork puzzle box:clockwork puzzle boxs]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_CLOCKTOY5]
[NAME:clockwork game set:clockwork game sets]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_CLOCKTOY6]
[NAME:clockwork doll:clockwork dolls]
[HARD_MAT]







