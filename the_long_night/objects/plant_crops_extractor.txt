plant_crops

[OBJECT:PLANT]



[PLANT:STIMULANT_OOZE]
	[NAME:stimulant nanofactory][NAME_PLURAL:stimulant nanofactories][ADJ:stimulant nanofactory]
	[USE_MATERIAL_TEMPLATE:STRUCTURAL:STRUCTURAL_PLANT_TEMPLATE]
		[MATERIAL_VALUE:2]
		[MATERIAL_REACTION_PRODUCT:DRINK_MAT:LOCAL_PLANT_MAT:DRINK]
		[MATERIAL_REACTION_PRODUCT:SEED_MAT:LOCAL_PLANT_MAT:SEED]
	[BASIC_MAT:LOCAL_PLANT_MAT:STRUCTURAL]
	[USE_MATERIAL_TEMPLATE:MUSHROOM:MUSHROOM_TEMPLATE]
		[EDIBLE_VERMIN]
	[PICKED_TILE:042][PICKED_COLOR:7:0:1]
	[GROWDUR:300][VALUE:2]
	[USE_MATERIAL_TEMPLATE:DRINK:PLANT_UPPER_TEMPLATE]
		[STATE_NAME_ADJ:ALL_SOLID:frozen stimulants]
		[STATE_NAME_ADJ:LIQUID:stimulants]
		[STATE_NAME_ADJ:GAS:boiling stimulants]
		[MATERIAL_VALUE:2]
		[DISPLAY_COLOR:7:0:1]
		[EDIBLE_RAW]
		[EDIBLE_COOKED]
		[PREFIX:NONE]
	[DRINK:LOCAL_PLANT_MAT:DRINK]

	[USE_MATERIAL_TEMPLATE:SEED:SEED_TEMPLATE]
		[MATERIAL_VALUE:1]
		[EDIBLE_VERMIN]
	[SEED:stimulant nanofactory packet:stimulant nanofactory packet:7:0:1:LOCAL_PLANT_MAT:SEED]
	[SPRING][SUMMER][AUTUMN][WINTER]
	[FREQUENCY:1]
	[CLUSTERSIZE:1]
	[PREFSTRING:ability to produce chemical stimulants]
	[WET]
	[BIOME:SUBTERRANEAN_WATER][BIOME:SUBTERRANEAN_CHASM]
	[UNDERGROUND_DEPTH:1:3]
	[SHRUB_TILE:'o']
	[DEAD_SHRUB_TILE:'o']
	[SHRUB_COLOR:7:0:0]
	[DEAD_SHRUB_COLOR:0:0:1]


[PLANT:DEPRESSANT_OOZE]
	[NAME:depressant nanofactory][NAME_PLURAL:depressant nanofactories][ADJ:depressant nanofactory]
	[USE_MATERIAL_TEMPLATE:STRUCTURAL:STRUCTURAL_PLANT_TEMPLATE]
		[MATERIAL_VALUE:2]
		[MATERIAL_REACTION_PRODUCT:DRINK_MAT:LOCAL_PLANT_MAT:DRINK]
		[MATERIAL_REACTION_PRODUCT:SEED_MAT:LOCAL_PLANT_MAT:SEED]
	[BASIC_MAT:LOCAL_PLANT_MAT:STRUCTURAL]
	[USE_MATERIAL_TEMPLATE:MUSHROOM:MUSHROOM_TEMPLATE]
		[EDIBLE_VERMIN]
	[PICKED_TILE:042][PICKED_COLOR:7:0:1]
	[GROWDUR:300][VALUE:2]
	[USE_MATERIAL_TEMPLATE:DRINK:PLANT_UPPER_TEMPLATE]
		[STATE_NAME_ADJ:ALL_SOLID:frozen depressants]
		[STATE_NAME_ADJ:LIQUID:depressants]
		[STATE_NAME_ADJ:GAS:boiling depressants]
		[MATERIAL_VALUE:2]
		[DISPLAY_COLOR:7:0:1]
		[EDIBLE_RAW]
		[EDIBLE_COOKED]
		[PREFIX:NONE]
	[DRINK:LOCAL_PLANT_MAT:DRINK]
	[USE_MATERIAL_TEMPLATE:SEED:SEED_TEMPLATE]
		[MATERIAL_VALUE:1]
		[EDIBLE_VERMIN]
	[SEED:depressant nanofactory packet:depressant nanofactory packet:7:0:1:LOCAL_PLANT_MAT:SEED]
	[SPRING][SUMMER][AUTUMN][WINTER]
	[FREQUENCY:1]
	[CLUSTERSIZE:1]
	[PREFSTRING:ability to produce chemical depressants]
	[WET]
	[BIOME:SUBTERRANEAN_WATER][BIOME:SUBTERRANEAN_CHASM]
	[UNDERGROUND_DEPTH:1:3]
	[SHRUB_TILE:'o']
	[DEAD_SHRUB_TILE:'o']
	[SHRUB_COLOR:7:0:0]
	[DEAD_SHRUB_COLOR:0:0:1]




[PLANT:FIBROUS_OOZE] 
	[NAME:fiber nanofactory][NAME_PLURAL:fiber nanofactories][ADJ:fiber nanofactory]
	[USE_MATERIAL_TEMPLATE:STRUCTURAL:STRUCTURAL_PLANT_TEMPLATE]
		[MATERIAL_VALUE:2]
		[MATERIAL_REACTION_PRODUCT:SEED_MAT:LOCAL_PLANT_MAT:SEED]
		[MATERIAL_REACTION_PRODUCT:PRESS_PAPER_MAT:LOCAL_PLANT_MAT:THREAD]
	[BASIC_MAT:LOCAL_PLANT_MAT:STRUCTURAL]
	[PICKED_TILE:231][PICKED_COLOR:7:0:0]
	[GROWDUR:300][VALUE:5]
	[USE_MATERIAL_TEMPLATE:SEED:SEED_TEMPLATE]
		[MATERIAL_VALUE:1]
		[EDIBLE_VERMIN]
		[EDIBLE_COOKED]
	[SEED:fiber nanofactory packet:fiber nanofactory packet:0:0:1:LOCAL_PLANT_MAT:SEED]
	[FREQUENCY:100]
	[CLUSTERSIZE:5]
	[USE_MATERIAL_TEMPLATE:THREAD:THREAD_PLANT_TEMPLATE]
		[STATE_NAME_ADJ:SOLID:synth-fiber]
		[STATE_NAME_ADJ:SOLID_PASTE:fiber slurry]
		[STATE_NAME_ADJ:SOLID_PRESSED:synth-paper]
		[PREFIX:NONE]
		[REACTION_CLASS:PAPER_SLURRY]
		[MATERIAL_VALUE:2]
		[STOCKPILE_GLOB_PASTE]
	[THREAD:LOCAL_PLANT_MAT:THREAD]
	[SPRING][SUMMER][AUTUMN][WINTER]
	[BIOME:SUBTERRANEAN_WATER][BIOME:SUBTERRANEAN_CHASM]
	[UNDERGROUND_DEPTH:1:3][SHRUB_COLOR:7:0:0]
	[FREQUENCY:1]
	[CLUSTERSIZE:1]
	[WET]
	[SHRUB_TILE:'o']
	[DEAD_SHRUB_TILE:'o']
	[PREFSTRING:utility for making fabric and paper]

[PLANT:OIL_OOZE] 
	[NAME:oil nanofactory][NAME_PLURAL:oil nanofactories][ADJ:oil nanofactory]	
	[USE_MATERIAL_TEMPLATE:STRUCTURAL:STRUCTURAL_PLANT_TEMPLATE]
		[MATERIAL_VALUE:2]
		[EDIBLE_VERMIN]
		[MATERIAL_REACTION_PRODUCT:SEED_MAT:LOCAL_PLANT_MAT:SEED]
	[BASIC_MAT:LOCAL_PLANT_MAT:STRUCTURAL]
	[PICKED_TILE:5][PICKED_COLOR:7:0:0]
	[GROWDUR:500][VALUE:5]
	[USE_MATERIAL_TEMPLATE:OIL:PLANT_OIL_TEMPLATE]
		[STATE_NAME_ADJ:ALL_SOLID:frozen synth-oil]
		[STATE_NAME_ADJ:LIQUID:synth-oil]
		[STATE_NAME_ADJ:GAS:boiling synth-oil]
		[PREFIX:NONE]
		[MATERIAL_VALUE:5]
		[EDIBLE_COOKED]
	[USE_MATERIAL_TEMPLATE:SOAP:PLANT_SOAP_TEMPLATE]
		[STATE_NAME_ADJ:ALL_SOLID:synth-soap]
		[STATE_NAME_ADJ:LIQUID:melted synth-soap]
		[STATE_NAME_ADJ:GAS:n/a]
		[PREFIX:NONE]
		[MATERIAL_VALUE:5]
	[USE_MATERIAL_TEMPLATE:SEED:SEED_TEMPLATE]
		[STATE_NAME_ADJ:ALL_SOLID:synth-oil packet]
		[STATE_NAME_ADJ:SOLID_PASTE:synth-oil paste]
		[STATE_NAME_ADJ:SOLID_PRESSED:synth-oil cake]
		[MATERIAL_VALUE:1]
		[EDIBLE_VERMIN]
		[EDIBLE_RAW]
		[EDIBLE_COOKED]
		[MATERIAL_REACTION_PRODUCT:PRESS_LIQUID_MAT:LOCAL_PLANT_MAT:OIL]
		[PREFIX:NONE]
		[STOCKPILE_GLOB_PASTE]
		[STOCKPILE_GLOB_PRESSED]
	[SEED:oil nanofactory packet:oil nanofactory packet:0:0:1:LOCAL_PLANT_MAT:SEED]
	[SPRING][SUMMER][AUTUMN][WINTER]
	[BIOME:SUBTERRANEAN_WATER][BIOME:SUBTERRANEAN_CHASM]
	[UNDERGROUND_DEPTH:1:3][SHRUB_COLOR:0:0:1]
	[FREQUENCY:1]
	[CLUSTERSIZE:1]
	[WET]
	[SHRUB_TILE:'o']
	[DEAD_SHRUB_TILE:'o']
	[PREFSTRING:utility for making various oils]
