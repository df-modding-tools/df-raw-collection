item_toy_dark_ages

[OBJECT:ITEM]

===========================================================================================================
Dark Ages III:War & Mythos: 0.43.05 | v37 | GM-X | AdventRPG.com
===========================================================================================================

[ITEM_TOY:ITEM_TOY_SHADOW_FIGURE]
[NAME:shadow figurine:shadow figurines]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_ABOLETH_FIGURE]
[NAME:aboleth figurine:aboleth figurines]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_ILLITHID_FIGURE]
[NAME:illithid figurine:illithid figurines]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_ILLITHID_DRAGON_FIGURE]
[NAME:illithid dragon figurine:illithid dragon figurines]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_DREAM_CATCHER]
[NAME:dream catcher:dream catchers]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_STAR_RAMMOK]
[NAME:star of rammok:stars of rammok]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_TALKING_HEAD]
[NAME:mechanical talking head:mechanical talking heads]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_MYSTICAL_SERPENT_FIGURE]
[NAME:mystical serpent figurine:mystical serpent figurines]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_RING_OF_MIGHT]
[NAME:ring of might:rings of might]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_SEAL_UNDERWORLD]
[NAME:seal of the underworld:seals of the underworld]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_SEAL_ARMOK]
[NAME:seal of armok:seals of armok]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_SEAL_GUARDIANS]
[NAME:seal of the guardian:seals of the guardian]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_SEAL_OF_THE_NIGHT]
[NAME:seal of the night:seals of the night]
[HARD_MAT]

[ITEM_TOY:ITEM_TOY_LYCAN_BEAD]
[NAME:lycanthrope bead:lycanthrope beads]
[HARD_MAT]
