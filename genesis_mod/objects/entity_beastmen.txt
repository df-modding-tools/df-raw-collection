entity_beastmen

[OBJECT:ENTITY]

[ENTITY:BEASTMEN]
[INDIV_CONTROLLABLE]
[ADVENTURE_TIER:13]
[CREATURE:BEASTMAN]
[TRANSLATION:GOBLIN]
[TOY:ITEM_TOY_BOWL]
[TOY:ITEM_TOY_CUP]
[INSTRUMENT:ITEM_INSTRUMENT_FLUTE]
[SELECT_SYMBOL:WAR:NAME_WAR]
[SUBSELECT_SYMBOL:WAR:VIOLENT]
[SELECT_SYMBOL:BATTLE:NAME_BATTLE]
[SUBSELECT_SYMBOL:BATTLE:VIOLENT]
[SELECT_SYMBOL:SIEGE:NAME_SIEGE]
[SUBSELECT_SYMBOL:SIEGE:VIOLENT]
[SELECT_SYMBOL:ROAD:NAME_ROAD]
[SELECT_SYMBOL:TUNNEL:NAME_TUNNEL]
[SELECT_SYMBOL:BRIDGE:NAME_BRIDGE]
[SELECT_SYMBOL:WALL:NAME_WALL]
[SELECT_SYMBOL:REMAINING:NATURE]
[CULL_SYMBOL:ALL:DOMESTIC]
[CULL_SYMBOL:ALL:FLOWERY]
[CULL_SYMBOL:ALL:HOLY]
[CULL_SYMBOL:ALL:PEACE]
[CULL_SYMBOL:ALL:NEGATOR]
[CULL_SYMBOL:ALL:GOOD]
[USE_EVIL_PLANTS]
[USE_EVIL_WOOD]
[USE_ANIMAL_PRODUCTS]
[RIVER_PRODUCTS]
[USE_MISC_PROCESSED_WOOD_PRODUCTS]
[ABUSE_BODIES]
[OUTDOOR_FARMING]
[INVADERS_IGNORE_NEUTRALS]
[AT_PEACE_WITH_WILDLIFE]
[SPHERE_ALIGNMENT:NATURE:512]
[ART_FACET_MODIFIER:GOOD:0]
[ART_FACET_MODIFIER:EVIL:512]
[FRIENDLY_COLOR:5:0:0]
[DEFAULT_SITE_TYPE:TREE_CITY]
[LIKES_SITE:TREE_CITY]
[TOLERATES_SITE:TREE_CITY]
[TOLERATES_SITE:DARK_FORTRESS]
[TOLERATES_SITE:CITY]
[START_BIOME:ANY_TEMPERATE_FOREST]
[START_BIOME:FOREST_TAIGA]
[BIOME_SUPPORT:ANY_WETLAND:1]
[BIOME_SUPPORT:ANY_FOREST:3]
[BIOME_SUPPORT:FOREST_TAIGA:5]
[BIOME_SUPPORT:ANY_TEMPERATE_FOREST:4]

[BIOME_SUPPORT:ANY_LAKE:1]
[BIOME_SUPPORT:ANY_RIVER:1]
[PROGRESS_TRIGGER_POPULATION:1]
[PROGRESS_TRIGGER_POP_SIEGE:2]
[ACTIVE_SEASON:SUMMER] --temporary?
[ACTIVE_SEASON:WINTER]
[MAX_STARTING_CIV_NUMBER:25]
[MAX_POP_NUMBER:1000]
[MAX_SITE_POP_NUMBER:50] --a band of them, not civilized
[SCOUT]
[PERMITTED_JOB:ANIMAL_CARETAKER]
[PERMITTED_JOB:HUNTER]
[PERMITTED_JOB:TRAPPER]
[PERMITTED_JOB:ANIMAL_DISSECTOR]
[PERMITTED_JOB:BONE_CARVER]
[PERMITTED_JOB:FISHERMAN]
[PERMITTED_JOB:FISH_DISSECTOR]
[PERMITTED_JOB:FISH_CLEANER]
[PERMITTED_JOB:COOK]
[PERMITTED_JOB:BUTCHER]
[PERMITTED_JOB:HERBALIST]
[PERMITTED_JOB:BREWER]
[PERMITTED_JOB:TRADER]
[PERMITTED_JOB:DIAGNOSER]
[PERMITTED_JOB:BONE_SETTER]
[PERMITTED_JOB:SUTURER]
[PERMITTED_JOB:SURGEON]
[ETHIC:KILL_ENTITY_MEMBER:PUNISH_CAPITAL]
[ETHIC:KILL_NEUTRAL:ACCEPTABLE]
[ETHIC:KILL_ENEMY:ACCEPTABLE]
[ETHIC:KILL_ANIMAL:ACCEPTABLE]
[ETHIC:KILL_PLANT:ACCEPTABLE]
[ETHIC:TORTURE_AS_EXAMPLE:ACCEPTABLE]
[ETHIC:TORTURE_FOR_INFORMATION:ACCEPTABLE]
[ETHIC:TORTURE_FOR_FUN:ACCEPTABLE]
[ETHIC:TORTURE_ANIMALS:ACCEPTABLE]
[ETHIC:TREASON:PUNISH_CAPITAL]
[ETHIC:OATH_BREAKING:PUNISH_CAPITAL]
[ETHIC:LYING:PUNISH_SERIOUS]
[ETHIC:VANDALISM:NOT_APPLICABLE]
[ETHIC:TRESPASSING:PUNISH_SERIOUS]
[ETHIC:THEFT:PUNISH_SERIOUS]
[ETHIC:ASSAULT:PERSONAL_MATTER]
[ETHIC:SLAVERY:ACCEPTABLE]
[ETHIC:EAT_SAPIENT_OTHER:ACCEPTABLE]
[ETHIC:EAT_SAPIENT_KILL:ACCEPTABLE]
[ETHIC:MAKE_TROPHY_SAME_RACE:ACCEPTABLE]
[ETHIC:MAKE_TROPHY_SAPIENT:ACCEPTABLE]
[ETHIC:MAKE_TROPHY_ANIMAL:ACCEPTABLE]
[VARIABLE_POSITIONS:ALL]

