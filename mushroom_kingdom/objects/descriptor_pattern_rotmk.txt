descriptor_pattern_rotmk

[OBJECT:DESCRIPTOR_PATTERN]


[COLOR_PATTERN:SPOTS_WHITE_RED_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:WHITE]
	[CP_COLOR:RED]

[COLOR_PATTERN:SPOTS_WHITE_BLUE_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:WHITE]
	[CP_COLOR:BLUE]

[COLOR_PATTERN:SPOTS_WHITE_GREEN_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:WHITE]
	[CP_COLOR:GREEN]

[COLOR_PATTERN:SPOTS_WHITE_YELLOW_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:WHITE]
	[CP_COLOR:YELLOW]

[COLOR_PATTERN:SPOTS_WHITE_PURPLE_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:WHITE]
	[CP_COLOR:PURPLE]

[COLOR_PATTERN:SPOTS_WHITE_AQUA_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:WHITE]
	[CP_COLOR:AQUA]

[COLOR_PATTERN:SPOTS_WHITE_PINK_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:WHITE]
	[CP_COLOR:PINK]

[COLOR_PATTERN:SPOTS_WHITE_BROWN_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:WHITE]
	[CP_COLOR:BROWN]

[COLOR_PATTERN:SPOTS_RED_WHITE_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:RED]
	[CP_COLOR:WHITE]

[COLOR_PATTERN:SPOTS_BLUE_WHITE_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:BLUE]
	[CP_COLOR:WHITE]

[COLOR_PATTERN:SPOTS_GREEN_WHITE_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:GREEN]
	[CP_COLOR:WHITE]

[COLOR_PATTERN:SPOTS_YELLOW_WHITE_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:YELLOW]
	[CP_COLOR:WHITE]

[COLOR_PATTERN:SPOTS_PURPLE_WHITE_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:PURPLE]
	[CP_COLOR:WHITE]

[COLOR_PATTERN:SPOTS_AQUA_WHITE_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:AQUA]
	[CP_COLOR:WHITE]

[COLOR_PATTERN:SPOTS_PINK_WHITE_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:PINK]
	[CP_COLOR:WHITE]

[COLOR_PATTERN:SPOTS_BROWN_WHITE_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:BROWN]
	[CP_COLOR:WHITE]

[COLOR_PATTERN:STRIPES_RED_WHITE_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:WHITE]
	[CP_COLOR:RED]

[COLOR_PATTERN:STRIPES_BLUE_WHITE_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:WHITE]
	[CP_COLOR:BLUE]

[COLOR_PATTERN:STRIPES_GREEN_WHITE_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:WHITE]
	[CP_COLOR:GREEN]

[COLOR_PATTERN:STRIPES_YELLOW_WHITE_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:WHITE]
	[CP_COLOR:YELLOW]

[COLOR_PATTERN:STRIPES_PURPLE_WHITE_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:WHITE]
	[CP_COLOR:PURPLE]

[COLOR_PATTERN:STRIPES_AQUA_WHITE_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:WHITE]
	[CP_COLOR:AQUA]

[COLOR_PATTERN:STRIPES_PINK_WHITE_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:WHITE]
	[CP_COLOR:PINK]

[COLOR_PATTERN:STRIPES_BROWN_WHITE_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:WHITE]
	[CP_COLOR:BROWN]

[COLOR_PATTERN:SPOTS_BLACK_RED_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:BLACK]
	[CP_COLOR:RED]

[COLOR_PATTERN:SPOTS_BLACK_BLUE_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:BLACK]
	[CP_COLOR:BLUE]

[COLOR_PATTERN:SPOTS_BLACK_GREEN_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:BLACK]
	[CP_COLOR:GREEN]

[COLOR_PATTERN:SPOTS_BLACK_YELLOW_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:BLACK]
	[CP_COLOR:YELLOW]

[COLOR_PATTERN:SPOTS_BLACK_PURPLE_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:BLACK]
	[CP_COLOR:PURPLE]

[COLOR_PATTERN:SPOTS_BLACK_AQUA_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:BLACK]
	[CP_COLOR:AQUA]

[COLOR_PATTERN:SPOTS_BLACK_PINK_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:BLACK]
	[CP_COLOR:PINK]

[COLOR_PATTERN:SPOTS_BLACK_BROWN_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:BLACK]
	[CP_COLOR:BROWN]

[COLOR_PATTERN:STRIPES_BLACK_RED_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:BLACK]
	[CP_COLOR:RED]

[COLOR_PATTERN:STRIPES_BLACK_BLUE_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:BLACK]
	[CP_COLOR:BLUE]

[COLOR_PATTERN:STRIPES_BLACK_GREEN_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:BLACK]
	[CP_COLOR:GREEN]

[COLOR_PATTERN:STRIPES_BLACK_YELLOW_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:BLACK]
	[CP_COLOR:YELLOW]

[COLOR_PATTERN:STRIPES_BLACK_PURPLE_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:BLACK]
	[CP_COLOR:PURPLE]

[COLOR_PATTERN:STRIPES_BLACK_AQUA_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:BLACK]
	[CP_COLOR:AQUA]

[COLOR_PATTERN:STRIPES_BLACK_PINK_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:BLACK]
	[CP_COLOR:PINK]

[COLOR_PATTERN:STRIPES_BLACK_BROWN_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:BLACK]
	[CP_COLOR:BROWN]

[COLOR_PATTERN:SPOTS_PURPLE_AQUA_ROTMK]
	[PATTERN:SPOTS]
	[CP_COLOR:PURPLE]
	[CP_COLOR:AQUA]

[COLOR_PATTERN:STRIPES_YELLOW_BLACK_ROTMK]
	[PATTERN:STRIPES]
	[CP_COLOR:YELLOW]
	[CP_COLOR:BLACK]