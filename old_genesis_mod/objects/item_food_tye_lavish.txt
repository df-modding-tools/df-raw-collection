item_food_tye_lavish

[OBJECT:ITEM]

[ITEM_FOOD:ITEM_SALAD_CHEF]
[NAME:exquisite salad]
[LEVEL:4]

[ITEM_FOOD:ITEM_FOOD_SOUP_COLD]
[NAME:cold soup]
[LEVEL:4]

[ITEM_FOOD:ITEM_FOOD_CAKE_SLAB]
[NAME:slabcake]
[LEVEL:4]

[ITEM_FOOD:ITEM_FOOD_CAKE_LAYER]
[NAME:layercake]
[LEVEL:4]

[ITEM_FOOD:ITEM_FOOD_FANCYJELLY]
[NAME:jelly]
[LEVEL:4]

[ITEM_FOOD:ITEM_FOOD_PLATTER]
[NAME:platter]
[LEVEL:4]