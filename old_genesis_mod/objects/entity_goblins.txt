entity_goblins

[OBJECT:ENTITY]

[ENTITY:GOBLINS] baseline gobs
	[ALL_MAIN_POPS_CONTROLLABLE]
	[CREATURE:GOBLIN]
	[TRANSLATION:GOBLIN]
	[DIGGER:ITEM_WEAPON_PICK]
	[WEAPON:ITEM_WEAPON_AXE_HAND]
	[WEAPON:ITEM_WEAPON_HAMMER_WAR]
	[WEAPON:ITEM_WEAPON_SWORD_CLEAVER]
	[WEAPON:ITEM_WEAPON_SPEAR_JAGGED]
	[WEAPON:ITEM_WEAPON_MACE_SPIKED]
	[WEAPON:ITEM_WEAPON_BOW_RAZOR]
		[AMMO:ITEM_AMMO_ARROWS_JAGGED]
	[WEAPON:ITEM_WEAPON_PIKE]
	[WEAPON:ITEM_WEAPON_BARDICHE]
	[WEAPON:ITEM_WEAPON_DAGGER]
	[WEAPON:ITEM_WEAPON_MORNINGSTAR]
	[WEAPON:ITEM_WEAPON_SCIMITAR]
	[ARMOR:ITEM_ARMOR_BREASTPLATE:COMMON]
	[ARMOR:ITEM_ARMOR_MAIL_SHIRT:COMMON]
	[ARMOR:ITEM_ARMOR_LEATHER:COMMON]
	[ARMOR:ITEM_ARMOR_CLOAK:FORCED]
	[ARMOR:ITEM_ARMOR_CAPE:FORCED]
	[ARMOR:ITEM_ARMOR_TUNIC:COMMON]
	[HELM:ITEM_HELM_1HORN:COMMON]
	[HELM:ITEM_HELM_SPIKED:COMMON]
	[HELM:ITEM_HELM_KETTLE:COMMON]
	[HELM:ITEM_HELM_CAP:COMMON]
	[HELM:ITEM_HELM_MASK:UNCOMMON]
	[GLOVES:ITEM_GLOVES_GAUNTLETS:UNCOMMON]
	[GLOVES:ITEM_GLOVES_MAIL:UNCOMMON]
	[SHOES:ITEM_SHOES_BOOTS:COMMON]
	[PANTS:ITEM_PANTS_PANTS:COMMON]
	[PANTS:ITEM_PANTS_GREAVES:COMMON]
	[PANTS:ITEM_PANTS_LEGGINGS:COMMON]
	[SHIELD:ITEM_SHIELD_SHIELD]
	[SHIELD:ITEM_SHIELD_BUCKLER]
	[SIEGEAMMO:ITEM_SIEGEAMMO_BALLISTA]
	[TOY:ITEM_TOY_AXE]
	[TOY:ITEM_TOY_BOWL]
	[TOY:ITEM_TOY_CUP]
	[TOY:ITEM_TOY_GOGGLES]
	[TOY:ITEM_TOY_MEDALLION]
	[TOY:ITEM_TOY_RINGTOSS]
	[TOY:ITEM_TOY_CUBESET]
	[TRAPCOMP:ITEM_TRAPCOMP_GIANTAXEBLADE]
	[TRAPCOMP:ITEM_TRAPCOMP_ENORMOUSCORKSCREW]
	[TRAPCOMP:ITEM_TRAPCOMP_SPIKEDBALL]
	[TRAPCOMP:ITEM_TRAPCOMP_LARGESERRATEDDISC]
	[TRAPCOMP:ITEM_TRAPCOMP_MENACINGSPIKE]
	[TRAPCOMP:ITEM_TRAPCOMP_WOODCLUB] --Tomi's
	[TOOL:ITEM_TOOL_CAULDRON]
	[TOOL:ITEM_TOOL_LADLE]
	[TOOL:ITEM_TOOL_BOWL]
	[TOOL:ITEM_TOOL_MORTAR]
	[TOOL:ITEM_TOOL_PESTLE]
	[TOOL:ITEM_TOOL_KNIFE_SLICING]
	[TOOL:ITEM_TOOL_KNIFE_MEAT_CLEAVER]
	[TOOL:ITEM_TOOL_FORK_CARVING]
	[TOOL:ITEM_TOOL_JUG]
	[TOOL:ITEM_TOOL_LARGE_POT]	
	[TOOL:ITEM_TOOL_POUCH]
	[TOOL:ITEM_TOOL_WHEELBARROW]
	[TOOL:ITEM_TOOL_ALTAR]
	[TOOL:ITEM_TOOL_DIE]

	[CLOTHING]
	[SUBTERRANEAN_CLOTHING]
	[CURRENCY:COPPER:1]
	[CURRENCY:SILVER:5]
	[CURRENCY:GOLD:15]
	[SELECT_SYMBOL:WAR:NAME_WAR]
	[SUBSELECT_SYMBOL:WAR:VIOLENT]
	[SELECT_SYMBOL:BATTLE:NAME_BATTLE]
	[SUBSELECT_SYMBOL:BATTLE:VIOLENT]
	[SELECT_SYMBOL:SIEGE:NAME_SIEGE]
	[SUBSELECT_SYMBOL:SIEGE:VIOLENT]
	[SELECT_SYMBOL:ROAD:NAME_ROAD]
	[SELECT_SYMBOL:TUNNEL:NAME_TUNNEL]
	[SELECT_SYMBOL:BRIDGE:NAME_BRIDGE]
	[SELECT_SYMBOL:WALL:NAME_WALL]
	[SELECT_SYMBOL:TEMPLE:NAME_BUILDING_TEMPLE]
	[SELECT_SYMBOL:LIBRARY:NAME_BUILDING_LIBRARY]
	[SELECT_SYMBOL:MERCHANT_COMPANY:NAME_ENTITY_MERCHANT_COMPANY]
	[SELECT_SYMBOL:CRAFT_GUILD:NAME_ENTITY_MERCHANT_COMPANY] this is correct for now

	[SELECT_SYMBOL:REMAINING:EVIL]
	[CULL_SYMBOL:ALL:DOMESTIC]
	[CULL_SYMBOL:ALL:FLOWERY]
	[CULL_SYMBOL:ALL:HOLY]
	[CULL_SYMBOL:ALL:PEACE]
	[CULL_SYMBOL:ALL:NEGATOR]
	[CULL_SYMBOL:ALL:GOOD]
	[STONE_PREF]
	[METAL_PREF]
	--OUTDOOR_FARMING] --xxxhack
	[USE_CAVE_ANIMALS]
	[USE_EVIL_ANIMALS]
	[USE_EVIL_PLANTS]
	[USE_EVIL_WOOD]
	[USE_ANIMAL_PRODUCTS]
	[USE_MISC_PROCESSED_WOOD_PRODUCTS]
	[EQUIPMENT_IMPROVEMENTS]
	[ABUSE_BODIES]
	[SPHERE_ALIGNMENT:WAR:512]
	[ART_FACET_MODIFIER:GOOD:0]
	[ART_FACET_MODIFIER:EVIL:512]
	[FRIENDLY_COLOR:5:0:1]
	[DEFAULT_SITE_TYPE:CITY]
	[LIKES_SITE:DARK_FORTRESS]
	[TOLERATES_SITE:CITY]
	[TOLERATES_SITE:CAVE_DETAILED]
	[TOLERATES_SITE:DARK_FORTRESS]
	[START_BIOME:NOT_FREEZING]
	[BIOME_SUPPORT:ALL_MAIN:3]
	[BIOME_SUPPORT:ANY_RIVER:2]
	[PROGRESS_TRIGGER_PRODUCTION:3]	
	[PROGRESS_TRIGGER_PROD_SIEGE:5] 300 000 euros
	[ACTIVE_SEASON:SPRING]
	--ACTIVE_SEASON:SUMMER]
	[ACTIVE_SEASON:AUTUMN]
	--ACTIVE_SEASON:WINTER]
	
	[BABYSNATCHER] 1st goblin civ sends kidnappers instead of itemthieves, ambushes, or sieges.
	
	[MAX_STARTING_CIV_NUMBER:4]
	[MAX_POP_NUMBER:1900] low for FPS???.
	[MAX_SITE_POP_NUMBER:260]
	[BEAST_HUNTER]
	[SCOUT]
	[SCHOLAR:HISTORIAN]
	[SCHOLAR:NATURALIST]
	[SCHOLAR:CHEMIST]
	[SCHOLAR:ENGINEER]

	[PERMITTED_JOB:MINER]
	[PERMITTED_JOB:CARPENTER]
	[PERMITTED_JOB:BOWYER]
	[PERMITTED_JOB:WOODCUTTER]
	[PERMITTED_JOB:ENGRAVER]
	[PERMITTED_JOB:MASON]
	[PERMITTED_JOB:ANIMAL_CARETAKER]
	[PERMITTED_JOB:ANIMAL_TRAINER]
	[PERMITTED_JOB:HUNTER]
	[PERMITTED_JOB:TRAPPER]
	[PERMITTED_JOB:ANIMAL_DISSECTOR]
	[PERMITTED_JOB:FURNACE_OPERATOR]
	[PERMITTED_JOB:WEAPONSMITH]
	[PERMITTED_JOB:ARMORER]
	[PERMITTED_JOB:BLACKSMITH]
	[PERMITTED_JOB:METALCRAFTER]
	[PERMITTED_JOB:WOODCRAFTER]
	[PERMITTED_JOB:STONECRAFTER]
	[PERMITTED_JOB:LEATHERWORKER]
	[PERMITTED_JOB:BONE_CARVER]
	[PERMITTED_JOB:WEAVER]
	[PERMITTED_JOB:CLOTHIER]
	[PERMITTED_JOB:GLASSMAKER]
	[PERMITTED_JOB:FISHERMAN]
	[PERMITTED_JOB:FISH_DISSECTOR]
	[PERMITTED_JOB:FISH_CLEANER]
	[PERMITTED_JOB:COOK]
	[PERMITTED_JOB:SHEARER]
	[PERMITTED_JOB:SPINNER]
	[PERMITTED_JOB:BUTCHER]
	[PERMITTED_JOB:TANNER]
	[PERMITTED_JOB:DYER]
	[PERMITTED_JOB:HERBALIST]
	[PERMITTED_JOB:BREWER]
	[PERMITTED_JOB:LYE_MAKER]
	[PERMITTED_JOB:WOOD_BURNER]
	[PERMITTED_JOB:MECHANIC]
	[PERMITTED_JOB:SIEGE_ENGINEER]
	[PERMITTED_JOB:SIEGE_OPERATOR]
	[PERMITTED_JOB:PUMP_OPERATOR]
	[PERMITTED_JOB:TRADER]
	[PERMITTED_JOB:ARCHITECT]
	[PERMITTED_JOB:DIAGNOSER]
	[PERMITTED_JOB:BONE_SETTER]
	[PERMITTED_JOB:SUTURER]
	[PERMITTED_JOB:SURGEON]
	[PERMITTED_JOB:PAPERMAKER]
	[PERMITTED_JOB:BOOKBINDER]

	[PERMITTED_REACTION:TAN_A_HIDE]
	[PERMITTED_REACTION:RENDER_FAT]
	[PERMITTED_REACTION:BRONZE_MAKING]
	[PERMITTED_REACTION:HARDEN_IRON]

	--WORLD_CONSTRUCTION:ROAD] --haha, no.
	[WORLD_CONSTRUCTION:TUNNEL]
	[WORLD_CONSTRUCTION:BRIDGE]
	[BUILDS_OUTDOOR_FORTIFICATIONS]

	[ETHIC:KILL_ENTITY_MEMBER:PERSONAL_MATTER]
	[ETHIC:KILL_NEUTRAL:REQUIRED]
	[ETHIC:KILL_ENEMY:REQUIRED]
	[ETHIC:KILL_ANIMAL:ACCEPTABLE]
	[ETHIC:KILL_PLANT:ACCEPTABLE]
	[ETHIC:TORTURE_AS_EXAMPLE:ACCEPTABLE]
	[ETHIC:TORTURE_FOR_INFORMATION:ACCEPTABLE]
	[ETHIC:TORTURE_FOR_FUN:ACCEPTABLE]
	[ETHIC:TORTURE_ANIMALS:ACCEPTABLE]
	[ETHIC:TREASON:PUNISH_CAPITAL]
	[ETHIC:OATH_BREAKING:PERSONAL_MATTER]
	[ETHIC:LYING:PERSONAL_MATTER]
	[ETHIC:VANDALISM:PERSONAL_MATTER]
	[ETHIC:TRESPASSING:PERSONAL_MATTER]
	[ETHIC:THEFT:PERSONAL_MATTER]
	[ETHIC:ASSAULT:PERSONAL_MATTER]
	[ETHIC:SLAVERY:PERSONAL_MATTER]
	[ETHIC:EAT_SAPIENT_OTHER:PERSONAL_MATTER]
	[ETHIC:EAT_SAPIENT_KILL:PERSONAL_MATTER]
	[ETHIC:MAKE_TROPHY_SAME_RACE:ACCEPTABLE]
	[ETHIC:MAKE_TROPHY_SAPIENT:ACCEPTABLE]
	[ETHIC:MAKE_TROPHY_ANIMAL:ACCEPTABLE]
	[VALUE:LAW:-50]
	[VALUE:LOYALTY:-30]
	[VALUE:FAMILY:-10]
	[VALUE:FRIENDSHIP:-15]
	[VALUE:POWER:50]
	[VALUE:TRUTH:-50]
	[VALUE:CUNNING:25]
	[VALUE:ELOQUENCE:-30]
	[VALUE:FAIRNESS:-50]
	[VALUE:DECORUM:-15]
	[VALUE:TRADITION:-15]
	[VALUE:ARTWORK:0]
	[VALUE:COOPERATION:-15]
	[VALUE:INDEPENDENCE:15]
	[VALUE:STOICISM:0]
	[VALUE:KNOWLEDGE:-25]
	[VALUE:INTROSPECTION:-35]
	[VALUE:SELF_CONTROL:-50]
	[VALUE:TRANQUILITY:-15]
	[VALUE:HARMONY:-30]
	[VALUE:MERRIMENT:0]
	[VALUE:CRAFTSMANSHIP:0]
	[VALUE:MARTIAL_PROWESS:15]
	[VALUE:SKILL:0]
	[VALUE:HARD_WORK:-15]
	[VALUE:SACRIFICE:-50]
	[VALUE:COMPETITION:0]
	[VALUE:PERSEVERANCE:-15]
	[VALUE:LEISURE_TIME:-20]
	[VALUE:COMMERCE:-20]
	[VALUE:ROMANCE:-10]
	[VALUE:NATURE:-50]
	[VALUE:PEACE:-45]
	[VARIABLE_POSITIONS:ALL]
	[SITE_VARIABLE_POSITIONS:ALL]	
	[BANDITRY:36] 36% of population
	[LOCAL_BANDITRY]
	[STONE_SHAPE:RECTANGULAR_CABOCHON]
	[GEM_SHAPE:RECTANGULAR_CABOCHON]
	[GEM_SHAPE:POINT_CUT_GEM]
	[GENERATE_WIND_INSTRUMENTS]
	[GENERATE_PERCUSSION_INSTRUMENTS]
	[GENERATE_MUSICAL_FORMS]
	--GENERATE_DANCE_FORMS]
	
[ENTITY:GOBLINES] second gobs, use Latin, write books.
	[ALL_MAIN_POPS_CONTROLLABLE]
	[CREATURE:GOBLINE]
	[TRANSLATION:LATIN]
	[DIGGER:ITEM_WEAPON_PICK]
	[WEAPON:ITEM_WEAPON_AXE_HAND]
	[WEAPON:ITEM_WEAPON_HAMMER_WAR]
	[WEAPON:ITEM_WEAPON_SWORD_CLEAVER]
	[WEAPON:ITEM_WEAPON_SPEAR_JAGGED]
	[WEAPON:ITEM_WEAPON_MACE_SPIKED]
	[WEAPON:ITEM_WEAPON_BOW_RAZOR]
		[AMMO:ITEM_AMMO_ARROWS_JAGGED]
	[WEAPON:ITEM_WEAPON_PIKE]
	[WEAPON:ITEM_WEAPON_BARDICHE]
	[WEAPON:ITEM_WEAPON_DAGGER]
	[WEAPON:ITEM_WEAPON_MORNINGSTAR]
	[WEAPON:ITEM_WEAPON_SCIMITAR]
	[ARMOR:ITEM_ARMOR_BREASTPLATE:COMMON]
	[ARMOR:ITEM_ARMOR_MAIL_SHIRT:COMMON]
	[ARMOR:ITEM_ARMOR_LEATHER:COMMON]
	[ARMOR:ITEM_ARMOR_CLOAK:FORCED]
	[ARMOR:ITEM_ARMOR_CAPE:FORCED]
	[ARMOR:ITEM_ARMOR_TUNIC:COMMON]
	[HELM:ITEM_HELM_1HORN:COMMON]
	[HELM:ITEM_HELM_SPIKED:COMMON]
	[HELM:ITEM_HELM_KETTLE:COMMON]
	[HELM:ITEM_HELM_CAP:COMMON]
	[HELM:ITEM_HELM_MASK:UNCOMMON]
	[GLOVES:ITEM_GLOVES_GAUNTLETS:UNCOMMON]
	[GLOVES:ITEM_GLOVES_MAIL:UNCOMMON]
	[SHOES:ITEM_SHOES_BOOTS:COMMON]
	[PANTS:ITEM_PANTS_PANTS:FORCED]
	[PANTS:ITEM_PANTS_GREAVES:UNCOMMON]
	[PANTS:ITEM_PANTS_LEGGINGS:COMMON]
	[SHIELD:ITEM_SHIELD_SHIELD]
	[SHIELD:ITEM_SHIELD_BUCKLER]
	[SIEGEAMMO:ITEM_SIEGEAMMO_BALLISTA]
	--TOY:ITEM_TOY_AXE]
	[TOY:ITEM_TOY_BOWL]
	[TOY:ITEM_TOY_CUP]
	[TOY:ITEM_TOY_GOGGLES]
	[TOY:ITEM_TOY_MEDALLION]
	[TOY:ITEM_TOY_RINGTOSS]
	[TOY:ITEM_TOY_CUBESET]
	[TOY:ITEM_TOY_SMOKINGPIPE]
	[TRAPCOMP:ITEM_TRAPCOMP_GIANTAXEBLADE]
	[TRAPCOMP:ITEM_TRAPCOMP_ENORMOUSCORKSCREW]
	[TRAPCOMP:ITEM_TRAPCOMP_SPIKEDBALL]
	[TRAPCOMP:ITEM_TRAPCOMP_LARGESERRATEDDISC]
	[TRAPCOMP:ITEM_TRAPCOMP_MENACINGSPIKE]
	[TRAPCOMP:ITEM_TRAPCOMP_WOODCLUB]
	[TOOL:ITEM_TOOL_CAULDRON]
	[TOOL:ITEM_TOOL_LADLE]
	[TOOL:ITEM_TOOL_BOWL]
	[TOOL:ITEM_TOOL_MORTAR]
	[TOOL:ITEM_TOOL_PESTLE]
	[TOOL:ITEM_TOOL_KNIFE_SLICING]
	[TOOL:ITEM_TOOL_KNIFE_MEAT_CLEAVER]
	[TOOL:ITEM_TOOL_FORK_CARVING]

	[TOOL:ITEM_TOOL_POUCH]
	[TOOL:ITEM_TOOL_NEST_BOX]
	[TOOL:ITEM_TOOL_JUG]
	[TOOL:ITEM_TOOL_LARGE_POT]
	[TOOL:ITEM_TOOL_HIVE]
	[TOOL:ITEM_TOOL_MINECART]
	[TOOL:ITEM_TOOL_WHEELBARROW]
	[TOOL:ITEM_TOOL_STEPLADDER]

	[TOOL:ITEM_TOOL_SCROLL_ROLLERS]
	[TOOL:ITEM_TOOL_BOOK_BINDING]
	[TOOL:ITEM_TOOL_SCROLL]
	[TOOL:ITEM_TOOL_QUIRE]
	[TOOL:ITEM_TOOL_BOOKCASE]
	[TOOL:ITEM_TOOL_PEDESTAL]
	[TOOL:ITEM_TOOL_DISPLAY_CASE]
	[TOOL:ITEM_TOOL_ALTAR]
	[TOOL:ITEM_TOOL_DIE]

	[CLOTHING]
	[SUBTERRANEAN_CLOTHING]
	[CURRENCY:COPPER:1]
	[CURRENCY:SILVER:5]
	[CURRENCY:GOLD:15]
	[SELECT_SYMBOL:WAR:NAME_WAR]
	[SUBSELECT_SYMBOL:WAR:VIOLENT]
	[SELECT_SYMBOL:BATTLE:NAME_BATTLE]
	[SUBSELECT_SYMBOL:BATTLE:VIOLENT]
	[SELECT_SYMBOL:SIEGE:NAME_SIEGE]
	[SUBSELECT_SYMBOL:SIEGE:VIOLENT]
	[SELECT_SYMBOL:ROAD:NAME_ROAD]
	[SELECT_SYMBOL:TUNNEL:NAME_TUNNEL]
	[SELECT_SYMBOL:BRIDGE:NAME_BRIDGE]
	[SELECT_SYMBOL:WALL:NAME_WALL]
	[SELECT_SYMBOL:TEMPLE:NAME_BUILDING_TEMPLE]
	[SELECT_SYMBOL:LIBRARY:NAME_BUILDING_LIBRARY]
	[SELECT_SYMBOL:MERCHANT_COMPANY:NAME_ENTITY_MERCHANT_COMPANY]
	[SELECT_SYMBOL:CRAFT_GUILD:NAME_ENTITY_MERCHANT_COMPANY] this is correct for now

	[SELECT_SYMBOL:REMAINING:EVIL]
	[CULL_SYMBOL:ALL:DOMESTIC]
	[CULL_SYMBOL:ALL:FLOWERY]
	[CULL_SYMBOL:ALL:HOLY]
	[CULL_SYMBOL:ALL:PEACE]
	[CULL_SYMBOL:ALL:NEGATOR]
	[CULL_SYMBOL:ALL:GOOD]
	[STONE_PREF]
	[METAL_PREF]
	--OUTDOOR_FARMING] --xxxhack
	[USE_CAVE_ANIMALS]
	[USE_EVIL_ANIMALS]
	[USE_EVIL_PLANTS]
	[USE_EVIL_WOOD]
	[USE_ANIMAL_PRODUCTS]
	[USE_MISC_PROCESSED_WOOD_PRODUCTS]
	[EQUIPMENT_IMPROVEMENTS]
	--ABUSE_BODIES]
	[SPHERE_ALIGNMENT:WAR:512]
	[ART_FACET_MODIFIER:GOOD:0]
	[ART_FACET_MODIFIER:EVIL:512]
	[FRIENDLY_COLOR:5:0:1]
	[DEFAULT_SITE_TYPE:CITY]
	[LIKES_SITE:DARK_FORTRESS]
	[TOLERATES_SITE:CITY]
	[TOLERATES_SITE:CAVE_DETAILED]
	[TOLERATES_SITE:DARK_FORTRESS]
	[START_BIOME:NOT_FREEZING]
	[BIOME_SUPPORT:ANY_GRASSLAND:2]
	[BIOME_SUPPORT:ANY_SAVANNA:2]
	[BIOME_SUPPORT:ANY_RIVER:2]
	[PROGRESS_TRIGGER_PRODUCTION:1]
	[PROGRESS_TRIGGER_PROD_SIEGE:5] 300 000 euros
	[ACTIVE_SEASON:SPRING]
	--ACTIVE_SEASON:SUMMER]
	--ACTIVE_SEASON:AUTUMN]
	[ACTIVE_SEASON:WINTER]
	
	--BABYSNATCHER] NOPE, pls SIEGE instead
	
	[MAX_STARTING_CIV_NUMBER:4]
	[MAX_POP_NUMBER:800] low for FPS
	[MAX_SITE_POP_NUMBER:120]
	[BEAST_HUNTER]
	[SCOUT]
	[MERCENARY] --only gob civ to do this

	[SCHOLAR:PHILOSOPHER]
	[SCHOLAR:MATHEMATICIAN]
	[SCHOLAR:HISTORIAN]
	[SCHOLAR:ASTRONOMER]
	[SCHOLAR:NATURALIST]
	[SCHOLAR:CHEMIST]
	[SCHOLAR:GEOGRAPHER]
	[SCHOLAR:DOCTOR]
	[SCHOLAR:ENGINEER]

	[PERMITTED_JOB:MINER]
	[PERMITTED_JOB:CARPENTER]
	[PERMITTED_JOB:BOWYER]
	[PERMITTED_JOB:WOODCUTTER]
	[PERMITTED_JOB:ENGRAVER]
	[PERMITTED_JOB:MASON]
	[PERMITTED_JOB:ANIMAL_CARETAKER]
	[PERMITTED_JOB:ANIMAL_TRAINER]
	[PERMITTED_JOB:HUNTER]
	[PERMITTED_JOB:TRAPPER]
	[PERMITTED_JOB:FURNACE_OPERATOR]
	[PERMITTED_JOB:WEAPONSMITH]
	[PERMITTED_JOB:ARMORER]
	[PERMITTED_JOB:BLACKSMITH]
	[PERMITTED_JOB:METALCRAFTER]
	[PERMITTED_JOB:WOODCRAFTER]
	[PERMITTED_JOB:STONECRAFTER]
	[PERMITTED_JOB:LEATHERWORKER]
	[PERMITTED_JOB:BONE_CARVER]
	[PERMITTED_JOB:WEAVER]
	[PERMITTED_JOB:CLOTHIER]
	[PERMITTED_JOB:GLASSMAKER]
	[PERMITTED_JOB:FISHERMAN]
	[PERMITTED_JOB:FISH_CLEANER]
	[PERMITTED_JOB:COOK]
	[PERMITTED_JOB:SHEARER]
	[PERMITTED_JOB:SPINNER]
	[PERMITTED_JOB:BUTCHER]
	[PERMITTED_JOB:TANNER]
	[PERMITTED_JOB:DYER]
	[PERMITTED_JOB:HERBALIST]
	[PERMITTED_JOB:BREWER]
	[PERMITTED_JOB:LYE_MAKER]
	[PERMITTED_JOB:WOOD_BURNER]
	[PERMITTED_JOB:MECHANIC]
	[PERMITTED_JOB:TRADER]
	[PERMITTED_JOB:ARCHITECT]
	[PERMITTED_JOB:DIAGNOSER]
	[PERMITTED_JOB:BONE_SETTER]
	[PERMITTED_JOB:SUTURER]
	[PERMITTED_JOB:SURGEON]
	[PERMITTED_JOB:PAPERMAKER] -goblines are a tad more civilized
	[PERMITTED_JOB:BOOKBINDER]

	[PERMITTED_REACTION:TAN_A_HIDE]
	[PERMITTED_REACTION:RENDER_FAT]
	[PERMITTED_REACTION:MAKE_SOAP_FROM_TALLOW]
	[PERMITTED_REACTION:MAKE_SOAP_FROM_OIL]
	[PERMITTED_REACTION:MAKE_PEARLASH]
	[PERMITTED_REACTION:MAKE_PLASTER_POWDER]

	[PERMITTED_REACTION:MAKE_QUICKLIME]
	[PERMITTED_REACTION:MAKE_MILK_OF_LIME]
	[PERMITTED_REACTION:MAKE_PARCHMENT]
	[PERMITTED_REACTION:MAKE_SCROLL]
	[PERMITTED_REACTION:MAKE_QUIRE]
	[PERMITTED_REACTION:MAKE_SHEET_FROM_PLANT]
	[PERMITTED_REACTION:MAKE_SLURRY_FROM_PLANT]
	[PERMITTED_REACTION:PRESS_PLANT_PAPER]
	[PERMITTED_REACTION:BIND_BOOK]
	[PERMITTED_REACTION:MAKE WOODEN DISPLAY CASE]

	[PERMITTED_REACTION:MAKE_CLAY_JUG]
	[PERMITTED_REACTION:MAKE_CLAY_BRICKS]
	[PERMITTED_REACTION:MAKE_CLAY_STATUE]
	[PERMITTED_REACTION:MAKE_LARGE_CLAY_POT]
	[PERMITTED_REACTION:MAKE_CLAY_CRAFTS]
	[PERMITTED_REACTION:GLAZE_JUG]
	[PERMITTED_REACTION:GLAZE_JUG_ASH]
	[PERMITTED_REACTION:GLAZE_STATUE]
	[PERMITTED_REACTION:GLAZE_STATUE_ASH]
	[PERMITTED_REACTION:GLAZE_LARGE_POT]
	[PERMITTED_REACTION:GLAZE_LARGE_POT_ASH]
	[PERMITTED_REACTION:GLAZE_CRAFT]
	[PERMITTED_REACTION:GLAZE_CRAFT_ASH]

	[PERMITTED_REACTION:HARDEN_IRON]
	[PERMITTED_REACTION:BRONZE_MAKING]
	[PERMITTED_REACTION:WHITE_BRONZE_MAKING]
	[PERMITTED_REACTION:STEEL_MAKING_PATTERNWELD]

	[WORLD_CONSTRUCTION:ROAD] --these gobs might make roads, imitate humans
	[WORLD_CONSTRUCTION:TUNNEL]
	[WORLD_CONSTRUCTION:BRIDGE]
	[BUILDS_OUTDOOR_FORTIFICATIONS]

	[ETHIC:KILL_ENTITY_MEMBER:PERSONAL_MATTER]
	[ETHIC:KILL_NEUTRAL:REQUIRED]
	[ETHIC:KILL_ENEMY:REQUIRED]
	[ETHIC:KILL_ANIMAL:ACCEPTABLE]
	[ETHIC:KILL_PLANT:ACCEPTABLE]
	[ETHIC:TORTURE_AS_EXAMPLE:ACCEPTABLE]
	[ETHIC:TORTURE_FOR_INFORMATION:ACCEPTABLE]
	[ETHIC:TORTURE_FOR_FUN:ACCEPTABLE]
	[ETHIC:TORTURE_ANIMALS:ACCEPTABLE]
	[ETHIC:TREASON:PUNISH_CAPITAL]
	[ETHIC:OATH_BREAKING:PERSONAL_MATTER]
	[ETHIC:LYING:PERSONAL_MATTER]
	[ETHIC:VANDALISM:PERSONAL_MATTER]
	[ETHIC:TRESPASSING:PERSONAL_MATTER]
	[ETHIC:THEFT:PERSONAL_MATTER]
	[ETHIC:ASSAULT:PERSONAL_MATTER]
	[ETHIC:SLAVERY:PERSONAL_MATTER]
	[ETHIC:EAT_SAPIENT_OTHER:PERSONAL_MATTER]
	[ETHIC:EAT_SAPIENT_KILL:PERSONAL_MATTER]
	[ETHIC:MAKE_TROPHY_SAME_RACE:ACCEPTABLE]
	[ETHIC:MAKE_TROPHY_SAPIENT:ACCEPTABLE]
	[ETHIC:MAKE_TROPHY_ANIMAL:ACCEPTABLE]
	[VALUE:LAW:-16] more law
	[VALUE:LOYALTY:10] more loyalty
	[VALUE:FAMILY:10]
	[VALUE:FRIENDSHIP:-15]
	[VALUE:POWER:30]
	[VALUE:TRUTH:-40]
	[VALUE:CUNNING:35] more cunning
	[VALUE:ELOQUENCE:-10] more
	[VALUE:FAIRNESS:-10] 40 more
	[VALUE:DECORUM:-15]
	[VALUE:TRADITION:-15]
	[VALUE:ARTWORK:18] more
	[VALUE:COOPERATION:15] more coop
	[VALUE:INDEPENDENCE:15]
	[VALUE:STOICISM:0]
	[VALUE:KNOWLEDGE:18] these the clever goblins
	[VALUE:INTROSPECTION:-10]
	[VALUE:SELF_CONTROL:-10]
	[VALUE:TRANQUILITY:-5]
	[VALUE:HARMONY:-5]
	[VALUE:MERRIMENT:10]
	[VALUE:CRAFTSMANSHIP:-6]
	[VALUE:MARTIAL_PROWESS:9]
	[VALUE:SKILL:20]
	[VALUE:HARD_WORK:-15]
	[VALUE:SACRIFICE:-50] selfish goblins
	[VALUE:COMPETITION:40]
	[VALUE:PERSEVERANCE:-15]
	[VALUE:LEISURE_TIME:40] lazy bookworm goblins
	[VALUE:COMMERCE:18]
	[VALUE:ROMANCE:45]
	[VALUE:NATURE:-36] goblins no like nature hippie elves
	[VALUE:PEACE:-5]
	[VARIABLE_POSITIONS:ALL]
	[SITE_VARIABLE_POSITIONS:ALL]	
	[BANDITRY:9] 9% of pop
	[LOCAL_BANDITRY]
	[STONE_SHAPE:RECTANGULAR_CABOCHON]
	[GEM_SHAPE:RECTANGULAR_CABOCHON]
	[GEM_SHAPE:POINT_CUT_GEM]
	[GENERATE_WIND_INSTRUMENTS]
	[GENERATE_PERCUSSION_INSTRUMENTS]
	[GENERATE_POETIC_FORMS]
	[GENERATE_MUSICAL_FORMS]
	[GENERATE_DANCE_FORMS]
	
[ENTITY:GROBLINS] third goblins, extra crude and primitive. Less armor.
	[ALL_MAIN_POPS_CONTROLLABLE]
	[CREATURE:GROBLIN]
	[TRANSLATION:GOBLIN]
	[DIGGER:ITEM_WEAPON_PICK]
	[WEAPON:ITEM_WEAPON_AXE_HAND]
	--WEAPON:ITEM_WEAPON_HAMMER_WAR]
	[WEAPON:ITEM_WEAPON_SWORD_CLEAVER]
	[WEAPON:ITEM_WEAPON_SPEAR_JAGGED]
	[WEAPON:ITEM_WEAPON_MACE_SPIKED]
	[WEAPON:ITEM_WEAPON_DAGGER]
	[WEAPON:ITEM_WEAPON_SCIMITAR]
	--ARMOR:ITEM_ARMOR_BREASTPLATE:COMMON]
	--ARMOR:ITEM_ARMOR_MAIL_SHIRT:COMMON]
	[ARMOR:ITEM_ARMOR_LEATHER:COMMON]
	[ARMOR:ITEM_ARMOR_CLOAK:FORCED]
	[ARMOR:ITEM_ARMOR_CAPE:FORCED]
	[ARMOR:ITEM_ARMOR_TUNIC:COMMON]
	[HELM:ITEM_HELM_1HORN:COMMON]
	[HELM:ITEM_HELM_SPIKED:COMMON]
	[HELM:ITEM_HELM_KETTLE:COMMON]
	--HELM:ITEM_HELM_CAP:COMMON]
	--HELM:ITEM_HELM_MASK:UNCOMMON]
	--GLOVES:ITEM_GLOVES_GAUNTLETS:UNCOMMON]
	--GLOVES:ITEM_GLOVES_MAIL:UNCOMMON]
	[SHOES:ITEM_SHOES_BOOTS:COMMON]
	[PANTS:ITEM_PANTS_PANTS:COMMON]
	--PANTS:ITEM_PANTS_GREAVES:COMMON]
	[PANTS:ITEM_PANTS_LEGGINGS:COMMON]
	[SIEGEAMMO:ITEM_SIEGEAMMO_BALLISTA]
	[TOY:ITEM_TOY_AXE]
	[TOY:ITEM_TOY_BOWL]
	[TOY:ITEM_TOY_CUP]
	--TOY:ITEM_TOY_GOGGLES]
	[TOY:ITEM_TOY_MEDALLION]
	[TOY:ITEM_TOY_RINGTOSS]
	[TOY:ITEM_TOY_CUBESET]
	[TRAPCOMP:ITEM_TRAPCOMP_GIANTAXEBLADE]
	--TRAPCOMP:ITEM_TRAPCOMP_ENORMOUSCORKSCREW]
	[TRAPCOMP:ITEM_TRAPCOMP_SPIKEDBALL]
	--TRAPCOMP:ITEM_TRAPCOMP_LARGESERRATEDDISC]
	[TRAPCOMP:ITEM_TRAPCOMP_MENACINGSPIKE]
	[TRAPCOMP:ITEM_TRAPCOMP_WOODCLUB] --Tomi's
	[TOOL:ITEM_TOOL_CAULDRON]
	[TOOL:ITEM_TOOL_LADLE]
	[TOOL:ITEM_TOOL_BOWL]
	[TOOL:ITEM_TOOL_MORTAR]
	[TOOL:ITEM_TOOL_PESTLE]
	[TOOL:ITEM_TOOL_KNIFE_SLICING]
	[TOOL:ITEM_TOOL_KNIFE_MEAT_CLEAVER]
	[TOOL:ITEM_TOOL_FORK_CARVING]
	[TOOL:ITEM_TOOL_JUG]
	[TOOL:ITEM_TOOL_LARGE_POT]	
	[TOOL:ITEM_TOOL_POUCH]
	[TOOL:ITEM_TOOL_WHEELBARROW]
	[TOOL:ITEM_TOOL_ALTAR]
	[TOOL:ITEM_TOOL_DIE]

	[CLOTHING]
	[SUBTERRANEAN_CLOTHING]
	[CURRENCY:COPPER:1]
	[CURRENCY:SILVER:5]
	[CURRENCY:GOLD:15]
	[SELECT_SYMBOL:WAR:NAME_WAR]
	[SUBSELECT_SYMBOL:WAR:VIOLENT]
	[SELECT_SYMBOL:BATTLE:NAME_BATTLE]
	[SUBSELECT_SYMBOL:BATTLE:VIOLENT]
	[SELECT_SYMBOL:SIEGE:NAME_SIEGE]
	[SUBSELECT_SYMBOL:SIEGE:VIOLENT]
	[SELECT_SYMBOL:ROAD:NAME_ROAD]
	[SELECT_SYMBOL:TUNNEL:NAME_TUNNEL]
	[SELECT_SYMBOL:BRIDGE:NAME_BRIDGE]
	[SELECT_SYMBOL:WALL:NAME_WALL]
	[SELECT_SYMBOL:TEMPLE:NAME_BUILDING_TEMPLE]
	[SELECT_SYMBOL:LIBRARY:NAME_BUILDING_LIBRARY]
	[SELECT_SYMBOL:MERCHANT_COMPANY:NAME_ENTITY_MERCHANT_COMPANY]
	[SELECT_SYMBOL:CRAFT_GUILD:NAME_ENTITY_MERCHANT_COMPANY] this is correct for now

	[SELECT_SYMBOL:REMAINING:EVIL]
	[CULL_SYMBOL:ALL:DOMESTIC]
	[CULL_SYMBOL:ALL:FLOWERY]
	[CULL_SYMBOL:ALL:HOLY]
	[CULL_SYMBOL:ALL:PEACE]
	[CULL_SYMBOL:ALL:NEGATOR]
	[CULL_SYMBOL:ALL:GOOD]
	[STONE_PREF]
	[METAL_PREF]
	--OUTDOOR_FARMING] --xxxhack
	[USE_CAVE_ANIMALS]
	[USE_EVIL_ANIMALS]
	[USE_EVIL_PLANTS]
	[USE_EVIL_WOOD]
	[USE_ANIMAL_PRODUCTS]
	[USE_MISC_PROCESSED_WOOD_PRODUCTS]
	[EQUIPMENT_IMPROVEMENTS]
	
	[ABUSE_BODIES]
	[SPHERE_ALIGNMENT:WAR:312]
	[ART_FACET_MODIFIER:GOOD:0]
	[ART_FACET_MODIFIER:EVIL:512]
	[FRIENDLY_COLOR:5:0:1]
	[DEFAULT_SITE_TYPE:CITY]
	[LIKES_SITE:DARK_FORTRESS]
	[TOLERATES_SITE:CITY]
	[TOLERATES_SITE:CAVE_DETAILED]
	[TOLERATES_SITE:DARK_FORTRESS]

	[START_BIOME:NOT_FREEZING]
	[BIOME_SUPPORT:ALL_MAIN:1]
	[BIOME_SUPPORT:ANY_RIVER:2]
	[PROGRESS_TRIGGER_PRODUCTION:2]
	[PROGRESS_TRIGGER_PROD_SIEGE:5] 300 000 euros
	--ACTIVE_SEASON:SPRING]
	[ACTIVE_SEASON:SUMMER]
	--ACTIVE_SEASON:AUTUMN]
	[ACTIVE_SEASON:WINTER]
	
	[BABYSNATCHER]
	[MAX_STARTING_CIV_NUMBER:4]
	[MAX_POP_NUMBER:800] low for FPS
	[MAX_SITE_POP_NUMBER:129]
	[BEAST_HUNTER]
	[SCOUT]
	--no scholars
	[PERMITTED_JOB:MINER]
	[PERMITTED_JOB:CARPENTER]
	[PERMITTED_JOB:BOWYER]
	[PERMITTED_JOB:WOODCUTTER]
	[PERMITTED_JOB:ENGRAVER]
	[PERMITTED_JOB:MASON]
	[PERMITTED_JOB:ANIMAL_CARETAKER]
	[PERMITTED_JOB:ANIMAL_TRAINER]
	[PERMITTED_JOB:HUNTER]
	[PERMITTED_JOB:FURNACE_OPERATOR]
	[PERMITTED_JOB:WEAPONSMITH]
	[PERMITTED_JOB:LEATHERWORKER]
	[PERMITTED_JOB:BONE_CARVER]
	[PERMITTED_JOB:WEAVER]
	[PERMITTED_JOB:CLOTHIER]
	[PERMITTED_JOB:COOK]
	[PERMITTED_JOB:BUTCHER]
	[PERMITTED_JOB:TANNER]
	[PERMITTED_JOB:DYER]
	[PERMITTED_JOB:HERBALIST]
	[PERMITTED_JOB:BREWER]
	[PERMITTED_REACTION:TAN_A_HIDE]
	[PERMITTED_REACTION:RENDER_FAT]
	[PERMITTED_REACTION:BRONZE_MAKING]
	[PERMITTED_REACTION:HARDEN_IRON]

	--WORLD_CONSTRUCTION:ROAD] --haha, no.
	--WORLD_CONSTRUCTION:TUNNEL]
	--WORLD_CONSTRUCTION:BRIDGE]
	--BUILDS_OUTDOOR_FORTIFICATIONS]

	[ETHIC:KILL_ENTITY_MEMBER:PERSONAL_MATTER]
	[ETHIC:KILL_NEUTRAL:REQUIRED]
	[ETHIC:KILL_ENEMY:REQUIRED]
	[ETHIC:KILL_ANIMAL:ACCEPTABLE]
	[ETHIC:KILL_PLANT:ACCEPTABLE]
	[ETHIC:TORTURE_AS_EXAMPLE:ACCEPTABLE]
	[ETHIC:TORTURE_FOR_INFORMATION:ACCEPTABLE]
	[ETHIC:TORTURE_FOR_FUN:ACCEPTABLE]
	[ETHIC:TORTURE_ANIMALS:ACCEPTABLE]
	[ETHIC:TREASON:PUNISH_CAPITAL]
	[ETHIC:OATH_BREAKING:PERSONAL_MATTER]
	[ETHIC:LYING:PERSONAL_MATTER]
	[ETHIC:VANDALISM:PERSONAL_MATTER]
	[ETHIC:TRESPASSING:PERSONAL_MATTER]
	[ETHIC:THEFT:ACCEPTABLE]
	[ETHIC:ASSAULT:ACCEPTABLE]
	[ETHIC:SLAVERY:ACCEPTABLE]
	[ETHIC:EAT_SAPIENT_OTHER:PERSONAL_MATTER]
	[ETHIC:EAT_SAPIENT_KILL:PERSONAL_MATTER]
	[ETHIC:MAKE_TROPHY_SAME_RACE:ACCEPTABLE]
	[ETHIC:MAKE_TROPHY_SAPIENT:ACCEPTABLE]
	[ETHIC:MAKE_TROPHY_ANIMAL:ACCEPTABLE]
	[VALUE:LAW:-40]
	[VALUE:LOYALTY:-5] more loyalty
	[VALUE:FAMILY:40]
	[VALUE:FRIENDSHIP:-15]
	[VALUE:POWER:40]
	[VALUE:TRUTH:-50]
	[VALUE:CUNNING:35] more cunning
	[VALUE:ELOQUENCE:-30]
	[VALUE:FAIRNESS:-50]
	[VALUE:DECORUM:-15]
	[VALUE:TRADITION:15]
	[VALUE:ARTWORK:-40] less
	[VALUE:COOPERATION:18] more coop
	[VALUE:INDEPENDENCE:45]
	[VALUE:STOICISM:0]
	[VALUE:KNOWLEDGE:-45]
	[VALUE:INTROSPECTION:-35]
	[VALUE:SELF_CONTROL:-50]
	[VALUE:TRANQUILITY:-15]
	[VALUE:HARMONY:-50]
	[VALUE:MERRIMENT:20] more
	[VALUE:CRAFTSMANSHIP:-25]
	[VALUE:MARTIAL_PROWESS:35] more
	[VALUE:SKILL:-25]
	[VALUE:HARD_WORK:-15]
	[VALUE:SACRIFICE:-10] 40 more
	[VALUE:COMPETITION:20] 20 more
	[VALUE:PERSEVERANCE:15] 30 more
	[VALUE:LEISURE_TIME:0]
	[VALUE:COMMERCE:-40]
	[VALUE:ROMANCE:-30]
	[VALUE:NATURE:-50] 20 less
	[VALUE:PEACE:-45] 10 less
	[VARIABLE_POSITIONS:ALL]
	[SITE_VARIABLE_POSITIONS:ALL]	
	[BANDITRY:30]
	[LOCAL_BANDITRY]
	[STONE_SHAPE:RECTANGULAR_CABOCHON]
	[GEM_SHAPE:RECTANGULAR_CABOCHON]
	[GEM_SHAPE:POINT_CUT_GEM]
	[GENERATE_WIND_INSTRUMENTS]
	[GENERATE_PERCUSSION_INSTRUMENTS]
	[GENERATE_MUSICAL_FORMS]
	--GENERATE_DANCE_FORMS]
